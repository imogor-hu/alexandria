To publish patch:
```yarn lerna version patch```

To publish minor version:
```yarn lerna version minor```

To force publish even without changes:
```yarn lerna version patch --force-publish```