import { MutableRefObject, useCallback } from "react";
import { from, Subject } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { FileUploadProps } from "../types";
import { createFileEntryFromFile, FileEntry } from "../file-entry";

export function useDrop(propsRef: MutableRefObject<FileUploadProps>, cancelSubject: Subject<FileEntry | null>) {
  return useCallback((acceptedFiles: (File & { existingId?: string })[]) => {
    const limit = propsRef.current.limit;
    const filesToAllow = limit ? limit - propsRef.current.value.length : Number.MAX_VALUE;

    if (0 === filesToAllow) return;

    if (filesToAllow < acceptedFiles.length) acceptedFiles = acceptedFiles.slice(0, filesToAllow);

    const uploads = acceptedFiles.map(createFileEntryFromFile);

    const uploadMap = new Map<string, FileEntry>([...propsRef.current.value].map((elem) => [elem.uid, elem]));
    for (const elem of uploads) {
      uploadMap.set(elem.uid, { ...uploadMap.get(elem.uid), ...elem });
    }
    propsRef.current.onChange([...uploadMap.values()]);

    if (propsRef.current.onUploadFile) {
      for (const upload of uploads) {
        from(propsRef.current.onUploadFile(upload))
          .pipe(takeUntil(cancelSubject.pipe(filter((file) => file === null || file.uid === upload.uid))))
          .subscribe((file) => {
            const files = propsRef.current.value;

            const foundIndex = files.findIndex((f) => f.uid === file.uid);

            const newFiles = [...files];

            if (foundIndex < 0) {
              newFiles.push(file);
            } else {
              newFiles[foundIndex] = { ...files[foundIndex], ...file };
            }

            propsRef.current.onChange(newFiles);
          });
      }
    }
  }, []);
}
