import { useCallback } from "react";
import { FileEntry } from "../file-entry";
import { throwError } from "rxjs";
import { client, UploadFileOptions, UploadStatusStatus } from "@litbase/client";
import { map } from "rxjs/operators";

export function useOnUploadFile(options?: UploadFileOptions) {
  return useCallback((entry: FileEntry) => {
    if (!entry.file) {
      return throwError(() => new Error("No file value in FileEntry"));
    }

    return client.uploadFile(entry.file, options).pipe(
      map(
        (status) =>
          ({
            uid: entry.uid,
            file: status.file,
            status: toTransitionStatus(status.status),
            error: status.error,
            progress: status.progress,
            contentUrl: status.contentUrl || status.response?.response?.url,
          } as FileEntry)
      )
    );
  }, []);
}

export function toTransitionStatus(value: UploadStatusStatus) {
  switch (value) {
    case UploadStatusStatus.Done:
      return "done";
    case UploadStatusStatus.Errored:
      return "errored";
    case UploadStatusStatus.Working:
    default:
      return "working";
  }
}
