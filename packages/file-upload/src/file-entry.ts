import { TransitionStatus } from "@litbase/alexandria";

export interface FileEntry {
  uid: string;
  status: TransitionStatus;
  file?: File;
  error?: Error;
  progress?: number;
  preview?: string;
  contentUrl?: string;
  thumbnailUrl?: string;
}

export function createFileEntryFromFile(file: File): FileEntry {
  return {
    uid: file.name + "-" + Date.now(),
    file: file,
    status: "working",
    progress: 0,
  };
}
