import { FileEntry } from "../file-entry";

export function getFileEntryFromName(url: string, existingEntries: FileEntry[]): FileEntry {
  return (
    existingEntries.find((e) => e.uid === url || e.contentUrl === url) || {
      uid: url,
      contentUrl: url,
      status: "done",
    }
  );
}
