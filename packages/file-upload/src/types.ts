import { ObservableInput } from "rxjs";
import { CSSProperties, ReactNode } from "react";
import { FileEntry } from "./file-entry";

export interface OnUploadFileCallback {
  (file: FileEntry): ObservableInput<FileEntry>;
}

export interface FileUploadProps {
  value: FileEntry[];

  onChange(value: FileEntry[]): void;

  onUploadFile?: OnUploadFileCallback;

  onDeleteFile?(file: FileEntry): void;

  limit?: number;
  className?: string;
  style?: CSSProperties;
  accept?: string | string[];
  disabled?: boolean;
  preview?: ({ file }: { file: FileEntry }) => JSX.Element | null;
  ratio?: string | number;
  addIcon?: ReactNode;
}
