import React, { MutableRefObject, useCallback, useEffect, useRef } from "react";
import { useDropzone, Accept } from "react-dropzone";
import { Subject } from "rxjs";
import { AddFileUploadItem, FileUploadItem } from "./file-upload-item";
import styled from "styled-components";
import { noop } from "lodash-es";
import { FileEntry } from "./file-entry";
import useConstant from "use-constant";
import { closestCenter, DndContext, DragEndEvent, MouseSensor, useSensor, useSensors } from "@dnd-kit/core";
import { restrictToWindowEdges } from "@dnd-kit/modifiers";
import { rectSortingStrategy, SortableContext } from "@dnd-kit/sortable";
import { FileUploadProps } from "./types";
import { useDrop } from "./hooks/use-drop";
import { moveItem, Well } from "@litbase/alexandria";

const dragModifiers = [restrictToWindowEdges];

/**
 * Component for uploading files. Uses a Well to accept drag & drop and an <input /> for file manager dialog upload.
 * Works with FileEntity[] values
 * */
export function FileUpload(props: FileUploadProps) {
  const {
    value = [],
    limit = Number.POSITIVE_INFINITY,
    className,
    style,
    accept,
    preview,
    ratio,
    onChange = noop,
    addIcon,
  } = props;

  const propsRef = useRef(props);
  propsRef.current = props;

  const disabled = props.disabled || limit <= value.length;
  const cancelSubject = useConstant(() => new Subject<FileEntry | null>());
  const onDeleteClick = useOnDeleteClick(propsRef, cancelSubject);
  const onDrop = useDrop(propsRef, cancelSubject);

  useEffect(() => () => cancelSubject.next(null), []);

  const multiple = 1 < limit;
  const { getRootProps, getInputProps } = useDropzone({
    accept: accept as Accept | undefined,
    disabled,
    multiple,
    onDrop,
  });

  const sensors = useSensors(useSensor(MouseSensor));

  function onDragEnd({ active, over }: DragEndEvent) {
    if (!over || active.id === over.id) return;

    // Find the item being moved
    const fromIndex = value.findIndex((item) => item.uid === active.id);
    // Find the item being moved over to
    const toIndex = value.findIndex((item) => item.uid === over.id);

    onChange(moveItem(value, fromIndex, toIndex));
  }

  return (
    <DndContext sensors={sensors} collisionDetection={closestCenter} modifiers={dragModifiers} onDragEnd={onDragEnd}>
      <SortableContext items={value.map((item) => item.uid)} strategy={rectSortingStrategy}>
        <StyledFileUpload $disabled={disabled} $multiple={multiple} {...getRootProps({ className, style })}>
          <input {...getInputProps()} />
          {value.map((file) => (
            <FileUploadItem key={file.uid} file={file} preview={preview} onDeleteClick={onDeleteClick} ratio={ratio} />
          ))}
          {value.length < limit && <AddFileUploadItem addIcon={addIcon} />}
        </StyledFileUpload>
      </SortableContext>
    </DndContext>
  );
}

function useOnDeleteClick(propsRef: MutableRefObject<FileUploadProps>, cancelSubject: Subject<FileEntry | null>) {
  return useCallback((file: FileEntry) => {
    const { onDeleteFile = noop, value, onChange } = propsRef.current;

    onDeleteFile(file);

    const foundIndex = value.indexOf(file);

    if (foundIndex < 0) return;

    const newFiles = [...value];
    newFiles.splice(foundIndex, 1);

    onChange(newFiles);

    cancelSubject.next(file);
  }, []);
}

const StyledFileUpload = styled(Well)<{ $multiple: boolean; $disabled: boolean }>`
  width: ${({ $multiple }) => ($multiple ? "100%" : "auto")};
  display: ${({ $multiple }) => ($multiple ? "flex" : "inline-flex")};
  flex-wrap: wrap;
`;
