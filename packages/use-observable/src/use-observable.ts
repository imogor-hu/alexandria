import { from, Observable, ObservableInput, Subject } from "rxjs";
import { filter, retryWhen, tap } from "rxjs/operators";
import { useRef } from "react";
import { useAction } from "use-action";
import { useForceUpdate } from "@litbase/use-force-update";
import { useConstant } from "./use-constant.js";

type UseObservableState<TValue> = [TValue, boolean, Error | null, boolean];

export function useObservable<T>(inputFactory: () => ObservableInput<T>): UseObservableState<T | null>;
export function useObservable<T>(inputFactory: () => ObservableInput<T>, initialValue: T): UseObservableState<T>;
export function useObservable<T, TInputs extends ReadonlyArray<unknown>>(
  inputFactory: (inputs$: Observable<TInputs>) => ObservableInput<T>,
  initialValue: T,
  inputs: TInputs
): UseObservableState<T>;
export function useObservable<T, TInputs extends ReadonlyArray<unknown>>(
  inputFactory: (inputs$: Observable<TInputs>) => ObservableInput<T>,
  initialValue: T = null as unknown as T,
  inputs: TInputs = [] as never
) {
  const inputs$ = useConstant(() => new Subject<TInputs>());
  const stateRef = useRef([initialValue, true, null, false]);
  const forceUpdate = useForceUpdate();

  useAction(() => {
    // Reset error
    stateRef.current[2] = null;

    inputs$.next(inputs);
  }, inputs);

  useAction(() => {
    // If the observable we get fires synchronously, don't call forceUpdate()
    let initialCall = true;

    const subscription = from(inputFactory(inputs$))
      .pipe(
        tap({
          error(error) {
            console.error(error);
            stateRef.current[1] = false;
            stateRef.current[2] = error;

            if (!initialCall) forceUpdate();
          },
        }),
        retryWhen((errors) => {
          let preventSyncReemit = true;

          Promise.resolve().then(() => (preventSyncReemit = false));

          return errors.pipe(filter(() => !preventSyncReemit));
        })
      )
      .subscribe({
        next(state) {
          const valueChange = stateRef.current[0] !== state;
          const isLoadingChange = stateRef.current[1] !== false;

          stateRef.current[0] = state;
          stateRef.current[1] = false;

          if (!initialCall && (valueChange || isLoadingChange)) {
            // setState needs a (value !== previousValue) every time, otherwise it will be a no-op
            forceUpdate();
          }
        },
        complete() {
          stateRef.current[3] = true;

          if (!initialCall) forceUpdate();
        },
      });

    inputs$.next(inputs);

    initialCall = false;

    return () => {
      // The unsubscribe should be enough, but better safe than sorry
      inputs$.complete();
      subscription.unsubscribe();
    };
  }, []);

  return stateRef.current;
}
