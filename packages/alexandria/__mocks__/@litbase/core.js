import * as actual from "@litbase/core";

module.exports = actual;
for (const collectionName of ["docs"]) {
  module.exports.litql.registerCollection({
    name: collectionName,
    collection: new actual.InMemoryDatabaseCollection(collectionName),
  });
}
