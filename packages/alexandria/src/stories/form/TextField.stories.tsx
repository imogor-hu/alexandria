import { Meta, Story } from "@storybook/react";
import { HookInputField } from "../../components/hook-form/hook-input-field.js";
import { HookForm } from "../../components/hook-form/hook-form.js";

export default {
  title: "Form/TextField",
  component: HookInputField,
  argTypes: {
    name: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <HookForm watchDefaultValues defaultValues={{ text: args.value }}>
    <HookInputField name="text" disabled={args.disabled} label={args.label} />
  </HookForm>
);
export const Normal = Template.bind({});

Normal.args = {
  value: "Some value",
  label: "Text field",
  disabled: false,
  // error: false,
};
