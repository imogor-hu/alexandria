import { Meta, Story } from "@storybook/react";
import { HookSubmitButton } from "../../components/hook-form/hook-submit-button.js";
import { HookForm } from "../../components/hook-form/hook-form.js";

export default {
  title: "Form/SubmitButton",
  component: HookSubmitButton,
  argTypes: {
    children: {
      control: "text",
    },
    className: {
      table: {
        disable: true,
      },
    },
    onClick: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <HookForm watchDefaultValues defaultValues={{ check: args.value }}>
    <HookSubmitButton>{args.children}</HookSubmitButton>
  </HookForm>
);
export const Normal = Template.bind({});
Normal.args = {
  children: "Submit",
};
