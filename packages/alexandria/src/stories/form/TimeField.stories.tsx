import { Meta, Story } from "@storybook/react";
import { HookTimeField } from "../../components/hook-form/hook-time-field.js";
import { HookForm } from "../../components/hook-form/hook-form.js";

export default {
  title: "Form/TimeField",
  component: HookTimeField,
  argTypes: {
    value: {
      control: "date",
    },
    label: {
      control: "text",
    },
    name: {
      table: {
        disable: true,
      },
    },
    parse: {
      table: {
        disable: true,
      },
    },
    transform: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <HookForm watchDefaultValues defaultValues={{ time: new Date(args.value) }}>
    <HookTimeField name="time" disabled={args.disabled} label={args.label} />
  </HookForm>
);

export const Normal = Template.bind({});
Normal.args = {
  value: new Date(),
  disabled: false,
  label: "Time input",
};
