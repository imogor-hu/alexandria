import { Meta, Story } from "@storybook/react";
import { Select } from "../../components/index.js";

export default {
  title: "Form/Select",
  component: Select,
  argTypes: {
    onChange: {
      table: {
        disable: true,
      },
    },
    parse: {
      table: {
        disable: true,
      },
    },
    format: {
      table: {
        disable: true,
      },
    },
    inputValue: {
      table: {
        disable: true,
      },
    },
    onInputChange: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <Select value={args.value} disabled={args.disabled} options={args.options} isCreatable={args.isCreatable} />
);

export const Normal = Template.bind({});

Normal.args = {
  value: "one",
  options: [{ value: "one", label: "Option One" }],
  disabled: false,
  isCreatable: false,
};
