import { Meta, Story } from "@storybook/react";
import { HookForm } from "../../components/hook-form/hook-form.js";
import { HookButtonGroupField } from "../../components/hook-form/hook-button-group-field.js";

export default {
  title: "Form/ButtonGroup",
  component: HookButtonGroupField,
  argTypes: {
    name: {
      table: {
        disable: true,
      },
    },
    label: {
      control: "text",
    },
  },
} as Meta;

const Template: Story = (args) => (
  <HookForm watchDefaultValues defaultValues={{ group: args.value }}>
    <HookButtonGroupField name="group" options={args.options} label={args.label} />
  </HookForm>
);
export const Normal = Template.bind({});

Normal.args = {
  value: "first",
  label: "Button group",
  options: [
    { value: "first", label: "First", tooltip: "First option" },
    { value: "second", label: "Second" },
  ],
};
