import { Meta, Story } from "@storybook/react";
import { HookRichTextField } from "../../components/hook-form/hook-rich-text-field.js";
import { HookForm } from "../../components/hook-form/hook-form.js";

export default {
  title: "Form/RichTextField",
  component: HookRichTextField,
  argTypes: {
    label: {
      control: "text",
    },
    config: {
      control: "text",
    },
    name: {
      table: {
        disable: true,
      },
    },
    onReady: {
      table: {
        disable: true,
      },
    },
    parse: {
      table: {
        disable: true,
      },
    },
    transform: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <HookForm watchDefaultValues defaultValues={{ text: args.value }}>
    <HookRichTextField name="text" label={args.label} />
  </HookForm>
);

export const Normal = Template.bind({});

Normal.args = {
  value: "Some value",
  label: "Text field",
};
