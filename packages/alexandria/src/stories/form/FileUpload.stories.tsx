import { Meta, Story } from "@storybook/react";
import { HookSingleFileUploadField } from "../../components/hook-form/hook-file-upload-field.js";
import { HookForm } from "../../components/hook-form/hook-form.js";

export default {
  title: "Form/FileUpload",
  component: HookSingleFileUploadField,
} as Meta;

const Template: Story = () => (
  <HookForm>
    <HookSingleFileUploadField
      name="file"
      onUploadFile={() => {
        window.alert("Actual upload not implemented...");
        return [];
      }}
    />
  </HookForm>
);

export const Normal = Template.bind({});
Normal.parameters = {
  controls: { hideNoControlsWarning: true },
};
