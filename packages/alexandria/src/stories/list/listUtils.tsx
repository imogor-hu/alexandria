import { Cell, GridRow } from "../../components/index.js";
import { ReactNode } from "react";

export interface LooseObject {
  [key: string]: ReactNode;
}

export const rowComponent = (item: LooseObject, index: number) => (
  <GridRow key={index}>
    {Object.keys(item).map((key, cellindex) => (
      <Cell key={cellindex}>{item[key]}</Cell>
    ))}
  </GridRow>
);

export function mockDataGenerator(rows: number, columns: number) {
  const mocked_data = [];
  for (let i = 0; i < rows; i++) {
    const obj: LooseObject = {};
    for (let j = 0; j < columns; j++) {
      obj["k" + j] = `R${i} | C${j}`;
    }
    mocked_data.push(obj);
  }
  return mocked_data;
}
