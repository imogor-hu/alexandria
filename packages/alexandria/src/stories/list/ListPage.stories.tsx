import { Meta, Story } from "@storybook/react";
import { GridRow, HeadCell } from "../../components/index.js";
import { CustomListPage } from "../../components/layout/list/list-page.js";
import { BrowserRouter } from "react-router-dom";
import { LooseObject, mockDataGenerator, rowComponent } from "./listUtils.js";

export default {
  title: "List/ListPage",
  component: CustomListPage,
  argTypes: {
    columns: {
      control: "number",
    },
    addButtonLink: {
      table: {
        disable: true,
      },
    },
    searchParams: {
      table: {
        disable: true,
      },
    },
    items: {
      table: {
        disable: true,
      },
    },
    buttons: {
      table: {
        disable: true,
      },
    },
    rowComponent: {
      table: {
        disable: true,
      },
    },
    headerRowComponent: {
      table: {
        disable: true,
      },
    },
    orderParams: {
      table: {
        disable: true,
      },
    },
    filters: {
      table: {
        disable: true,
      },
    },
    children: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => {
  const headCells = [];
  for (let i = 0; i < args.columns; i++) {
    headCells.push("Column " + i);
  }
  const headerRowComponent = (
    <GridRow>
      {headCells.map((item, index) => (
        <HeadCell key={index}>{item}</HeadCell>
      ))}
    </GridRow>
  );
  return (
    <BrowserRouter>
      <CustomListPage<LooseObject>
        items={mockDataGenerator(args.rows, args.columns)}
        columns={`repeat(${args.columns},1fr)`}
        rowComponent={rowComponent}
        headerRowComponent={headerRowComponent}
        title={args.title}
        searchParams={args.search && Object.keys(mockDataGenerator(1, 1)[0])}
        hideAddButtons={args.hideAddButtons}
        pagination={args.pagination}
      />
    </BrowserRouter>
  );
};

export const Normal = Template.bind({});

Normal.args = {
  rows: 10,
  columns: 5,
  title: "Title",
  hideAddButtons: false,
  pagination: true,
  search: true,
};
