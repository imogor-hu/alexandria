import { Meta, Story } from "@storybook/react";
import { Box } from "../../components/index.js";

export default {
  title: "Layout/Box",
  component: Box,
} as Meta;

const Template: Story = () => <Box />;

export const Normal = Template.bind({});

Normal.parameters = {
  controls: { hideNoControlsWarning: true },
};
