import { Meta, Story } from "@storybook/react";
import { Card } from "../../components/index.js";

export default {
  title: "Layout/Card",
  component: Card,
  argTypes: {
    $kind: { control: "radio", options: ["default", "error"] },
  },
} as Meta;

const Template: Story = (args) => <Card {...args}>Example text</Card>;

export const Normal = Template.bind({});

Normal.args = {
  $kind: "default",
  $padding: 10,
  $hideOverflow: false,
  $small: false,
  $extraSmall: false,
};
