import { Meta, Story } from "@storybook/react";
import { Well } from "../../components/well.js";

export default {
  title: "Layout/Well",
  component: Well,
} as Meta;

const Template: Story = () => <Well>Some content</Well>;

export const Normal = Template.bind({});

Normal.parameters = {
  controls: { hideNoControlsWarning: true },
};
