import { action } from "@storybook/addon-actions";
import { Meta, Story } from "@storybook/react";
import { ConfirmDeleteButton, ModalProvider } from "../../../components/index.js";

export default {
  title: "Grid/ConfirmDeleteButton",
  component: ConfirmDeleteButton,
  argTypes: {
    onConfirm: {
      action: "clicked",
    },
    content: {
      control: "text",
    },
    title: {
      control: "text",
    },
    component: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const Template: Story = (args) => (
  <ModalProvider>
    <ConfirmDeleteButton content={args.content} title={args.title} onConfirm={action("onConfirm")} />
  </ModalProvider>
);

export const Normal = Template.bind({});

Normal.args = {
  content: "Confirm operation?",
  title: "Confirmation title",
};
