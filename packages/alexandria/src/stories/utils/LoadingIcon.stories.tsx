import { Meta, Story } from "@storybook/react";
import { LoadingIcon } from "../../components/index.js";

export default {
  title: "Utils/LoadingIcon",
  component: LoadingIcon,
} as Meta;

const Template: Story = () => <LoadingIcon />;

export const Loading_Icon = Template.bind({});

Loading_Icon.parameters = {
  controls: { hideNoControlsWarning: true },
};
