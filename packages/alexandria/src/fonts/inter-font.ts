import { css } from "styled-components";

import interRegular from "../assets/fonts/inter/Inter-Regular.woff2";
import interMedium from "../assets/fonts/inter/Inter-Medium.woff2";
import interBold from "../assets/fonts/inter/Inter-Bold.woff2";

export const interFont = css`
  @font-face {
    font-family: "Inter";
    font-style: normal;
    font-weight: 400;
    src: url("${interRegular}") format("woff2");
  }

  @font-face {
    font-family: "Inter";
    font-style: normal;
    font-weight: 500;
    src: url("${interMedium}") format("woff2");
  }

  @font-face {
    font-family: "Inter";
    font-style: normal;
    font-weight: 700;
    src: url("${interBold}") format("woff2");
  }
`;
