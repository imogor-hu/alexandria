import "@testing-library/jest-dom";
import { vi } from "vitest";

class ResizeObserver {
  observe() {
    // do nothing
  }
  unobserve() {
    // do nothing
  }
  disconnect() {
    // do nothing
  }
}

// @ts-ignore
global.ResizeObserver = ResizeObserver;
// @ts-ignore
global.IntersectionObserver = ResizeObserver;
global.window = global.window || {};

vi.mock("@formkit/auto-animate");
