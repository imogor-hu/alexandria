import { BehaviorSubject } from "rxjs";
import { LanguageKey, TranslatedObject } from "../models/translated-object.js";
import { useObservable } from "@litbase/use-observable";

export const currentLanguage$ = new BehaviorSubject<LanguageKey>("hu");

export function setCurrentLanguage(language: LanguageKey) {
  currentLanguage$.next(language);
}

export function getTranslatableDisplayText(value?: TranslatedObject | string): string {
  return (
    (value as TranslatedObject)?.hu ||
    (value as TranslatedObject)?.en ||
    (typeof value === "string" && (value as string)) ||
    ""
  );
}

export function useCurrentLanguage() {
  const [currentLanguage] = useObservable<LanguageKey>(() => currentLanguage$, currentLanguage$.getValue());
  return currentLanguage;
}
