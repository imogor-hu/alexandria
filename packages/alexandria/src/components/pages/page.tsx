import styled from "styled-components";
import { FontWeight } from "../../themes/index.js";

export const Page = styled.div`
  padding: 1.2rem;
`;

export const PageContent = styled.div`
  background: whitesmoke;
`;

export const PageTitle = styled.h1`
  font-size: ${({ theme }) => theme.text3xl};
  font-weight: ${FontWeight.Bold};
  margin: 0;
  line-height: ${({ theme }) => theme.leading9};
`;

export const PageBar = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin: ${({ theme }) => theme.spacing4} 0;

  &:first-child {
    margin-top: 0;
  }

  &:last-child {
    margin-bottom: 0;
  }

  ${PageTitle} {
    flex-grow: 1;
  }
`;

export const PageBarButtons = styled.div`
  display: flex;
  align-items: center;

  // Can't be just Button + Button, because sometimes we need to wrap buttons in divs so Tippy works on
  // disabled buttons
  > * + * {
    margin-left: ${({ theme }) => theme.spacing3};
  }
`;
