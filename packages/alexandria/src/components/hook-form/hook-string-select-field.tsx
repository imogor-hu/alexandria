import { HookSelectField } from "./hook-select-field.js";

export function HookStringSelectField({ options, ...props }: { name: string; label: string; options: string[] }) {
  return (
    <HookSelectField
      noParse
      // @ts-ignore
      parse={(value) => {
        return { value, label: value };
      }}
      // @ts-ignore
      format={(value) => value.value}
      options={options.map((elem) => ({ value: elem, label: elem }))}
      {...props}
    />
  );
}
