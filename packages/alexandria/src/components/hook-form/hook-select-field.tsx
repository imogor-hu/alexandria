import { ReactNode, useMemo, useState } from "react";
import { identity } from "lodash-es";
import { BinaryId, DocumentInterface, MultiPromiseQueryObject, Projection } from "@litbase/core";
import { useQuery } from "@litbase/react";
import { TranslatedObject } from "../../models/translated-object.js";
import { getTranslatableDisplayText } from "../../services/translation-service.js";
import { Select, SelectProps, selectTranslations } from "../form/index.js";
import { useControllerEx } from "./use-controller-ex.js";
import { HookFieldWrapper } from "./hook-field-wrapper.js";
import { debounceTime, Observable } from "rxjs";
import { useObservable } from "@litbase/use-observable";
import { switchMap } from "rxjs/operators";

export interface HookSelectFieldProps<TOption, TOutput = TOption>
  extends Omit<SelectProps<TOption, TOutput>, "value" | "onChange" | "onBlur" | "parse" | "transform"> {
  label?: ReactNode;
  name: string;

  parse?(value: unknown): unknown;

  transform?(value: unknown): unknown;

  // Disable parse on the react-hook-form controller level
  noParse?: boolean;
}

export function HookSelectField<TOption, TOutput = TOption>({
  name,
  label,
  parse,
  transform,
  noParse = false,
  ...props
}: HookSelectFieldProps<TOption, TOutput>) {
  const {
    field: { value, onChange, onBlur },
  } = useControllerEx({ name, parse: noParse ? undefined : parse, transform: noParse ? undefined : transform });

  return (
    <HookFieldWrapper name={name} label={label}>
      {/* @ts-ignore */}
      <Select value={value} onChange={onChange} onBlur={onBlur} {...props} parse={noParse ? parse : undefined} />
    </HookFieldWrapper>
  );
}

export interface HookDocumentSelectFieldProps<TCollection extends string, TValue, TRealValue = TValue>
  extends Omit<HookSelectFieldProps<TValue, TRealValue>, "options"> {
  collection: TCollection;
  options?: Projection;
}

export function HookDocumentSelectField<TCollection extends string, TValue extends DocumentInterface>({
  options,
  getOptionLabel,
  collection,
  isLoading,
  ...props
}: HookDocumentSelectFieldProps<TCollection, TValue>) {
  const [docs, isQueryLoading] = useQuery(collection, {
    $matchAll: {},
    _id: 1,
    ...options,
  });

  return (
    <HookSelectField<TValue>
      options={docs as TValue[]}
      getOptionValue={(option: TValue) => option._id.toBase64String()}
      getOptionLabel={getOptionLabel}
      parse={(entity: null | TValue) => (entity ? entity : null)}
      format={(value: TValue | null) => (value ? value : [])}
      isLoading={isLoading || isQueryLoading}
      {...props}
    />
  );
}

export function HookDocumentSelectFieldBase<TCollection extends string, TValue extends Record<string, unknown>>({
  options,
  getOptionLabel,
  getId,
  fetcher,
  debounce = 250,
  ...props
}: Omit<HookDocumentSelectFieldProps<TCollection, TValue>, "collection"> & {
  fetcher: (query: string) => Promise<TValue[]> | Observable<TValue[]>;
  getId: (elem: TValue) => string;
  debounce?: number;
}) {
  const [inputValue, setInputValue] = useState("");
  const [docs, isLoading] = useObservable(
    (inputs$) => {
      return inputs$.pipe(
        debounceTime(debounce),
        switchMap(([query]) => fetcher(query))
      );
    },
    [],
    [inputValue]
  );

  return (
    <HookSelectField<TValue>
      options={docs as TValue[]}
      getOptionValue={getId}
      getOptionLabel={getOptionLabel}
      noParse
      parse={(id: null | TValue) => {
        // @ts-ignore
        return id === null ? null : docs.find((elem) => getId(elem)?.equals?.(id) || getId(elem) == id);
      }}
      // @ts-ignore
      format={(value: TValue | null) => {
        return value ? getId(value) : null;
      }}
      isLoading={isLoading}
      inputValue={inputValue}
      onInputChange={setInputValue}
      {...props}
    />
  );
}

export type HookMultipleDocumentSelectFieldProps<TCollection extends string, TValue> = HookDocumentSelectFieldProps<
  TCollection,
  TValue,
  string
>;

export function HookMultipleDocumentSelectField<TCollection extends string, TValue extends DocumentInterface>({
  options,
  collection,
  ...props
}: HookMultipleDocumentSelectFieldProps<TCollection, TValue>) {
  const [docs, isLoading] = useQuery<TValue>(collection, {
    $matchAll: {},
    _id: 1,
    ...options,
  } as MultiPromiseQueryObject);

  const sortedDocs = useMemo(() => {
    return [...docs].sort((a, b) => {
      return getLabel(a as string | LabeledObject).localeCompare(getLabel(b as string | LabeledObject));
    });
  }, [docs]);

  return (
    <HookSelectField<TValue, string>
      {...(isLoading ? { placeholder: selectTranslations.loading } : {})}
      options={sortedDocs}
      getOptionValue={(option: TValue) => new BinaryId(option._id.value).toBase64String()}
      parse={(ids: string[]) => {
        if (null == ids) {
          return [];
        }

        if (!Array.isArray(ids)) {
          ids = [ids];
        }

        return ids.map((id) => docs.find((doc) => doc._id.toBase64String() === id)).filter(identity);
      }}
      format={(value) => {
        if (Array.isArray(value)) {
          return value.map((v) => new BinaryId(v._id.value).toBase64String());
        }

        if (null != value) {
          return [new BinaryId(value._id.value).toBase64String()];
        }

        return [];
      }}
      isLoading={props.isLoading || isLoading}
      isMulti
      {...props}
    />
  );
}

interface LabeledObject {
  name?: string | TranslatedObject;
  label?: string | TranslatedObject;
  title?: string | TranslatedObject;
}

function getLabel<TValue>(elem: string | LabeledObject) {
  if (typeof elem === "string") {
    return elem;
  }

  const label = elem.name || elem.label || elem.title;

  return getTranslatableDisplayText(label);
}
