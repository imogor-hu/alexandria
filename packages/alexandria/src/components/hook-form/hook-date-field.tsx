import { ComponentPropsWithoutRef, ReactNode } from "react";
import { HookFieldWrapper } from "./hook-field-wrapper.js";
import { useControllerEx } from "./use-controller-ex.js";
import { DateInput } from "../form/index.js";

export interface HookDateFieldProps<TInterim> extends ComponentPropsWithoutRef<"input"> {
  name: string;
  label?: ReactNode;
  parse?: (value: string | null) => TInterim;
  transform?: (value: TInterim) => string | null;
}

export function HookDateField<TInterim = unknown>({
  name,
  label,
  parse,
  transform,
  disabled,
}: HookDateFieldProps<TInterim>) {
  const {
    field: { value, onChange, onBlur, ref },
    fieldState: { error },
  } = useControllerEx({ name, parse, transform });

  return (
    <HookFieldWrapper name={name} label={label}>
      <DateInput
        hasError={!!error}
        value={value ? new Date(value) : undefined}
        onChange={(event) => onChange(event.target.value.toISOString())}
        onBlur={onBlur}
        ref={ref}
        disabled={disabled}
      />
    </HookFieldWrapper>
  );
}
