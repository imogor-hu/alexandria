import TextArea from "react-textarea-autosize";
import styled from "styled-components";
import { useController } from "react-hook-form";
import { ComponentProps } from "react";
import { HookFieldWrapper } from "./hook-field-wrapper.js";
import { useUniqueId } from "../form/index.js";

export function HookTextareaField({
  name,
  label,
  disabled = false,
  ...props
}: { name: string; label: string; disabled?: boolean } & ComponentProps<typeof StyledTextarea>) {
  const controller = useController({ name });
  const fieldId = useUniqueId();
  return (
    <HookFieldWrapper id={fieldId} name={name} label={label} $fullWidth>
      <StyledTextarea
        disabled={disabled}
        minRows="5"
        placeholder={disabled ? "" : props.placeholder}
        value={controller.field.value}
        onChange={controller.field.onChange}
        id={fieldId}
        {...props}
      />
    </HookFieldWrapper>
  );
}

const StyledTextarea = styled(TextArea)`
  padding: ${({ theme }) => theme.spacing3};
  max-width: ${({ theme }) => theme.fieldMaxWidth5Xl};
  width: 100%;
  border-radius: ${({ theme }) => theme.rounded};
  font-family: ${({ theme }) => theme.fontFamilyBase};
  border: 1px solid ${({ theme }) => theme.gray300};

  ::placeholder {
    font-size: ${({ theme }) => theme.textBase};
  }
`;
