import styled from "styled-components";
import { RegisterOptions, useFormContext } from "react-hook-form";
import { ComponentPropsWithoutRef, ReactNode } from "react";
import { HookFieldWrapper, HookFieldWrapperProps } from "./hook-field-wrapper.js";
import { TextInput, useUniqueId } from "../form/index.js";

export interface HookInputFieldProps extends ComponentPropsWithoutRef<"input"> {
  name: string;
  label?: ReactNode;
  registerOptions?: RegisterOptions;
  wrapperProps?: Partial<HookFieldWrapperProps>;
}

export function HookInputField({ name, label, registerOptions, wrapperProps, ...props }: HookInputFieldProps) {
  const {
    register,
    formState: { errors },
  } = useFormContext();
  const hasError = !!errors[name];

  const fieldId = useUniqueId();

  return (
    <HookFieldWrapper {...wrapperProps} id={fieldId} name={name} label={label}>
      <InputFieldContainer>
        {/* without key, TextInput will not react to name change for some reason */}
        <TextInput id={fieldId} key={name} hasError={hasError} {...props} {...register(name, registerOptions)} />
      </InputFieldContainer>
    </HookFieldWrapper>
  );
}

const InputFieldContainer = styled.div`
  position: relative;
`;
