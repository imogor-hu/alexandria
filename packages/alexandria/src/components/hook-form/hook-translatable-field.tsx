import { Children, cloneElement, ReactElement, ReactNode, Fragment } from "react";
import { currentLanguage$ } from "../../services/translation-service.js";
import { useObservable } from "@litbase/use-observable";
import { FlagIcon } from "../form/index.js";

export function HookTranslatableField({ children }: { children: ReactElement<{ name: string; label: string }> }) {
  const child: ReactElement<{ name: string; label: ReactNode }> = Children.only(children);
  const [currentLanguage] = useObservable(() => currentLanguage$, "hu");

  // We use fragment, to trigger a rerender of the field component. React-hook-form doesn't properly react to changes
  // in name, post-initialization, so we need to trigger a complete rerender
  return (
    <Fragment key={currentLanguage}>
      {cloneElement(child, {
        name: `${child.props.name}.${currentLanguage}`,
        label: (
          <>
            {child.props.label} <FlagIcon language={currentLanguage} />
          </>
        ),
      })}
    </Fragment>
  );
}
