import { ComponentPropsWithoutRef, ReactNode } from "react";
import styled from "styled-components";
import { useFormContext } from "react-hook-form";
import { HookFieldWrapper } from "./hook-field-wrapper.js";
import { FieldLabel, useUniqueId } from "../form/index.js";
import { FontWeight, transition } from "../../themes/index.js";

export interface HookCheckboxFieldProps extends ComponentPropsWithoutRef<"input"> {
  name: string;
  checkboxLabel?: ReactNode;
}

export function HookCheckboxField({ name, checkboxLabel, ...props }: HookCheckboxFieldProps) {
  const { register } = useFormContext();
  const id = useUniqueId("checkbox_");

  return (
    <HookFieldWrapper name={name}>
      <StyledCheckboxField>
        <StyledCheckbox type="checkbox" {...props} id={id} {...register(name)} />
        {checkboxLabel && <HookCheckboxFieldLabel htmlFor={id}>{checkboxLabel}</HookCheckboxFieldLabel>}
      </StyledCheckboxField>
    </HookFieldWrapper>
  );
}

const StyledCheckboxField = styled.div`
  display: flex;
  align-items: center;
`;

const StyledCheckbox = styled.input`
  appearance: none;
  display: inline-block;
  width: 1rem;
  height: 1rem;
  color: ${({ theme }) => theme.primary500};
  color-adjust: exact;
  border: 1px solid ${({ theme }) => theme.gray300};
  border-radius: ${({ theme }) => theme.rounded};
  transition: ${({ theme }) => transition(["background-color", "border-color"], theme.defaultTransitionTiming)};

  &:checked {
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg viewBox='0 0 16 16' fill='%23fff' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M5.707 7.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4a1 1 0 00-1.414-1.414L7 8.586 5.707 7.293z'/%3E%3C/svg%3E");
    border-color: transparent;
    background-color: currentColor;
    background-size: 100% 100%;
    background-position: 50%;
    background-repeat: no-repeat;
  }
}
`;

const HookCheckboxFieldLabel = styled(FieldLabel)`
  display: inline-block;
  margin: 0 ${({ theme }) => theme.spacing2};
  font-weight: ${FontWeight.Regular};
`;
