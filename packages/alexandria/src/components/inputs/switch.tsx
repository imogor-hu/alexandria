import { FieldProps } from "formik";
import { spacing1 } from "../../themes/index.js";
import styled from "styled-components";
import { ComponentProps } from "react";

export interface SwitchProps extends Omit<ComponentProps<"input">, "ref" | "value" | "onChange"> {
  value: boolean;
  onChange: (value: boolean) => void;
  className?: string;
}

export function Switch({ value, onChange, className, id, ...props }: SwitchProps) {
  return (
    <StyledSwitch className={className} $isActive={value} onClick={() => onChange(!value)} {...props}>
      <input type="checkbox" hidden id={id} checked={value} value={value ? "1" : "0"} readOnly />
      <Placeholder $isActive={value} />
      <Puck />
    </StyledSwitch>
  );
}

export function SwitchField({ field, form, className }: FieldProps<boolean> & { className?: string }) {
  return (
    <Switch className={className} value={field.value} onChange={(value) => form.setFieldValue(field.name, value)} />
  );
}

const Placeholder = styled.div<{ $isActive: boolean }>`
  width: ${({ $isActive }) => ($isActive ? 100 : 0)}%;
  transition: width ${({ theme }) => theme.defaultTransitionTiming};
`;

const Puck = styled.div`
  min-width: ${({ theme }) => theme.textLg};
  height: ${({ theme }) => theme.textLg};
  border-radius: 50%;
  background: ${({ theme }) => theme.gray50};
  box-shadow: ${({ theme }) => theme.shadow};
`;

export const StyledSwitch = styled.div<{ $isActive: boolean }>`
  background-color: ${({ theme, $isActive }) => ($isActive ? theme.primary400 : theme.gray400)};
  display: flex;
  height: fit-content;
  border-radius: ${({ theme }) => theme.roundedFull};
  padding: ${spacing1};
  width: ${({ theme }) => theme.spacing12};
  transition: background 0.5s;
  cursor: pointer;
`;
