import { ComponentPropsWithoutRef, ForwardedRef, forwardRef, ForwardRefRenderFunction, ReactNode } from "react";
import { FieldProps } from "formik";
import { ErrorCircle } from "@styled-icons/boxicons-solid";
import { useFormikWrapperContext } from "./formik-wrapper.js";
import { getSelfAndAncestorFields } from "../../utils/index.js";
import styled from "styled-components";
import { transition } from "../../themes/index.js";

export interface InputFieldProps extends FieldProps<string> {
  type?: string;
  placeholder?: string;
}

export function InputField({ field, form, ...rest }: InputFieldProps) {
  const { errors, touched } = useFormikWrapperContext((context) => {
    const fieldNames = getSelfAndAncestorFields(field.name);

    return [
      ...fieldNames.map((fieldName) => context.errors[fieldName as keyof typeof context.errors]),
      ...fieldNames.map((fieldName) => context.touched[fieldName as keyof typeof context.touched]),
    ];
  });

  const hasError = getSelfAndAncestorFields(field.name).some(
    (fieldName) => !!errors[fieldName as keyof typeof errors] && !!touched[fieldName as keyof typeof touched]
  );

  return <TextInput hasError={hasError} {...field} {...rest} />;
}

export interface TextInputProps extends ComponentPropsWithoutRef<"input"> {
  hasError?: boolean;
  children?: ReactNode | undefined;
}

function TextInputComponent({ hasError, ...props }: TextInputProps, ref: ForwardedRef<HTMLInputElement>) {
  return (
    <InputFieldContainer>
      <Input {...props} $hasError={hasError} ref={ref} />
      {hasError && <InputFieldErrorSymbol />}
    </InputFieldContainer>
  );
}

export const TextInput = forwardRef(TextInputComponent as ForwardRefRenderFunction<HTMLInputElement, TextInputProps>);

const InputFieldContainer = styled.div`
  position: relative;
`;

export const Input = styled.input<{ $hasError?: boolean }>`
  border-radius: ${({ theme }) => theme.roundedMd};
  padding: ${({ theme }) => theme.spacing2} ${({ theme }) => theme.spacing3};
  border: 1px solid ${({ $hasError, theme }) => (!$hasError ? theme.gray300 : theme.red300)};
  font-size: ${({ theme }) => theme.textSm};
  display: block;
  width: 100%;
  transition: ${({ theme }) =>
    transition(["background-color", "border-color", "color"], theme.defaultTransitionTiming)};

  &:disabled {
    background-color: ${({ theme }) => theme.gray50};
    color: ${({ theme }) => theme.gray400};
    cursor: not-allowed;
  }

  &::placeholder {
    color: ${({ theme }) => theme.gray500};
  }
`;

const InputFieldErrorSymbol = styled(ErrorCircle)`
  position: absolute;
  right: ${({ theme }) => theme.spacing2};
  top: 50%;
  transform: translateY(-50%);
  color: ${({ theme }) => theme.red500};
  font-size: ${({ theme }) => theme.text2xl};
`;
