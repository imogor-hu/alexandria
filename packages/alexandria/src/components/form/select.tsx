import { FieldProps } from "formik";
import ReactSelect, { CreatableProps } from "react-select/creatable";
import { identity, noop } from "lodash-es";
import { GroupBase, InputActionMeta } from "react-select";
import { Medium } from "../../themes/index.js";
import { ReactNode } from "react";

export const selectTranslations = {
  placeholder: "None" as ReactNode,
  noOptionsMessage: "No options founds" as ReactNode,
  loading: "Loading..." as ReactNode,
  formatCreateLabel: (input: string) =>
    (
      <>
        create <Medium>{input}</Medium>...
      </>
    ) as ReactNode,
};

export interface SelectProps<TOption, TOutput = TOption>
  extends Omit<
    CreatableProps<TOption, true, GroupBase<TOption>>,
    "value" | "onChange" | "getOptionValue" | "getOptionLabel"
  > {
  value?: TOutput | TOutput[] | null;

  onChange?(value: TOutput | TOutput[] | null): void;

  parse?(realValue: TOutput | TOutput[] | null): TOption | TOption[] | null;

  format?(value: TOption | TOption[] | null): TOutput | TOutput[] | null;

  disabled?: boolean;
  isCreatable?: boolean;
  inputValue?: string;
  onInputChange?: (newInputValue: string, meta: InputActionMeta) => void;

  getOptionValue?(value: TOption): string;

  getOptionLabel?(value: TOption): string;

  options?: TOption[];
}

export function Select<TOption, TOutput = TOption>({
  onChange = noop,
  value = null,
  parse = identity,
  format = identity,
  disabled,
  isCreatable,
  styles,
  ...props
}: SelectProps<TOption, TOutput>) {
  return (
    <ReactSelect<TOption, true, GroupBase<TOption>>
      onChange={(newValue) => onChange(format(newValue as TOption | TOption[] | null))}
      styles={{
        menu: (provided) => ({ ...provided, zIndex: 21 }),
        ...styles,
      }}
      value={parse(value)}
      placeholder={selectTranslations.placeholder}
      noOptionsMessage={() => selectTranslations.noOptionsMessage}
      isDisabled={disabled}
      isValidNewOption={(input) => !!isCreatable && !!input.trim()}
      formatCreateLabel={selectTranslations.formatCreateLabel}
      {...props}
    />
  );
}

export interface SelectFieldProps<TOption, TOutput = TOption>
  extends FieldProps<TOutput | null>,
    Omit<SelectProps<TOption, TOutput>, "form" | "value" | "onChange" | "onBlur"> {}

export function SelectField<TOption, TOutput = TOption>({
  field,
  form,
  meta,
  ...props
}: SelectFieldProps<TOption, TOutput>) {
  return (
    <Select<TOption, TOutput>
      value={field.value}
      onChange={(newValue) => form.setFieldValue(field.name, newValue)}
      onBlur={field.onBlur}
      {...props}
    />
  );
}
