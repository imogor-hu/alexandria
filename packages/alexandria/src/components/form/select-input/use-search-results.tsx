import { from, map, Observable, of, switchMap } from "rxjs";
import { useState } from "react";
import { useObservable } from "@litbase/use-observable";
import { distinctUntilChanged, throttleTime } from "rxjs/operators";
import { getValueOf } from "@litbase/core";
import { useRefValue } from "../../../hooks/index.js";

export interface UseSearchResultsOptions<T> {
  searchText: string;
  options?: T[];
  optionsCallback?: (value: string) => Observable<T[]> | Promise<T[]> | T[];
  throttleMs?: number;
  throttleLeading?: boolean;
}

export function useSearchResults<T>({
  searchText,
  options,
  optionsCallback,
  throttleMs = 250,
  throttleLeading = false,
}: UseSearchResultsOptions<T>) {
  const [isLoading, setIsLoading] = useState(false);
  const optionsCallbackRef = useRefValue(optionsCallback);
  const [results] = useObservable(
    (inputs$) => {
      let updateIndex = 0;

      return inputs$.pipe(
        map(([value]) => value.trim()),
        distinctUntilChanged(),
        map((value, index) => {
          updateIndex++;
          setIsLoading(true);
          return { value, index };
        }),
        throttleMs ? throttleTime(throttleMs, undefined, { leading: throttleLeading, trailing: true }) : map((x) => x),
        switchMap(({ value, index }) => {
          let sourceObservable: Observable<T[]>;
          const optionsCallback = optionsCallbackRef.current;

          if (options) {
            sourceObservable = of(simpleFilter(value, options));
          } else if (optionsCallback) {
            const callbackValue = optionsCallback(value);
            sourceObservable = Array.isArray(callbackValue) ? of(callbackValue) : from(callbackValue);
          } else {
            sourceObservable = of([]);
          }

          return sourceObservable.pipe(map((result) => ({ result, index })));
        }),
        map(({ result, index }) => {
          if (updateIndex <= index) {
            setIsLoading(false);
          }

          return result;
        })
      );
    },
    [] as T[],
    [searchText]
  );

  return [results, isLoading] as const;
}

function simpleFilter<T>(searchText: string, options: T[]) {
  const simplifiedSearchText = searchText.trim().toLowerCase();
  return options.filter((v) => String(getValueOf(v)).toLowerCase().includes(simplifiedSearchText));
}
