import { useField } from "formik";
import styled from "styled-components";
import TextareaAutosize from "react-textarea-autosize";
import { transition } from "../../themes/index.js";

interface TextInputProps {
  name: string;
  label?: string;
  multiline?: boolean;
  minRows?: number;
}

export function TextField({ label, multiline, minRows, ...props }: TextInputProps) {
  const [field, { error, touched }] = useField(props);

  return (
    <TextFieldWrapper hasError={!!error}>
      <TextFieldInputWrapper>
        {/* placeholder is needed for the :placeholder-show selector to work */}
        {multiline ? (
          <TextFieldInput as={TextareaAutosize as any} {...field} minRows={minRows} placeholder=" " />
        ) : (
          <TextFieldInput {...field} placeholder=" " />
        )}
        {/* Needs to be after the input for the adjacent selector to work */}
        {label && <TextFieldLabel>{label}</TextFieldLabel>}
      </TextFieldInputWrapper>
      {touched && error && <TextFieldError>{error}</TextFieldError>}
    </TextFieldWrapper>
  );
}

const TextFieldInput = styled.input`
  display: block;
  background: none;
  border: none;
  outline: 0;
  width: 100%;
  padding: ${(props) => props.theme.spacing2} ${(props) => props.theme.spacing4};
  color: #fff;

  transition: ${(props) => transition(["background-color"], props.theme.defaultTransitionTiming)};
`;

const TextFieldLabel = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  transform: translateY(1.6rem) scale(1);
  text-decoration: none;
  white-space: nowrap;
  transform-origin: top left;
  padding-left: 1.3rem; // Offset a bit so it starts at the same point as the input
  color: #c8c8c8;
  transition: ${(props) => transition(["transform", "color"], props.theme.defaultTransitionTiming)};

  ${TextFieldInput}:focus + &,
  ${TextFieldInput}:not(:placeholder-shown) + & {
    transform: translateY(0.7rem) scale(0.75);
  }
`;

const TextFieldError = styled.div`
  margin-top: ${(props) => props.theme.spacing4};
  border-radius: ${(props) => props.theme.defaultBorderRadius};
  background-color: ${(props) => props.theme.danger900};
  border: 1px solid ${(props) => props.theme.danger700};
  padding: ${(props) => props.theme.spacing2} ${(props) => props.theme.spacing3};
  font-size: ${(props) => props.theme.textSm};
`;

const TextFieldInputWrapper = styled.span`
  position: relative;
  display: block;
  background: #454545;
  border-radius: ${(props) => props.theme.defaultBorderRadius} ${(props) => props.theme.defaultBorderRadius} 0 0;
  padding-top: ${(props) => props.theme.spacing8}; /* Leave space for label */
  cursor: text;
  transition: ${(props) => transition(["background-color"], props.theme.defaultTransitionTiming)};

  &:before {
    content: "";
    display: block;
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
    border-bottom: 1px solid #c8c8c8;

    transition: ${(props) => transition(["border-color"], props.theme.defaultTransitionTiming)};
  }

  &:after {
    content: "";
    display: block;
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
    transform: scaleX(0);
    border-bottom: 0.2rem solid #a6d4fa;

    transition: ${(props) => transition(["transform"], props.theme.defaultTransitionTiming)};
  }

  &:hover:not(:focus-within) {
    background: #4d4d4d;

    &:before {
      border-bottom-color: #ffffff;
    }
  }

  &:focus-within {
    ${TextFieldLabel} {
      color: #a6d4fa;
    }

    &:after {
      transform: scaleX(1);
    }
  }
`;

export const TextFieldWrapper = styled.label<{ hasError: boolean }>`
  display: block;
`;
