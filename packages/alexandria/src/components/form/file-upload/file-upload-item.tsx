import { MouseEvent, ReactElement, ReactNode, useMemo } from "react";
import { Error, Trash } from "@styled-icons/boxicons-solid";
import { CheckCircle, Plus, Search } from "@styled-icons/boxicons-regular";
import { FileEntry } from "./file-entry.js";
import Tippy from "@tippyjs/react";
import { animated } from "react-spring";
import { StyledIconBase } from "@styled-icons/styled-icon";
import { FileUploadItemPreview } from "./file-upload-item-preview.js";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import {
  roundedMd,
  roundedSm,
  size40,
  spacing2,
  spacing6,
  transition,
  zIndex10,
  zIndex20,
} from "../../../themes/index.js";
import styled from "styled-components";
import { useStatusTransition } from "../../../hooks/use-status-transition.js";
import { Button } from "../../inputs/button.js";
import { getDisplayableErrorMessage } from "../../../utils/index.js";
import { AspectRatio } from "../../utils/aspect-ratio.js";

export const fromStyle = {
  opacity: 0,
  transform: `translate(-50%, -50%) translateY(${spacing6})`,
};

export const leaveStyle = {
  opacity: 0,
  transform: `translate(-50%, -50%) translateY(-${spacing6})`,
};

export const enterStyle = {
  opacity: 1,
  transform: `translate(-50%, -50%) translateY(0rem)`,
};

export interface FileUploadItemProps {
  file: FileEntry;
  preview?: ({ file }: { file: FileEntry }) => ReactElement | null;

  onDeleteClick(file: FileEntry): void;

  ratio?: string | number;
  extraButtons?: (fileEntry: FileEntry) => ReactNode;
}

/**
 * Component that show file upload progress, preview, and exposes controls on the file (Delete, open, etc.)
 * */
export function FileUploadItem({
  file,
  onDeleteClick,
  preview: Preview = FileUploadItemPreview,
  ratio = "10/6",
  extraButtons,
}: FileUploadItemProps) {
  const { attributes, listeners, setNodeRef, transform, transition } = useSortable({
    id: file.entryId,
  });

  function onDeleteButtonClick(event: MouseEvent<HTMLDivElement>) {
    event.preventDefault();
    event.stopPropagation();
    onDeleteClick(file);
  }

  return (
    <StyledFileUploadItem
      $uploading={file.status === "working"}
      // @ts-ignore
      onClick={stopClickPropagation}
      ratio={ratio}
      ref={setNodeRef}
      style={{ transform: CSS.Transform.toString(transform), transition }}
      {...attributes}
      {...listeners}
    >
      <div onMouseDown={onWrapperMouseDown}>
        <Preview file={file} />
        {extraButtons && extraButtons(file)}
        <FileUploadItemDeleteButton onClick={onDeleteButtonClick}>
          <Trash />
        </FileUploadItemDeleteButton>
        <FileUploadStatus file={file} />
        <FileUploadItemOpenButton file={file} />
      </div>
    </StyledFileUploadItem>
  );
}

function onWrapperMouseDown(event: MouseEvent<HTMLDivElement>) {
  // If the clicked element is a button or a element, prevent it from triggering any dragging
  const target = event.target as HTMLElement;

  if (target.closest("button") || target.closest("a")) {
    event.stopPropagation();
  }
}

function FileUploadStatus({ file }: { file: FileEntry }) {
  const transitions = useStatusTransition({ status: file.status, fromStyle, enterStyle, leaveStyle });

  return (
    <>
      {transitions((style, status) => {
        switch (status) {
          case "working":
            return (
              <FileUploadItemProgress style={style}>
                <FileUploadItemProgressBar style={{ width: Math.floor(file.progress || 0) + "%" }} />
              </FileUploadItemProgress>
            );
          case "errored":
            return (
              <FileUploadItemError style={style}>
                <Tippy content={getDisplayableErrorMessage(file.error)}>
                  <Error />
                </Tippy>
              </FileUploadItemError>
            );
          default:
            return (
              <FileUploadItemSuccess style={style}>
                <CheckCircle />
              </FileUploadItemSuccess>
            );
        }
      })}
    </>
  );
}

function FileUploadItemOpenButton({ file }: { file: FileEntry }) {
  const url = useMemo(() => {
    return file.preview || file.contentUrl;
  }, [file]);

  return (
    <StyledFileUploadItemOpenButton as="a" target="_blank" hidden={file.status !== "done"} href={url}>
      <Search />
    </StyledFileUploadItemOpenButton>
  );
}

export function AddFileUploadItem() {
  return (
    <StyledAddFileUploadItem ratio="10/6">
      <div>
        {/* Wrapper div for <AspectRatio> */}
        <Plus />
      </div>
    </StyledAddFileUploadItem>
  );
}

function stopClickPropagation(event: MouseEvent<HTMLDivElement>) {
  event.stopPropagation();
}

export const FileUploadItemButton = styled(Button).attrs(() => ({ $onlyIcon: true }))`
  position: absolute;
  display: flex;
  opacity: 0;
  z-index: ${zIndex20};

  transition: ${({ theme }) => transition(["opacity"], theme.defaultTransitionTiming)};
`;

const StyledFileUploadItemOpenButton = styled(FileUploadItemButton)`
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const FileUploadItemDeleteButton = styled(FileUploadItemButton).attrs(() => ({ $size: "xs", $kind: "danger" }))`
  right: 0.3rem;
  top: 0.3rem;
`;

const FileUploadItemIndicator = styled(animated.div)`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  padding: ${spacing2};
  border-radius: ${roundedMd};
  z-index: ${zIndex20};
  line-height: 1;
  box-shadow: ${({ theme }) => theme.shadowLg};

  ${StyledIconBase} {
    font-size: ${({ theme }) => theme.text3xl};
  }
`;

const FileUploadItemError = styled(FileUploadItemIndicator)`
  background-color: ${({ theme }) => theme.red500};
  color: ${({ theme }) => theme.red100};
`;

const FileUploadItemSuccess = styled(FileUploadItemIndicator)`
  background-color: ${({ theme }) => theme.green500};
  color: ${({ theme }) => theme.green100};
`;

const FileUploadItemProgress = styled(animated.div)`
  position: absolute;
  left: 50%;
  top: 50%;
  width: 85%;
  transform: translate(-50%, -50%);
  box-shadow: ${({ theme }) => theme.shadowMd};
  background: ${({ theme }) => theme.primary100};
  border: 1px solid ${({ theme }) => theme.primary50};
  border-radius: ${roundedSm};
  z-index: ${zIndex20};
  /* React spring currently can leave elements in after the animation. If this happens, don't prevent the user from
  clicking buttons */
  pointer-events: none;
`;

const FileUploadItemProgressBar = styled.div`
  height: ${({ theme }) => theme.textXs};
  background-color: ${({ theme }) => theme.primary400};
  transition: ${({ theme }) => transition(["width"], theme.defaultTransitionTiming)};
`;

export const StyledFileUploadItemBase = styled(AspectRatio)`
  width: ${size40};
  border-radius: ${({ theme }) => theme.roundedMd};
  margin: ${({ theme }) => theme.spacing2};
  position: relative;
  border: 1px solid ${({ theme }) => theme.gray200};
  box-shadow: ${({ theme }) => theme.shadowMd};
`;

export const StyledFileUploadItem = styled(StyledFileUploadItemBase)<{ $uploading: boolean }>`
  cursor: move;

  &:after {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: ${({ theme }) => theme.gray900};
    z-index: ${zIndex10};
    border-radius: ${({ theme }) => theme.roundedMd};
    opacity: ${({ $uploading }) => ($uploading ? 0.12 : 0)};
    transition: ${({ theme }) => transition(["opacity"], theme.defaultTransitionTiming)};
  }

  &:hover {
    &:before {
      opacity: 0.12;
    }

    ${FileUploadItemButton} {
      opacity: 1;
    }
  }
`;

const StyledAddFileUploadItem = styled(StyledFileUploadItemBase)`
  color: black;
  background-color: white;
  transition: ${({ theme }) => transition(["color", "background-color"], theme.defaultTransitionTiming)};
  font-size: ${({ theme }) => theme.text3xl};
  cursor: pointer;

  &:hover {
    background-color: ${({ theme }) => theme.primary400};
    color: white;
  }

  > div {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
