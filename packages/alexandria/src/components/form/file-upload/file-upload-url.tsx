import { FileUpload, FileUploadProps } from "./file-upload.js";
import { ComponentType, useRef } from "react";
import { createFileEntryFromString, FileEntry, fileEntryToString } from "./file-entry.js";
import { useAction } from "use-action";
import { useForceUpdate } from "../../../hooks/index.js";
import { identity } from "lodash-es";

export interface FileUploadUrlProps extends Omit<FileUploadProps, "value" | "onChange"> {
  value: string[] | undefined;

  onChange(value: string[]): void;

  onFilesChange?(files: FileEntry[]): void;

  onCreateFileEntryFromString?(file: FileEntry): FileEntry;

  uploadComponent?: ComponentType<FileUploadProps>;
  fileFromString?: (value: string) => FileEntry;
  fileToString?: (fileEntry: FileEntry) => string;
}

/**
 * Wraps FileUpload to be usable with string[]
 * (Converts string[] value to FileEntry[] that's usable by FileUpload)
 * */
export function FileUploadUrl({
  value,
  onChange,
  onFilesChange,
  onCreateFileEntryFromString = identity,
  uploadComponent: UploadComponent = FileUpload,
  fileFromString = createFileEntryFromString,
  fileToString = fileEntryToString,
  ...props
}: FileUploadUrlProps) {
  value = value || [];

  const forceUpdate = useForceUpdate();
  const filesRef = useRef([] as FileEntry[]);

  function setFiles(files: FileEntry[], defer = false) {
    filesRef.current = files;

    if (defer) {
      Promise.resolve().then(() => onFilesChange && onFilesChange(files));
    } else {
      onFilesChange && onFilesChange(files);
    }
  }

  useAction(() => {
    setFiles(
      (value || []).map(
        (value) =>
          filesRef.current.find((file) => file.contentUrl === value || file.entryId === value) ||
          onCreateFileEntryFromString(fileFromString(value))
      ),
      true
    );
  }, [value.length ? value : ""]);

  return (
    <UploadComponent
      value={filesRef.current}
      onChange={(newEntries) => {
        setFiles(newEntries);

        if (shouldSetFieldValue(value || [], newEntries)) {
          // No need to forceUpdate as the parent component will do that for us
          onChange(newEntries.map((entry) => fileToString(entry)));
        } else {
          forceUpdate();
        }
      }}
      {...props}
    />
  );
}

function shouldSetFieldValue(value: string[], files: FileEntry[]) {
  if (value.length !== files.length) return true;

  for (let i = 0; i < value.length; i++) {
    if (value[i] !== fileEntryToString(files[i])) return true;
  }

  return false;
}
