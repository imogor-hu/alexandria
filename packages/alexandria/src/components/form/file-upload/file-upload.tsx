import {
  ComponentProps,
  CSSProperties,
  MutableRefObject,
  ReactElement,
  ReactNode,
  useCallback,
  useEffect,
  useRef,
} from "react";
import { Accept, useDropzone } from "react-dropzone";
import { from, ObservableInput, Subject } from "rxjs";
import { AddFileUploadItem, FileUploadItem } from "./file-upload-item.js";
import { noop } from "lodash-es";
import { createFileEntryFromFile, FileEntry } from "./file-entry.js";
import useConstant from "use-constant";
import { filter, takeUntil } from "rxjs/operators";
import { closestCenter, DndContext, DragEndEvent, MouseSensor, useSensor, useSensors } from "@dnd-kit/core";
import { restrictToWindowEdges } from "@dnd-kit/modifiers";
import { rectSortingStrategy, SortableContext } from "@dnd-kit/sortable";
import { moveItem } from "../../../utils/util-functions.js";
import { Well } from "../../well.js";
import styled from "styled-components";

export interface FileUploadProps extends Omit<ComponentProps<"input">, "accept" | "onChange" | "value" | "form"> {
  value: FileEntry[];

  onChange(value: FileEntry[]): void;

  onUploadFile?(file: FileEntry): ObservableInput<FileEntry>;

  onDeleteFile?(file: FileEntry): void;

  limit?: number;
  className?: string;
  style?: CSSProperties;
  accept?: Accept;
  disabled?: boolean;
  preview?: ({ file }: { file: FileEntry }) => ReactElement | null;
  ratio?: string | number;
  extraButtons?: (fileEntry: FileEntry) => ReactNode;
}

const dragModifiers = [restrictToWindowEdges];

/**
 * Component for uploading files. Uses a Well to accept drag & drop and an <input /> for file manager dialog upload.
 * Works with FileEntity[] values
 * */
export function FileUpload({
  value = [],
  limit = Number.POSITIVE_INFINITY,
  className,
  style,
  accept,
  preview,
  ratio,
  extraButtons,
  onChange = noop,
  onUploadFile,
  ...props
}: FileUploadProps) {
  const propsRef = useRef({ ...props, onChange, value, onUploadFile });
  propsRef.current = { ...props, onChange, value, onUploadFile };

  const disabled = props.disabled || limit <= value.length;
  const cancelSubject = useConstant(() => new Subject<FileEntry | null>());
  const onDeleteClick = useOnDeleteClick(propsRef, cancelSubject);
  const onDrop = useDrop(propsRef, cancelSubject);

  useEffect(() => () => cancelSubject.next(null), []);

  const multiple = 1 < limit;
  const { getRootProps, getInputProps } = useDropzone({
    accept,
    disabled,
    multiple,
    onDrop,
  });

  const sensors = useSensors(useSensor(MouseSensor));

  function onDragEnd({ active, over }: DragEndEvent) {
    if (!over || active.id === over.id) return;

    // Find the item being moved
    const fromIndex = value.findIndex((item) => item.entryId === active.id);
    // Find the item being moved over to
    const toIndex = value.findIndex((item) => item.entryId === over.id);

    onChange(moveItem(value, fromIndex, toIndex));
  }

  return (
    <DndContext sensors={sensors} collisionDetection={closestCenter} modifiers={dragModifiers} onDragEnd={onDragEnd}>
      <SortableContext items={value.map((item) => item.entryId)} strategy={rectSortingStrategy}>
        <StyledFileUpload $disabled={disabled} $multiple={multiple} {...getRootProps({ className, style })}>
          <input {...props} {...getInputProps()} />
          {value.map((file) => (
            <FileUploadItem
              key={file.entryId}
              file={file}
              preview={preview}
              onDeleteClick={onDeleteClick}
              ratio={ratio}
              extraButtons={extraButtons}
            />
          ))}
          {value.length < limit && <AddFileUploadItem />}
        </StyledFileUpload>
      </SortableContext>
    </DndContext>
  );
}

function useOnDeleteClick(propsRef: MutableRefObject<FileUploadProps>, cancelSubject: Subject<FileEntry | null>) {
  return useCallback((file: FileEntry) => {
    const { onDeleteFile = noop, value, onChange } = propsRef.current;

    onDeleteFile(file);

    const foundIndex = value.indexOf(file);

    if (foundIndex < 0) return;

    const newFiles = [...value];
    newFiles.splice(foundIndex, 1);

    onChange(newFiles);

    cancelSubject.next(file);
  }, []);
}

function useDrop(propsRef: MutableRefObject<FileUploadProps>, cancelSubject: Subject<FileEntry | null>) {
  return useCallback((acceptedFiles: File[]) => {
    const limit = propsRef.current.limit;
    const filesToAllow = limit ? limit - propsRef.current.value.length : Number.MAX_VALUE;

    if (0 === filesToAllow) return;

    if (filesToAllow < acceptedFiles.length) acceptedFiles = acceptedFiles.slice(0, filesToAllow);

    const uploads = acceptedFiles.map(createFileEntryFromFile);

    propsRef.current.onChange([...propsRef.current.value, ...uploads]);

    if (propsRef.current.onUploadFile) {
      for (const upload of uploads) {
        from(propsRef.current.onUploadFile(upload))
          .pipe(takeUntil(cancelSubject.pipe(filter((file) => file === null || file.entryId === upload.entryId))))
          .subscribe((file: FileEntry) => {
            const files = propsRef.current.value;

            const foundIndex = files.findIndex((f: FileEntry) => f.entryId === file.entryId);

            const newFiles = [...files];
            if (foundIndex < 0) {
              newFiles.push(file);
            } else {
              newFiles[foundIndex] = file;
            }

            propsRef.current.value = newFiles;
            propsRef.current.onChange(newFiles);
          });
      }
    }
  }, []);
}

const StyledFileUpload = styled(Well)<{ $multiple: boolean; $disabled: boolean }>`
  width: ${({ $multiple }) => ($multiple ? "100%" : "auto")};
  display: ${({ $multiple }) => ($multiple ? "flex" : "inline-flex")};
  flex-wrap: wrap;
`;

/*
 * Upload thumbnail like this: Send form data to api/stored-files, with the usual file property,
 * as well as a thumbnailOf iri, and sizeName string
 * */
