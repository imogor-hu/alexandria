import { TransitionStatus } from "../../../hooks/use-status-transition.js";
import { uniqueId } from "lodash-es";

export type FileEntryStatus = "uploading" | "uploaded" | "errored";

export interface ThumbnailOptions {
  /** The display width and height of the image; The <img /> tag will be this many px wide and high. */
  width: number;
  height: number;
  /** Manual crop options, with which we can specify a top left and bottom right corner on the original image. */
  cropWidth?: number;
  cropHeight?: number;
  x?: number;
  y?: number;
}

export interface FileEntry {
  entryId: string;
  status: TransitionStatus;
  file?: File;
  error?: Error;
  progress?: number;
  preview?: string;
  contentUrl?: string;
}

export function fileEntryToString(entry: FileEntry) {
  return entry.contentUrl || entry.entryId;
}

export function nextFileEntryId() {
  return uniqueId("uploading://");
}

export function createFileEntryFromFile(file: File): FileEntry {
  return {
    entryId: nextFileEntryId(),
    file: file,
    status: "working",
    progress: 0,
  };
}

export function createFileEntryFromString(value: string): FileEntry {
  return {
    entryId: nextFileEntryId(),
    contentUrl: value,
    status: "done",
  };
}
