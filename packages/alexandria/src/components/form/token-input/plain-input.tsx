import { InputToken, TokenInputManager } from "./token-input-manager.js";
import { FormEvent, useEffect, useRef } from "react";
import styled from "styled-components";

export interface PlainInputProps {
  value: InputToken;
  onChange: (newValue: InputToken) => void;
  manager: TokenInputManager;
}

export function PlainInput({ value, onChange, manager }: PlainInputProps) {
  const ref = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (ref.current && ref.current?.textContent !== value.text) {
      ref.current.textContent = value.text;
    }
  }, [value.text]);

  return (
    <StyledPlainInput
      ref={ref}
      contentEditable
      onInput={(event: FormEvent<HTMLDivElement>) =>
        onChange({ ...value, type: "text", text: event.currentTarget.textContent || "" })
      }
      onKeyDown={(event) => {
        if (event.key === "Enter") {
          event.preventDefault();
          const activeIndex = manager.activeIndex;
          manager.replaceToken({ ...value, type: "token", text: event.currentTarget.textContent || "" }, activeIndex);
          manager.addToken({ type: "text", text: "" });
          setTimeout(() => (manager.activeIndex = activeIndex + 1), 0);
        }
      }}
    />
  );
}

const StyledPlainInput = styled.div`
  display: inline-block;

  &:focus {
    outline: 0;
  }
`;
