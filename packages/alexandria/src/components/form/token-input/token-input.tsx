import { ReactNode } from "react";
import styled from "styled-components";
import { InputToken, TokenInputManager, useTokenInput } from "./token-input-manager.js";
import { PlainInput } from "./plain-input.js";

export interface TokenInputProps {
  tokens: InputToken[];

  onTokensChange(newValue: InputToken[]): void;

  onRenderToken?(token: InputToken, manager: TokenInputManager): ReactNode;
}

export function TokenInput({ tokens, onTokensChange, onRenderToken }: TokenInputProps) {
  const manager = useTokenInput(tokens, onTokensChange);

  return (
    <StyledTokenInput
      ref={manager.handleRef}
      onFocus={manager.handleFocus}
      onKeyDown={manager.handleKeyDown}
      tabIndex={0}
    >
      {tokens.map(
        (token, index) =>
          onRenderToken?.(token, manager) || (
            <PlainInput
              key={token.key}
              value={token}
              onChange={(newValue) => manager.replaceToken(newValue, index)}
              manager={manager}
            />
          )
      )}
    </StyledTokenInput>
  );
}

const StyledTokenInput = styled.div`
  cursor: text;
  background-color: #ddd;
`;
