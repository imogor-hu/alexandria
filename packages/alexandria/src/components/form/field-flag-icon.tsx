import { flagIcons } from "../../utils/index.js";
import { currentLanguage$ } from "../../services/translation-service.js";
import { spacing1, spacing2 } from "../../themes/index.js";
import styled from "styled-components";
import { useObservable } from "@litbase/use-observable";
import { ComponentPropsWithoutRef, ComponentType } from "react";

export interface FlagIconProps extends ComponentPropsWithoutRef<"svg"> {
  language: string;
  flags?: Record<string, ComponentType<"svg">>;
}

export function FlagIcon({ language, flags = flagIcons, ...props }: FlagIconProps) {
  const FlagIcon = flags[language];

  return <StyledFlagIcon as={FlagIcon as unknown as undefined} {...props} />;
}

const StyledFlagIcon = styled.svg`
  height: 1em;
  padding-left: 0.5em;
`;

export function FieldFlagIcon() {
  const [currentLanguage] = useObservable(() => currentLanguage$, "hu");

  return <StyledFieldFlagIcon language={currentLanguage} />;
}

export const StyledFieldFlagIcon = styled(FlagIcon)`
  height: ${({ theme }) => theme.textXs};
  margin-top: -${spacing2};
  width: auto;
  padding-left: ${spacing1};
`;
