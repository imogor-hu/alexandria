import { useMemo } from "react";
import { identity } from "lodash-es";
import { DocumentInterface, MultiPromiseQueryObject } from "@litbase/core";
import { SelectField, SelectFieldProps, selectTranslations } from "./select.js";
import { useQuery } from "@litbase/react";
import { TranslatedObject } from "../../models/translated-object.js";
import { getTranslatableDisplayText } from "../../services/translation-service.js";

export interface EntitySelectFieldProps<TCollection extends string, TValue, TRealValue = TValue>
  extends Omit<SelectFieldProps<TValue, TRealValue>, "options"> {
  collection: TCollection;
  options?: Record<string, unknown> | TValue[];
}

export function DocumentSelectField<TCollection extends string, TValue extends DocumentInterface>({
  options,
  getOptionLabel,
  collection,
  ...props
}: EntitySelectFieldProps<TCollection, TValue>) {
  const [docs, isLoading] = useQuery(collection, { $matchAll: {}, _id: 1, ...options } as MultiPromiseQueryObject);

  return (
    <SelectField<TValue>
      options={docs as TValue[]}
      getOptionValue={(option) => option._id.toBase64String()}
      getOptionLabel={getOptionLabel}
      parse={(entity) => (entity ? entity : null)}
      format={(value) => (value ? value : null)}
      isLoading={props.isLoading || isLoading}
      {...props}
    />
  );
}

export type MultipleEntitySelectFieldProps<TCollection extends string, TValue> = EntitySelectFieldProps<
  TCollection,
  TValue,
  string
>;

export function MultipleEntitySelectField<TCollection extends string, TValue extends DocumentInterface>({
  options,
  collection,
  ...props
}: MultipleEntitySelectFieldProps<TCollection, TValue>) {
  const [docs, isLoading] = useQuery<TValue>(collection, {
    $matchAll: {},
    _id: 1,
    ...options,
  } as MultiPromiseQueryObject);
  const sortedDocs = useMemo(() => {
    return [...docs].sort((a, b) => {
      function getLabel(elem: TValue) {
        const label =
          (typeof elem === "string" && elem) ||
          (elem as unknown as { name: string | TranslatedObject }).name ||
          (elem as unknown as { label: string | TranslatedObject }).label ||
          (elem as unknown as { title: string | TranslatedObject }).title;
        if (typeof label === "string") {
          return label;
        }
        return getTranslatableDisplayText(label);
      }

      return getLabel(a).localeCompare(getLabel(b));
    });
  }, [docs]);

  return (
    <SelectField<TValue, string>
      {...(isLoading ? { placeholder: selectTranslations.loading } : {})}
      options={sortedDocs}
      getOptionValue={(option: TValue) => option._id.toBase64String()}
      parse={(ids: string[]) => {
        if (null == ids) {
          return [];
        }

        if (!Array.isArray(ids)) {
          ids = [ids];
        }

        return ids.map((id) => docs.find((doc) => doc._id.toBase64String() === id)).filter(identity) as TValue[];
      }}
      format={(value) => {
        if (Array.isArray(value)) {
          return value.map((v) => v._id.toBase64String());
        }

        if (null != value) {
          return [value._id.toBase64String()];
        }

        return [];
      }}
      isLoading={props.isLoading || isLoading}
      isMulti
      {...props}
    />
  );
}
