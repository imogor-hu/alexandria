import styled, { AnyStyledComponent } from "styled-components";
import { generateClockElement, generateClockElements, TimeInputMode } from "./time-input-helpers.js";
import { SpringValue } from "react-spring";

const HourNumber = styled.div`
  position: absolute;
  height: 1.2em;
  width: 1.2em;
  line-height: 1.2em;
  text-align: center;
  transform: translate(-50%, -50%);
  cursor: pointer;
`;
const LargeHourNumber = styled(HourNumber)`
  font-size: 1.1em;
`;
const SmallHourNumber = styled(HourNumber)`
  font-size: 0.7em;
`;

const MinuteDot = styled.div`
  position: absolute;
  transform: translate(-50%, -50%);
  width: 0.4rem;
  height: 0.4rem;
  border-radius: 50%;
  background-color: currentColor;
`;

interface ClockFaceProps {
  mode: TimeInputMode;
  style: { [key: string]: SpringValue };
  component: AnyStyledComponent;
  minute: number;
}

export function ClockFace({ mode, style, component: Component, minute }: ClockFaceProps) {
  const innerHourRadius = 25;
  const outerHourRadius = 40;

  return (
    <Component style={style}>
      {"hour" === mode ? (
        <>
          {generateClockElements(1, 12, 1, outerHourRadius, LargeHourNumber)}
          {generateClockElements(13, 24, 1, innerHourRadius, SmallHourNumber)}
        </>
      ) : (
        <>
          {generateClockElements(5, 60, 5, outerHourRadius, LargeHourNumber)}
          {!!(minute % 5) && generateClockElement(MinuteDot, null, minute, outerHourRadius)}
        </>
      )}
    </Component>
  );
}
