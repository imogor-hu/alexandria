import { FieldProps } from "formik";
import { TimeInput } from "./time-input.js";
import Tippy from "@tippyjs/react";
import { ChangeEvent, useCallback, useRef, FocusEvent } from "react";
import { useAction } from "use-action";
import { TimeValue } from "./time-input-helpers.js";
import { TextInput, TextInputProps } from "../input.js";
import { useForceUpdate, useRefValue } from "../../../hooks/index.js";

export interface TimeFieldProps extends FieldProps<string | Date>, Omit<TextInputProps, "form"> {}

export function TimeField({ field, form, ...props }: TimeFieldProps) {
  const valueRef = useRef<string | Date>(field.value);
  const forceUpdate = useForceUpdate();
  const hasFocusRef = useRef(false);
  const fieldRef = useRefValue(field);
  const formRef = useRefValue(form);

  useAction(() => {
    if (!hasFocusRef.current) {
      valueRef.current = field.value;
    }
  }, [field.value]);

  const onInputChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    const stringValue = event.currentTarget.value;

    if (isValidStringTime(stringValue)) {
      valueRef.current = convertStringToValue(stringValue, valueRef.current);
      formRef.current.setFieldValue(fieldRef.current.name, valueRef.current);
    }

    forceUpdate();
  }, []);

  const onValueChange = useCallback((value: TimeValue) => {
    formRef.current.setFieldValue(
      fieldRef.current.name,
      convertStringToValue(timeValueToString(value), valueRef.current)
    );
    forceUpdate();
  }, []);

  const onFocus = useCallback(() => {
    hasFocusRef.current = true;
  }, []);

  const onBlur = useCallback((event: FocusEvent<HTMLInputElement>) => {
    const field = fieldRef.current;

    field.onBlur(event);

    hasFocusRef.current = false;

    if (valueRef.current !== field.value) {
      valueRef.current = field.value;
      forceUpdate();
    }
  }, []);

  return (
    <Tippy
      content={<TimeInput value={stringToTimeValue(convertValueToString(field.value))} onChange={onValueChange} />}
      trigger="focusin"
      interactive
    >
      <TextInput
        name={field.name}
        value={convertValueToString(valueRef.current)}
        onChange={onInputChange}
        onFocus={onFocus}
        onBlur={onBlur}
        {...props}
        key={props.key || undefined}
      />
    </Tippy>
  );
}

/**
 * Converts any value that can be given to the time input field to a common format
 * */
function convertValueToString(value: Date | string) {
  if (typeof value === "string") {
    if (isValidDateString(value)) {
      const dateValue = new Date(value);
      return String(dateValue.getHours()).padStart(2, "0") + ":" + String(dateValue.getMinutes()).padStart(2, "0");
    } else {
      return value;
    }
  } else {
    return String(value.getHours()).padStart(2, "0") + ":" + String(value.getMinutes()).padStart(2, "0");
  }
}

/**
 * Converts back common value time input field internally works with to the type of value
 * the field is used with. It determines that value by the type of the previous value.
 * */
function convertStringToValue(stringValue: string, previousValue: string | Date) {
  function doConvert(date: Date, string: string) {
    const newValue = date;
    const [hourString, minuteString] = string.split(":");
    newValue.setHours(Number(hourString));
    newValue.setMinutes(Number(minuteString));
    return newValue;
  }

  if (typeof previousValue === "string") {
    if (isValidDateString(previousValue)) {
      return doConvert(new Date(previousValue), stringValue);
    } else {
      return stringValue;
    }
  } else {
    return doConvert(previousValue, stringValue);
  }
}

function isValidStringTime(value: unknown) {
  if (typeof value !== "string") return false;

  const match = value.match(/(\d{1,2}):(\d{1,2})/);

  if (!match) return false;

  const hours = Number(match[1]);
  const minutes = Number(match[2]);

  return !Number.isNaN(hours) && !Number.isNaN(minutes);
}

function stringToTimeValue(value: unknown): TimeValue {
  if (typeof value !== "string") return [0, 0];

  const match = value.match(/(\d{1,2}):(\d{1,2})/);

  if (!match) return [0, 0];

  const hours = Number(match[1]);
  const minutes = Number(match[2]);

  if (Number.isNaN(hours) || Number.isNaN(minutes)) return [0, 0];

  return [hours, minutes];
}

function timeValueToString(value: TimeValue): string {
  if (!Array.isArray(value) || 2 !== value.length || typeof value[0] !== "number" || typeof value[1] !== "number")
    return "00:00";

  return `${value[0].toString().padStart(2, "0")}:${value[1].toString().padStart(2, "0")}`;
}

function isValidDateString(input: string) {
  return input.length > 5 && new Date(input).toString() !== "Invalid Date";
}
