import { default as React, ElementType } from "react";
import { SpringValue } from "react-spring";

export type TimeValue = [number, number];
export type TimeInputMode = "hour" | "minute";

type Vector2 = [number, number];

interface BoundsSubset {
  left: number;
  top: number;
  width: number;
}

export function getPointOnCircle(rad: number, radius: number, centerPoint: [number, number] = [0, 0]) {
  const [centerX, centerY] = centerPoint;

  const x = centerX + radius * Math.cos(rad);
  const y = centerY + radius * Math.sin(rad);

  return [x, y];
}

export function generateClockElements(from: number, to: number, step: number, radius: number, component: ElementType) {
  const elements = [];

  for (let num = from, index = 0; index < 12; index++, num += step) {
    elements.push(
      generateClockElement(component, num !== 24 && num !== 60 ? num : "00", (60 / 12) * index + 5, radius)
    );
  }

  return elements;
}

export function generateClockElement(
  Component: ElementType,
  text: number | string | null,
  index: number,
  radius: number
) {
  const rad = Math.PI * 2 * (index / 60) - Math.PI / 2;
  const [left, top] = getPointOnCircle(rad, radius, [50, 50]);

  return (
    <Component key={text} style={{ left: `${left}%`, top: `${top}%` }}>
      {text}
    </Component>
  );
}

export function generateClockPoint(index: number, radius: number, Component: ElementType) {
  const [left, top] = getPointOnCircle(Math.PI * 2 * (index / 60), radius, [50, 50]);

  return <Component style={{ left: `${left}%`, top: `${top}%` }} />;
}

export function getDragDetails(xy: Vector2, clockBounds: BoundsSubset, sliceCount: number) {
  const [pointerX, pointerY] = xy;
  const { left: clockLeft, top: clockTop, width: clockSize } = clockBounds;

  const handSize = clockSize / 2;
  const centerX = clockLeft + handSize;
  const centerY = clockTop + handSize;

  const dx = pointerX - centerX;
  const dy = pointerY - centerY;
  const distanceFromCenter = Math.sqrt(dx ** 2 + dy ** 2);
  // How much angle does each hour get? (1rad = 180deg, so only divide by 6 instead of 12, for example)
  const anglePerHour = Math.PI / (sliceCount / 2);
  // Offset the angle by half the anglePerHour value, so each hour number is at the center of their respective
  // slice
  const angleOffset = anglePerHour / 2;
  // Get the angle between the clicked point and the center of the clock with the magic of atan2. The value is
  // in radians.
  // Offset the result with 90deg so the angles start at the top and then offset it more with the above
  // explained offset value.
  let angle = Math.atan2(dy, dx) + Math.PI / 2 + angleOffset;

  // atan2 gives values in the range of -180deg to +180deg (in radians), so if we get a negative value, add
  // 360deg (in radians), so we get a range of 0deg to 360deg (in radians).
  if (angle < 0) angle += Math.PI * 2;

  const hourIndex = Math.floor(angle / anglePerHour);
  const distancePercent = Math.floor((distanceFromCenter / handSize) * 100);

  return [hourIndex, distancePercent];
}

export function getHourValue(xy: Vector2, clockBounds: BoundsSubset) {
  const [index, distancePercent] = getDragDetails(xy, clockBounds, 12);

  if (distancePercent < 70) {
    if (index === 0) return 0;
    else return index + 12;
  } else {
    if (index === 0) return 12;
    else return index;
  }
}

export function getMinuteValue(xy: Vector2, clockBounds: BoundsSubset) {
  const [index] = getDragDetails(xy, clockBounds, 60);

  return index;
}

export function getAnimationValues(mode: TimeInputMode, value: TimeValue, rotation?: SpringValue<number>) {
  let targetRotation: number;
  let targetHandLength: number;
  let targetHandTopRadius: number;
  let targetHandTopOffset: number;

  if ("hour" === mode) {
    if (0 < value[0] && value[0] < 13) {
      targetRotation = value[0] * (360 / 12);
      targetHandLength = 1;
      targetHandTopRadius = 0.09;
      targetHandTopOffset = 0.1;
    } else {
      targetRotation = Math.max(0, (value[0] - 12) * (360 / 12));
      targetHandLength = 0.5;
      targetHandTopRadius = 0.06;
      targetHandTopOffset = 0.25;
    }
  } else {
    targetRotation = value[1] * (360 / 60);
    targetHandLength = 1;
    targetHandTopRadius = 0.09;
    targetHandTopOffset = 0.1;
  }

  if (rotation) {
    const startingRotation = rotation.get();

    // Fins the shortest rotational path to the target angle
    // https://stackoverflow.com/a/28037434
    const diff = ((targetRotation - startingRotation + 180) % 360) - 180;
    targetRotation = startingRotation + (diff < -180 ? diff + 360 : diff);
  }

  return {
    rotation: targetRotation,
    handLength: targetHandLength,
    handTopRadius: targetHandTopRadius,
    handTopOffset: targetHandTopOffset,
  };
}
