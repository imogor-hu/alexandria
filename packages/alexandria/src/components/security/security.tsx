import { Children, Fragment, isValidElement, ReactElement, ReactNode } from "react";
import { isEmptyUser, isUserGuest } from "@litbase/core";
import {
  IndexRouteProps,
  LayoutRouteProps,
  Navigate,
  Outlet,
  PathRouteProps,
  Route,
  RouteObject,
  RoutesProps,
  useRoutes,
} from "react-router-dom";
import { from, isObservable, Observable, of } from "rxjs";
import { client } from "@litbase/client";
import { map } from "rxjs/operators";
import { useObservable } from "@litbase/use-observable";
import { isPromise } from "formik";

type RouteProps = PathRouteProps | LayoutRouteProps | IndexRouteProps;
type SecurityRouteProps = RouteProps & {
  path?: string;
  isGranted: string;
  subject?: unknown;
  fallback?: string;
};

export function SecurityRoute({ element = <Outlet />, isGranted, subject, fallback, ...props }: SecurityRouteProps) {
  return (
    <Route
      {...props}
      element={
        <Security
          isGranted={isGranted}
          subject={subject}
          path={props.path}
          fallback={fallback ? <Navigate to={fallback} /> : null}
        >
          {element}
        </Security>
      }
    />
  );
}

export interface SecurityRoutesProps extends RoutesProps {
  loading?: ReactNode;
}

// Based on the actual Routes component
export function SecurityRoutes({ children, location, loading }: SecurityRoutesProps) {
  const routes = useRoutes(createRoutesFromChildren(children), location);

  return (
    <Security isGranted="ANY" loading={loading}>
      {routes}
    </Security>
  );
}

export interface SecurityProps {
  isGranted: string;
  subject?: unknown;
  fallback?: ReactNode;
  loading?: ReactNode;
  children?: ReactNode;
  path?: string;
}

export function Security({ isGranted, subject, fallback = null, loading = null, children, path }: SecurityProps) {
  const user = useProvidedUser();

  if (user.isLoading) {
    return loading as ReactElement;
  }

  const result = isSecurityGranted({ isGranted, subject, user });

  const onVoteResult = securityConfig.onVote({ isGranted, subject, user, result, children, fallback, path });

  if (undefined !== onVoteResult) {
    return onVoteResult as ReactElement;
  }

  return (result ? children : fallback) as ReactElement;
}

// Based on:
// https://github.com/remix-run/react-router/blob/d32662ddd67d489a46171b450ec933a0ce88d346/packages/react-router/index.tsx#L755
function createRoutesFromChildren(children: ReactNode): RouteObject[] {
  const routes: RouteObject[] = [];

  Children.forEach(children, (element) => {
    if (!isValidElement(element)) {
      // Ignore non-elements. This allows people to more easily inline
      // conditionals in their route config.
      return;
    }

    if (element.type === Fragment) {
      // Transparently support React.Fragment and its children.
      routes.push(...createRoutesFromChildren(element.props.children));
      return;
    }

    if (
      element.type === SecurityRoute ||
      (typeof element.type !== "string" && element.type.name === SecurityRoute.name)
    ) {
      element = SecurityRoute(element.props);
    }

    // Rerun this to make typescript happy
    if (!isValidElement(element)) return;

    // Rewrote the invariant part (see original source)
    if (element.type !== Route) {
      throw new Error(
        `[${
          typeof element.type === "string" ? element.type : element.type.name
        }] is not a <Route> component. All component children of <Routes> must be a <Route> or <React.Fragment>`
      );
    }

    const props = element.props as PathRouteProps;

    const route: RouteObject = {
      caseSensitive: props.caseSensitive,
      element: props.element,
      index: props.index,
      path: props.path,
    };

    if (props.children) {
      route.children = createRoutesFromChildren(props.children);
    }

    routes.push(route);
  });

  return routes;
}

interface IsSecurityGrantedOptions {
  isGranted: string;
  subject?: unknown;
  user: ProvidedUser;
}

export interface VoteResult {
  isGranted: string;
  subject?: unknown;
  user: ProvidedUser;
  result: boolean | null;
  children: ReactNode;
  fallback?: ReactNode;
  path?: string;
}

export function isSecurityGranted(options: IsSecurityGrantedOptions) {
  for (const voter of securityVoters) {
    const result = voter(options);

    if (result === true || result === false) {
      return result;
    }
  }

  return null;
}

export function useProvidedUser() {
  const [value] = useObservable(
    () => {
      const result = securityConfig.userProvider();

      if (isObservable(result)) {
        return result;
      }

      if (isPromise(result)) {
        return from(result);
      }

      return of(result);
    },
    { user: {}, isAuthenticated: false, isLoading: true },
    []
  );

  return value;
}

export function useIsSecurityGranted(options: Omit<IsSecurityGrantedOptions, "user">) {
  const user = useProvidedUser();

  return isSecurityGranted({ ...options, user });
}

export interface ProvidedUser<T = unknown> {
  user: T;
  isAuthenticated: boolean;
  isLoading: boolean;
}

export interface UserProviderFunction {
  (): Observable<ProvidedUser> | Promise<ProvidedUser> | ProvidedUser;
}

export interface OnVoteFunction {
  (options: VoteResult): ReactNode;
}

export interface VoterFunction {
  (options: IsSecurityGrantedOptions): boolean | null;
}

export interface SecurityConfig {
  userProvider: UserProviderFunction;
  onVote: OnVoteFunction;
  voters: VoterFunction[];
}

export const securityConfig: SecurityConfig = {
  userProvider: () =>
    client.currentUser$.pipe(
      map((user) => ({ user, isAuthenticated: !isEmptyUser(user) && !isUserGuest(user), isLoading: isEmptyUser(user) }))
    ),
  onVote: () => undefined,
  voters: [
    ({ isGranted, user }) => {
      if (isGranted === "ANY") {
        return true;
      }

      if (isGranted === "IS_AUTHENTICATED") {
        return user.isAuthenticated;
      }

      if (isGranted === "IS_NOT_AUTHENTICATED") {
        return !user.isAuthenticated;
      }

      return null;
    },
  ],
};

export const securityVoters = securityConfig.voters;
