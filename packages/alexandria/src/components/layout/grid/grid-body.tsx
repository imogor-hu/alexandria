import { Link } from "react-router-dom";
import { Cell } from "./grid.js";
import { ReactNode } from "react";
import { FontWeight, spacing16, spacing5, transition } from "../../../themes/index.js";
import styled from "styled-components";
import { LargeLoadingIcon } from "../../utils/loading-icon.js";
import { Button, ButtonProps } from "../../inputs/button.js";

export interface GridBodyProps {
  isLoading?: boolean;
  isEmpty?: boolean;
  emptyLabel?: ReactNode;
  children: ReactNode;
}

export function GridBody({ isLoading, isEmpty, emptyLabel = "No results found", children }: GridBodyProps) {
  if (!isLoading && isEmpty) {
    return <NoResultsCell>{emptyLabel}</NoResultsCell>;
  }

  if (isLoading && isEmpty) {
    return (
      <FullSpanCell>
        <LargeLoadingIcon />
      </FullSpanCell>
    );
  }

  return children as JSX.Element;
}

export const HitName = styled(Link)`
  color: ${({ theme }) => theme.primary700};
  font-weight: ${FontWeight.Medium};
  cursor: pointer;
  transition: ${({ theme }) => transition(["color"], theme.defaultTransitionTiming)};
  line-height: ${({ theme }) => theme.leading5}; // Make it easier to click

  &:hover {
    color: ${({ theme }) => theme.primary500};
    text-decoration: none;
  }
`;

export const FullSpanCell = styled(Cell)`
  padding: ${spacing16} ${spacing5};
  justify-content: center;
  // Make the row go from the first, to the last (first from last => -1) column.
  // Source: https://stackoverflow.com/a/50612664
  grid-column: 1 / -1;
`;

export const NoResultsCell = styled(FullSpanCell)`
  font-size: ${({ theme }) => theme.textXl};
  color: ${({ theme }) => theme.gray500};
`;

export const GridButton = styled(Button).attrs<ButtonProps>(({ $display, $kind, $onlyIcon }) => ({
  $display: $display ?? "inline-flex",
  $kind: $kind ?? "link",
  $onlyIcon: $onlyIcon ?? true,
}))`
  font-size: ${({ theme }) => theme.textXl};
  background-color: transparent;

  & + & {
    margin-left: ${({ theme }) => theme.spacing3};
  }
`;
