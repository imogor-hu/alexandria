import { animated, useSpring } from "react-spring";
import { useRef, useState, ReactNode } from "react";
import { ChevronRight } from "@styled-icons/boxicons-regular";
import { FontWeight, spacing4, spacing8, zIndex10 } from "../../../themes/index.js";
import styled, { css } from "styled-components";
import { useMeasure } from "../../../hooks/index.js";

interface GridProps {
  columns: string;
  gap?: string;
}

export const Grid = styled.div.attrs<GridProps>(({ columns, gap, style }) => ({
  style: { ...style, gridTemplateColumns: columns, gap },
}))<GridProps>`
  display: grid;
  font-size: ${({ theme }) => theme.textSm};
  position: relative;
  overflow: visible;
`;

export const Cell = styled.div<{ $align?: "normal" | "start" | "center" | "end"; $fullWidth?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: ${({ $align = "normal" }) => $align};
  padding: ${({ theme }) => theme.spacing4} ${({ theme }) => theme.spacing6};
  ${({ $fullWidth }) => $fullWidth && "grid-column: 1 / -1"};
`;

export const HeadCell = styled(Cell)`
  position: sticky;
  top: -${({ theme }) => theme.spacing8}; // Offset page padding
  background-color: ${({ theme }) => theme.gray50};
  border-bottom: 1px solid ${({ theme }) => theme.gray200};
  padding: ${({ theme }) => theme.spacing3} ${({ theme }) => theme.spacing6};
  text-transform: uppercase;
  color: ${({ theme }) => theme.gray500};
  font-size: ${({ theme }) => theme.textXs};
  font-weight: ${FontWeight.Medium};
  z-index: ${zIndex10};

  &:first-child {
    border-top-left-radius: ${({ theme }) => theme.roundedLg};
  }

  &:last-child {
    border-top-right-radius: ${({ theme }) => theme.roundedLg};
  }
`;

export const FootCell = styled(Cell)<{ $fullWidth?: boolean }>`
  grid-column: ${({ $fullWidth }) => ($fullWidth ? "1 / -1" : "auto")};
  justify-content: end;
  padding: ${({ theme }) => theme.spacing3} ${({ theme }) => theme.spacing6};
  background-color: ${({ theme }) => theme.gray50};
  border-radius: 0 0 ${({ theme }) => theme.roundedLg} ${({ theme }) => theme.roundedLg};
`;

const gridRowAlternating = css`
  &:nth-child(2n) {
    ${Cell} {
      background-color: ${({ theme }) => theme.gray100};
    }
  }
`;

const gridRowManualAlternate = css<{ $parity?: number | boolean }>`
  ${Cell} {
    ${({ theme, $parity }) => typeof $parity === "number" && !!($parity % 2) && `background-color: ${theme.gray100}`};
  }
`;

export const GridRow = styled.div<{ $disabled?: boolean; $parity?: number | boolean }>`
  display: contents;

  ${Cell} {
    opacity: ${({ $disabled }) => ($disabled ? "0.35" : "1")};
    pointer-events: ${({ $disabled }) => ($disabled ? "none" : "auto")};
  }

  ${({ $parity }) => (true === $parity ? gridRowAlternating : gridRowManualAlternate)};
`;

export function CollapsableGridRow(props: {
  content?: ReactNode;
  children: ReactNode;
  open: boolean;
  onToggle: () => void;
  $parity?: number;
  $disabled?: boolean;
}) {
  const contentRef = useRef<HTMLDivElement | null>(null);
  const [contentHeight, setContentHeight] = useState(0);
  useMeasure((bounds) => setContentHeight(bounds.height), contentRef);
  const animStyles = useSpring({
    height: props.open ? `${contentHeight}px` : `0px`,
    transform: props.open ? "rotate(90deg)" : "rotate(0deg)",
  });
  return (
    <>
      <GridRow $disabled={props.$disabled}>
        <Cell>
          {props.content && (
            <OpenStateIndicator onClick={() => props.onToggle()} style={{ transform: animStyles.transform }} />
          )}
        </Cell>
        {props.children}
      </GridRow>
      {props.content && (
        <NonGridGridRow>
          <CollapsibleGridRowContent $parity={props.$parity || 0} style={{ height: animStyles.height }}>
            <div ref={contentRef}>
              <CollapsibleGridRowContainer>{props.content}</CollapsibleGridRowContainer>
            </div>
          </CollapsibleGridRowContent>
        </NonGridGridRow>
      )}
    </>
  );
}

const CollapsibleGridRowContainer = styled.div`
  padding: ${spacing4} ${spacing8};
`;

const CollapsibleGridRowContent = styled(animated.div)<{ $parity: number }>`
  overflow: hidden;
  ${({ theme, $parity }) => $parity && `background-color: ${theme.gray100}`};
`;

const OpenStateIndicator = styled(animated(ChevronRight))`
  font-size: 1.5rem;
  cursor: pointer;
`;

const NonGridGridRow = styled(GridRow)`
  > * {
    grid-column: 1/-1;
  }
`;
