import { ReactNode } from "react";
import { CaretUp } from "@styled-icons/boxicons-regular";
import { spacing2 } from "../../../themes/index.js";
import styled from "styled-components";
import { HeadCell } from "./grid.js";

type SortValue<T> = { [key in keyof T]?: "asc" | "desc" };

interface SortableHeadCellProps<T> {
  children: ReactNode;
  name: keyof T;
  value: SortValue<T>;
  onSort: (value: SortValue<T>) => void;
}

export function SortableHeadCell<T>({ children, name, value, onSort }: SortableHeadCellProps<T>) {
  const currentValue = value[name];

  function onClick(direction: "asc" | "desc") {
    onSort({ [name]: currentValue === direction ? undefined : direction } as SortValue<T>);
  }

  return (
    <CellBody>
      {children}
      <Col>
        <Arrow $isActive={currentValue === "asc"} onClick={() => onClick("asc")} />
        <DownArrow $isActive={currentValue === "desc"} onClick={() => onClick("desc")} />
      </Col>
    </CellBody>
  );
}

const Arrow = styled(CaretUp)<{ $isActive: boolean }>`
  color: ${({ $isActive }) => ($isActive ? "black" : "gray")};
  cursor: pointer;
  font-size: 1.2rem;
`;

const DownArrow = styled(Arrow)`
  transform: rotate(180deg);
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-left: ${spacing2};
`;

const CellBody = styled(HeadCell)`
  display: flex;
`;
