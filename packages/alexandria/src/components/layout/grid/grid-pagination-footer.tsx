import { ReactElement, useCallback, MouseEvent, ReactNode } from "react";
import { clamp } from "lodash-es";
import { ChevronLeft, ChevronRight } from "@styled-icons/boxicons-regular";
import { StyledIconBase } from "@styled-icons/styled-icon";
import { FontWeight, spacing2, spacing3, spacing6 } from "../../../themes/index.js";
import styled from "styled-components";
import { Button, ButtonGroup } from "../../inputs/button.js";

interface GridPaginationFooterProps {
  pageSize: number;
  onPageSizeChange: (newValue: number) => void;
  pageCount: number;
  currentPage: number;
  goToPage: (newPage: number) => void;
  itemCount: number;
  pageText?: string | ReactNode;
}

export function GridPaginationFooter({ pageText = "db / oldal", ...props }: GridPaginationFooterProps) {
  return (
    <StyledGridPaginationFooter>
      <HitsPerPageSelector
        value={props.pageSize}
        onChange={(value) => props.onPageSizeChange(Number(value))}
        options={[
          { value: 25, label: "25" },
          { value: 50, label: "50" },
          { value: 100, label: "100" },
        ]}
      />
      <HitsPerPageLabel>{pageText}</HitsPerPageLabel>
      <StateResults itemCount={props.itemCount} hitsPerPage={props.pageSize} page={props.currentPage - 1} />
      <Pagination goToPage={props.goToPage} currentPage={props.currentPage} nPages={props.pageCount} />
    </StyledGridPaginationFooter>
  );
}

interface HitsPerPageSelectorProps {
  options: { value: number; label: string }[];
  value: number;
  onChange: (newValue: number) => void;
}

function HitsPerPageSelector(props: HitsPerPageSelectorProps) {
  return (
    <select value={props.value} onChange={(event) => props.onChange(Number(event.target.value))}>
      {props.options.map((elem, index) => (
        <option key={index} value={elem.value}>
          {elem.label}
        </option>
      ))}
    </select>
  );
}

function getHuEnding(value: number) {
  switch (value % 10) {
    case 3:
    case 6:
    case 8:
      return "ból";
    default:
      return "ből";
  }
}

interface StateResultsProps {
  itemCount?: number;
  hitsPerPage?: number;
  page?: number;
}

function StateResults({ hitsPerPage, itemCount, page }: StateResultsProps) {
  if (!itemCount || !hitsPerPage || page === undefined) return <StyledStateResults />;

  const start = page * hitsPerPage + 1;
  const end = Math.min(start + hitsPerPage - 1, itemCount);

  return (
    <StyledStateResults>
      <span>
        <b>{start}</b> – <b>{end}</b> találat a <b>{itemCount}</b>-{getHuEnding(itemCount)}
      </span>
    </StyledStateResults>
  );
}

const StyledStateResults = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: flex-end;

  b {
    font-weight: ${FontWeight.Medium};
  }
`;

interface ConnectPaginationProps {
  currentPage: number;
  nPages: number;

  goToPage(page: number): void;
}

function Pagination({ currentPage, nPages, goToPage }: ConnectPaginationProps) {
  const padding = 2;

  const pageElements: ReactElement[] = [];

  for (let i = 0, length = Math.min(nPages - 1, padding * 2 + 3); i < length; i++) {
    const p = clamp(currentPage - padding - 1, 2, nPages - length + 1) + i;

    if ((i === 0 && 2 < p) || (i === length - 1 && p < nPages - 1)) {
      pageElements.push(<PageButton key={i} />);
    } else {
      pageElements.push(<PageButton key={i} page={p} goToPage={goToPage} current={currentPage === p} />);
    }
  }

  return (
    <StyledPagination>
      {1 < nPages && <StepButton page={currentPage} step={-1} disabled={currentPage === 1} goToPage={goToPage} />}
      <PageButton page={1} goToPage={goToPage} current={currentPage === 1} />
      {pageElements}
      {1 < nPages && currentPage + padding + 1 < nPages && 1 + pageElements.length < nPages && (
        <PageButton page={nPages} current={currentPage === nPages} goToPage={goToPage} />
      )}
      {1 < nPages && <StepButton page={currentPage} step={1} disabled={currentPage === nPages} goToPage={goToPage} />}
    </StyledPagination>
  );
}

const StyledPagination = styled(ButtonGroup)`
  margin-left: ${spacing6};
`;

function StepButton({
  page,
  step,
  goToPage,
  disabled,
}: {
  page: number;
  step: number;
  goToPage: ConnectPaginationProps["goToPage"];
  disabled: boolean;
}) {
  const onClick = useCallback(
    (event: MouseEvent<HTMLButtonElement>) => {
      event.preventDefault();
      goToPage(page + step);
    },
    [goToPage, page, step]
  );

  return (
    <StyledStepButton disabled={disabled} onClick={onClick} $onlyIcon>
      {0 < step ? <ChevronRight /> : <ChevronLeft />}
    </StyledStepButton>
  );
}

const StyledStepButton = styled(Button)`
  ${StyledIconBase} {
    margin-left: -0.375em;
    margin-right: -0.375em;
  }
`;

function PageButton({
  page,
  goToPage,
  current,
}: {
  page?: number;
  goToPage?: ConnectPaginationProps["goToPage"];
  current?: boolean;
}) {
  function onClick(event: MouseEvent<HTMLButtonElement>) {
    event.preventDefault();

    if (current || !page || !goToPage) return;

    goToPage(page);
  }

  return (
    <Button disabled={current || !page} $isActive={current} onClick={onClick}>
      {page || "…"}
    </Button>
  );
}

const HitsPerPageLabel = styled.span`
  margin-left: ${spacing2};
`;

const StyledGridPaginationFooter = styled.div`
  display: flex;
  align-items: center;
  background-color: white;
  padding: ${spacing3};
  border-top: 1px solid ${({ theme }) => theme.gray200};
  font-size: ${({ theme }) => theme.textSm};

  select {
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20' fill='none'%3E%3Cpath d='M7 7l3-3 3 3m0 6l-3 3-3-3' stroke='%239fa6b2' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round'/%3E%3C/svg%3E");
    appearance: none;
    color-adjust: exact;
    background-repeat: no-repeat;
    background-color: #fff;
    border: 1px solid ${({ theme }) => theme.gray300};
    border-radius: 0.375em;
    padding: 0.5em 2em 0.5em 0.75em;
    font-size: ${({ theme }) => theme.textSm};
    line-height: 1.5;
    background-position: right 0.5em center;
    background-size: 1.5em 1.5em;
  }
`;
