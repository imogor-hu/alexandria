import { ChevronRight } from "@styled-icons/boxicons-regular";
import { ComponentProps, forwardRef, MouseEvent, ReactNode, Ref } from "react";
import { animated } from "react-spring";
import styled, { css } from "styled-components";
import { GridButton } from "./grid-body.js";
import { Collapsible, CollapsibleContent, useCollapsible } from "../../sidebar/collapsible.js";
import { spacing4, spacing6, transition } from "../../../themes/index.js";
import { Cell } from "./grid.js";
import { ReorderableListRow } from "../list/reorderable-list-page.js";

export function CollapsibleGridRow({ children }: { children: ReactNode }) {
  return <Collapsible>{children}</Collapsible>;
}

function CollapsibleGridRowHeaderComponent(
  { children, ...props }: { children: ReactNode } & ComponentProps<typeof StyledCollapsibleGridRowHeader>,
  ref: Ref<HTMLDivElement>
) {
  const { isOpen, setIsOpen } = useCollapsible();

  function onClick(event: MouseEvent<HTMLDivElement>) {
    if ((event.target as HTMLElement)?.closest(".drag-handle")) return;

    setIsOpen(!isOpen);
  }

  return (
    <StyledCollapsibleGridRowHeader {...props} onClick={onClick} $isOpen={isOpen} ref={ref}>
      {children}
    </StyledCollapsibleGridRowHeader>
  );
}

export const CollapsibleGridRowHeader = forwardRef(CollapsibleGridRowHeaderComponent);

export function CollapsibleIndicatorCell() {
  const { isOpen } = useCollapsible();

  return (
    <Cell>
      <ToggleBlockButton $isOpen={isOpen}>
        <ChevronRight />
      </ToggleBlockButton>
    </Cell>
  );
}

const openBlockEntryHeaderStyle = css`
  ${Cell} {
    background-color: ${({ theme }) => theme.gray100};
  }
`;

const StyledCollapsibleGridRowHeader = styled(ReorderableListRow)<{ $isOpen: boolean }>`
  ${Cell} {
    transition: ${({ theme }) => transition(["background-color"], theme.defaultTransitionTiming)};
  }

  &:hover {
    ${Cell} {
      background-color: ${({ theme }) => theme.gray50};
      cursor: pointer;
    }
  }

  ${({ $isOpen }) => $isOpen && openBlockEntryHeaderStyle};
`;

const ToggleBlockButton = styled(GridButton)<{ $isOpen: boolean }>`
  transform: ${({ $isOpen }) => ($isOpen ? "rotate(90deg)" : "rotate(0deg)")};

  transition: ${({ theme }) => transition(["transform"], theme.defaultTransitionTiming)};
`;

export function CollapsibleGridRowContent({ children }: { children: ReactNode }) {
  const { isOpen } = useCollapsible();

  return (
    <ReorderableListRow>
      <ReorderableListRowCell $isOpen={isOpen}>
        <CollapsibleContent>
          <CollapsibleContentInner>{children}</CollapsibleContentInner>
        </CollapsibleContent>
      </ReorderableListRowCell>
    </ReorderableListRow>
  );
}

const CollapsibleContentInner = styled.div`
  padding: ${spacing4} ${spacing6};
`;

const ReorderableListRowCell = styled(Cell).attrs(() => ({ as: animated.div }))<{ $isOpen: boolean }>`
  grid-column: 1 / -1;
  overflow: ${({ $isOpen }) => ($isOpen ? "visible" : "hidden")};
  padding: 0;
  border-top-color: transparent !important;

  > div {
    width: 100%;
  }
`;

export const CollapsibleContentButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-top: ${spacing4};
`;
