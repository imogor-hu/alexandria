import { StyledIconBase } from "@styled-icons/styled-icon";
import { CSSProperties, useCallback, useMemo, useRef, useState, ChangeEvent, MouseEvent } from "react";
import { throttle } from "lodash-es";
import { Search, X } from "@styled-icons/boxicons-regular";
import { Input, useUniqueId } from "../../form/index.js";
import { LoadingIcon } from "../../utils/loading-icon.js";
import { spacing3 } from "../../../themes/index.js";
import styled from "styled-components";

interface GridSearchBarProps {
  isSearchLoading?: boolean;
  search: (query: string) => void;
  style?: CSSProperties;
  placeholder?: string;
}

export function GridSearchHeader({ isSearchLoading, search, style, placeholder = "Keresés" }: GridSearchBarProps) {
  const [isSearching, setIsSearching] = useState(false);
  const [hasContent, setHasContent] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);

  const throttleSearch = useMemo(() => {
    return throttle(
      (newValue: string) => {
        search(newValue);

        setIsSearching(false);
      },
      250,
      { leading: false, trailing: true }
    );
  }, [search]);

  const onChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setIsSearching(true);

      const newValue = event.currentTarget.value;
      throttleSearch(newValue);
      setHasContent(!!newValue);
    },
    [throttleSearch]
  );

  const onClearClick = useCallback((event: MouseEvent<SVGSVGElement>) => {
    event.preventDefault();
    if (!inputRef.current) {
      return;
    }

    inputRef.current.value = "";
    search("");
    setHasContent(false);
  }, []);

  const fieldId = useUniqueId("grid-search-input");

  return (
    <StyledGridSearchBar style={style}>
      <label htmlFor={fieldId}>{isSearchLoading || isSearching ? <LoadingIcon /> : <Search />}</label>
      <GridSearchBarInput ref={inputRef} id={fieldId} type="text" onChange={onChange} placeholder={placeholder} />
      <X style={{ opacity: hasContent ? "1" : "0" }} onClick={onClearClick} />
    </StyledGridSearchBar>
  );
}

const StyledGridSearchBar = styled.div`
  display: flex;
  align-items: center;
  background-color: white;
  padding: ${spacing3};
  border-bottom: 1px solid ${({ theme }) => theme.gray200};

  label {
    margin: 0;
  }

  ${StyledIconBase} {
    font-size: ${({ theme }) => theme.textXl};
    margin: -0.375em 0;
    cursor: pointer;
  }
`;

const GridSearchBarInput = styled(Input)`
  background-color: transparent;
  border: none;
  flex-grow: 1;
  padding-top: 0;
  padding-bottom: 0;
`;
