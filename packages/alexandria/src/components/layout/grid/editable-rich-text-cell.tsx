import { useRef, useState } from "react";
import { useField } from "formik";
import { isEqual } from "lodash-es";
import { ModalDialogText, SimpleModal } from "../../utils/simple-modal.js";
import { spacing1, spacing2, spacing4, spacing8 } from "../../../themes/index.js";
import styled from "styled-components";
import { FieldLabel } from "../../form/field-label.js";
import { useModal } from "../../utils/modal-provider.js";
import { TranslatedObject } from "../../../models/translated-object.js";
import { getTranslatableDisplayText } from "../../../services/translation-service.js";
import { Cell } from "./grid.js";
import { RichTextEditor } from "../../form/rich-text-editor.js";
import { Button } from "../../inputs/button.js";
import { RichTextDisplay } from "../../utils/rich-text-display.js";

export function EditableRichTextCell({
  onChange,
  content,
  controlled,
  className,
}: {
  onChange: (content: string | TranslatedObject) => void;
  content: string | TranslatedObject;
  controlled?: boolean;
  className?: string;
}) {
  const { showModal } = useModal();
  const [cachedValue, setCachedValue] = useState(content);
  const value = (controlled !== false && content) || cachedValue;
  return (
    <StyledCell
      className={className}
      onClick={() =>
        showModal(
          <EditModal
            value={value}
            onDone={(value) => {
              setCachedValue(value);
              onChange(value);
            }}
          />
        )
      }
    >
      <RichTextDisplay content={typeof value === "string" ? value : getTranslatableDisplayText(value)} />
    </StyledCell>
  );
}

export function InlineEditableRichTextCell({
  className,
  label,
  name,
  onChange,
}: {
  label?: string;
  className?: string;
  name: string;
  onChange?: () => void;
}) {
  const [{ value }, , { setValue }] = useField<string | TranslatedObject>(name);
  const { showModal } = useModal();
  const ref = useRef<HTMLInputElement | null>(null);
  return (
    <StyledCell
      className={className}
      onClick={() =>
        showModal(
          <EditModal
            label={label}
            value={value}
            onDone={(newValue) => {
              // We don't want to trigger a save if no value actually changed.
              if (
                (typeof newValue === "string" && value !== newValue) ||
                (typeof newValue === "object" && !isEqual(newValue, value))
              ) {
                setValue(newValue);
                onChange?.();
              }
            }}
          />
        )
      }
    >
      <RichTextDisplay
        content={typeof value === "string" ? value : getTranslatableDisplayText(value as TranslatedObject)}
      />
      <input hidden ref={ref} />
    </StyledCell>
  );
}

function EditModal({
  value,
  onDone,
  label,
}: {
  value: string | TranslatedObject;
  onDone: (value: TranslatedObject | string) => void;
  label?: string;
}) {
  const [state, setState] = useState<string | TranslatedObject>(value);
  const isString = typeof value === "string";
  const { dismissModal } = useModal();
  return (
    <StyledModal hideCloseIcon>
      <Col>
        {label && <h3>{label}</h3>}
        {!isString && <FieldLabel>Magyar</FieldLabel>}
        <RichTextEditor
          value={isString ? (state as string) : (state as TranslatedObject).hu}
          onChange={(value) => setState(isString ? value : { ...(state as TranslatedObject), hu: value })}
        />
        {!isString && (
          <>
            <FieldLabel>Angol</FieldLabel>
            <RichTextEditor
              value={(state as TranslatedObject).en}
              onChange={(value) => setState({ ...(state as TranslatedObject), en: value })}
            />
          </>
        )}
        <Row>
          <Button onClick={() => dismissModal()}>Mégse</Button>
          <Button
            $kind="primary"
            onClick={() => {
              onDone(state);
              dismissModal();
            }}
          >
            Kész
          </Button>
        </Row>
      </Col>
    </StyledModal>
  );
}

const StyledModal = styled(SimpleModal)`
  ${ModalDialogText} {
    overflow-x: hidden;
  }
`;

const Row = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: ${spacing4};

  > * + * {
    margin-left: ${spacing2};
  }
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
`;

const StyledCell = styled(Cell)`
  padding: ${spacing1};
  cursor: pointer;

  > div {
    padding: ${spacing2};
    border: 1px solid transparent;
    min-height: ${spacing8};
    width: 100%;
  }

  > div:hover {
    border-radius: ${({ theme }) => theme.rounded};
    border: 1px solid #858585;
    background: white;
  }
`;
