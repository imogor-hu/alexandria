import { Meta, Story } from "@storybook/react";
import { Carousel } from "../../components/index.js";
import styled from "styled-components";
import { useState } from "react";

export default {
  title: "Layout/Carousel",
  component: Carousel,
} as Meta;

const Template: Story = () => {
  const [index, setIndex] = useState(0);

  return (
    <StyledCarousel index={index} onIndexChange={setIndex}>
      <CarouselItem style={{ backgroundColor: "red" }}>1</CarouselItem>
      <CarouselItem style={{ backgroundColor: "blue" }}>2</CarouselItem>
      <CarouselItem style={{ backgroundColor: "green" }}>3</CarouselItem>
      <CarouselItem style={{ backgroundColor: "purple" }}>4</CarouselItem>
      <CarouselItem style={{ backgroundColor: "orange" }}>5</CarouselItem>
    </StyledCarousel>
  );
};

const StyledCarousel = styled(Carousel)`
  width: 100%;
`;

const CarouselItem = styled.div`
  height: 250px;
  width: 100%;
  color: white;
  display: inline-flex;
  align-items: center;
  justify-content: center;
`;

export const Normal = Template.bind({});

Normal.parameters = {
  controls: { hideNoControlsWarning: true },
};
