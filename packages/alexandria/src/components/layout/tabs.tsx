import {
  Children,
  cloneElement,
  createContext,
  MouseEvent,
  ReactElement,
  ReactNode,
  useContext,
  useMemo,
  useRef,
  useState,
} from "react";
import { animated, useTransition } from "react-spring";
import { noop } from "lodash-es";
import { StyledIconBase } from "@styled-icons/styled-icon";
import { matchPath, useLocation, useNavigate, useParams } from "react-router-dom";
import { useAction } from "use-action";
import styled from "styled-components";
import { FontWeight, roundedLg, transition } from "../../themes/index.js";
import { LoadingIcon } from "../utils/loading-icon.js";

export const TabsContext = createContext<{
  activeIndex: number;
  setActiveIndex(value: number): void;
  extraTabProps(index: number): Record<string, unknown>;
}>({
  activeIndex: 0,
  setActiveIndex: noop,
  extraTabProps: defaultExtraTabProps,
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function defaultExtraTabProps(index: number) {
  return {};
}

export function Tabs({
  children,
  activeIndex: propActiveIndex,
  onChange = noop,
  extraTabProps = defaultExtraTabProps,
}: {
  children: ReactNode;
  activeIndex?: number;
  onChange?(newValue: number): void;
  extraTabProps?(index: number): Record<string, unknown>;
}) {
  const [stateActiveIndex, stateSetActiveIndex] = useState(0);

  let activeIndex: number, setActiveIndex: (newValue: number) => void;

  if (undefined !== propActiveIndex) {
    activeIndex = propActiveIndex;
    setActiveIndex = onChange;
  } else {
    activeIndex = stateActiveIndex;
    setActiveIndex = stateSetActiveIndex;
  }
  const contextValue = useMemo(() => ({ activeIndex, setActiveIndex, extraTabProps }), [activeIndex, setActiveIndex]);
  contextValue.extraTabProps = extraTabProps;

  return <TabsContext.Provider value={contextValue}>{children}</TabsContext.Provider>;
}

export function TabList({ children, className }: { children: ReactElement[]; className?: string }) {
  const { activeIndex, setActiveIndex, extraTabProps } = useContext(TabsContext);

  return (
    <StyledTabList className={className}>
      {Children.map(children, (child, index) =>
        cloneElement(child, {
          $isActive: activeIndex === index,
          onClick(event: MouseEvent<HTMLElement>) {
            event.preventDefault();
            setActiveIndex(index);
          },
          ...extraTabProps(index),
        })
      )}
    </StyledTabList>
  );
}

type PositionProperty = "absolute" | "static";

export function TabContents({ children }: { children: ReactElement[] }) {
  const { activeIndex } = useContext(TabsContext);
  const childrenArray = Children.toArray(children);
  const transitions = useTransition(activeIndex, {
    from: { position: "absolute" as PositionProperty, opacity: 0 },
    enter: { position: "static" as PositionProperty, opacity: 1 },
    leave: { position: "absolute" as PositionProperty, opacity: 0 },
  });

  return (
    <StyledTabContents>
      {transitions((style, item) => (
        <TabContent style={style}>{childrenArray[item]}</TabContent>
      ))}
    </StyledTabContents>
  );
}

export function RouteTabs({
  children,
  routes,
  onChange,
}: {
  children: ReactNode;
  routes: string[];
  onChange?(index: number): void;
}) {
  const navigate = useNavigate();
  const params = useParams<Record<string, string>>();
  const location = useLocation();
  const activeIndexRef = useRef(0);

  useAction(() => {
    activeIndexRef.current = getRouteIndex(location.pathname, routes);

    Promise.resolve().then(() => onChange?.(activeIndexRef.current));
  }, [location.pathname]);

  return (
    <Tabs
      activeIndex={activeIndexRef.current}
      extraTabProps={(index) => ({
        as: "a",
        href: resolveRoute(routes[index], params),
      })}
      onChange={(newIndex) => {
        const route = resolveRoute(routes[newIndex] || routes[0], params);

        navigate(route);
      }}
    >
      {children}
    </Tabs>
  );
}

function getRouteIndex(pathname: string, routes: string[]) {
  const index = routes.findIndex((route) => !!matchPath({ path: route, end: false }, pathname));
  return index < 0 ? 0 : index;
}

function resolveRoute(pathname: string, params: Partial<Record<string, string>>) {
  return pathname.replace(/:[a-zA-Z]+/, (match) => {
    const paramName = match.substring(1);

    if (params[paramName]) return params[paramName] as string;

    return match;
  });
}

export const TabContent = styled(animated.div)`
  top: 0;
  left: 0;
  width: 100%;
`;

const StyledTabContents = styled.div`
  position: relative;
`;

export const Tab = styled.div<{ $isActive?: boolean }>`
  display: inline-flex;
  align-items: center;
  font-size: ${({ theme }) => theme.textSm};
  font-weight: ${FontWeight.Medium};
  color: ${({ theme, $isActive }) => ($isActive ? theme.primary500 : theme.gray500)};
  cursor: pointer;
  border-bottom: 2px solid ${({ theme, $isActive }) => ($isActive ? theme.primary500 : "transparent")};
  transition: ${({ theme }) => transition(["color", "border-color"], theme.defaultTransitionTiming)};
  padding: ${({ theme }) => theme.spacing2} ${({ theme }) => theme.spacing2} ${({ theme }) => theme.spacing2}
    ${({ theme }) => theme.spacing1};
  /* To allow border overlap */
  margin-bottom: -1px;

  &:hover {
    color: ${({ theme }) => theme.primary500};
    text-decoration: none;
  }

  & + & {
    margin-left: ${({ theme }) => theme.spacing3};
  }

  ${StyledIconBase} {
    font-size: ${({ theme }) => theme.textXl};
    margin-right: ${({ theme }) => theme.spacing1};
  }
`;

const StyledTabList = styled.div`
  margin-bottom: ${({ theme }) => theme.spacing3};
  border-bottom: 1px solid ${({ theme }) => theme.gray400};
`;

export const TabNumber = styled.span<{ $slim?: boolean }>`
  font-size: ${({ theme }) => theme.textXs};
  margin-left: ${({ theme }) => theme.spacing2};
  padding: 0 ${({ theme, $slim }) => ($slim ? theme.spacing2 : theme.spacing3)};
  background-color: ${({ theme }) => theme.primary300};
  color: ${({ theme }) => theme.primary800};
  border-radius: ${roundedLg};
`;

export const TabLoadingIcon = styled(LoadingIcon)`
  font-size: ${({ theme }) => theme.textXs};
  margin-left: ${({ theme }) => theme.spacing2};
  color: ${({ theme }) => theme.primary600};
`;
