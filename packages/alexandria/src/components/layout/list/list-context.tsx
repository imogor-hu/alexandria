import { ListCardProps } from "./list-card.js";
import { createContext, useMemo } from "react";
import { noop } from "../../utils/noop.js";

export function defaultContextValue<T>() {
  return {
    currentPage: 0,
    onCurrentPageChange: noop,
    pageSize: 25,
    onPageSizeChange: noop,
    showPagination: false,
    isSearchLoading: false,
    onSearchChange: noop,
    items: [],
    columns: "auto",
    rowComponent: () => <></>,
    headerRowComponent: <></>,
  } as ListCardProps<T>;
}

export function createListPageContext<T>() {
  return useMemo(() => createContext<ListCardProps<T>>(defaultContextValue<T>()), []);
}
