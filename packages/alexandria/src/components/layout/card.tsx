import styled, { css } from "styled-components";
import { FontWeight, spacing2, spacing4, spacing6 } from "../../themes/index.js";
import { BaseThemeInterface } from "../../themes/base-theme.js";

export type KindType = "default" | "error";

const cardKindError = css`
  color: ${({ theme }) => theme.red700};
  background-color: ${({ theme }) => theme.red100};
  border: 1px solid ${({ theme }) => theme.red400};
`;

function getPadding(theme: BaseThemeInterface, padding: number | [number, number] = 6): string {
  if (typeof padding === "number") return theme[`spacing${padding}` as keyof BaseThemeInterface] as string;
  else return getPadding(theme, padding[0]) + " " + getPadding(theme, padding[1]);
}

export const Card = styled.div<{
  $kind?: KindType;
  $padding?: number | [number, number];
  $hideOverflow?: boolean;
  $small?: boolean;
  $extraSmall?: boolean;
}>`
  background-color: #fff;
  box-shadow: ${({ theme }) => theme.shadow};
  padding: ${({ theme, $padding }) => getPadding(theme, $padding)};
  border-radius: ${({ theme }) => theme.roundedLg};
  overflow: ${({ $hideOverflow }) => ($hideOverflow ? "hidden" : "initial")};

  ${({ $kind }) => $kind === "error" && cardKindError};
  ${({ $small }) => ($small ? "max-width: 45rem; margin-left: auto; margin-right: auto;" : "")}
  ${({ $extraSmall }) => ($extraSmall ? "max-width: 20rem; margin-left: auto; margin-right: auto;" : "")}
`;

export const CardTitle = styled.h3`
  font-size: ${({ theme }) => theme.textLg};
  color: ${({ theme }) => theme.gray900};
  line-height: ${({ theme }) => theme.leading6};
  font-weight: ${FontWeight.Medium};
  margin: 0 0 ${spacing2};
`;

export const cardNoPaddingTop = css`
  margin-top: -${spacing6};
`;

export const cardNoPaddingBottom = css`
  margin-bottom: -${spacing6};
`;

export const cardNoPaddingHorizontal = css`
  margin-left: -${spacing6};
  margin-right: -${spacing6};
`;

export const FormCardTitle = styled(CardTitle)`
  ${cardNoPaddingTop};
  ${cardNoPaddingHorizontal};
  margin-bottom: ${spacing4};
  padding: ${spacing4} ${spacing6};
  border-bottom: 1px solid ${({ theme }) => theme.gray200};
`;
