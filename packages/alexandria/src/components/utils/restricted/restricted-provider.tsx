import { Observable } from "rxjs";
import { createContext, ReactNode } from "react";

export enum AccessLevel {
  Normal = "normal",
  Admin = "admin",
}

export const accessLevelOrder = [AccessLevel.Normal, AccessLevel.Admin];

export interface RestrictedProps {
  level?: AccessLevel;
  guest?: boolean;
  any?: boolean;
  fallback?: ReactNode;
  loading?: ReactNode;
  children?: ReactNode;
  exact?: boolean;
}

type DefaultUserType = Record<string, unknown>;

interface IsGrantedFunction<TUser = DefaultUserType> {
  (
    args: RestrictedProps & {
      isEmptyUser: (user: TUser) => boolean;
      isGuestUser: (user: TUser) => boolean;
    },
    user: TUser
  ): boolean;
}

interface RestrictedContextValue<TUser = DefaultUserType> {
  user$: Observable<TUser>;
  defaultUser: TUser;
  isGranted: IsGrantedFunction<TUser>;
  isEmptyUser: (user: TUser) => boolean;
  isGuestUser: (user: TUser) => boolean;
}

export const RestrictedContext = createContext<RestrictedContextValue | null>(null);

interface RestrictedProviderProps<TUser = DefaultUserType> extends RestrictedContextValue<TUser> {
  children: ReactNode;
}

export function RestrictedProvider<TUser = DefaultUserType>({
  children,
  isGranted = defaultIsGranted as IsGrantedFunction<TUser>,
  ...props
}: RestrictedProviderProps<TUser>) {
  return (
    <RestrictedContext.Provider value={{ ...props, isGranted } as unknown as RestrictedContextValue}>
      {children}
    </RestrictedContext.Provider>
  );
}

interface IsGrantedOptions<TUser = DefaultUserType> extends RestrictedProps {
  level?: AccessLevel;
  guest?: boolean;
  exact?: boolean;
  isEmptyUser: (user: TUser) => boolean;
  isGuestUser: (user: TUser) => boolean;
}

export function defaultIsGranted<TUser = DefaultUserType>(
  { level = AccessLevel.Normal, guest, exact, isEmptyUser, isGuestUser }: IsGrantedOptions<TUser>,
  user: TUser
) {
  const userLevel = isUserAdmin(user) ? AccessLevel.Admin : AccessLevel.Normal;

  if (isEmptyUser(user)) return false;

  // If the page is only allowed for guests and the user is not a guest, or vice-versa, present fallback
  if (isGuestUser(user) !== !!guest) {
    return false;
  }

  if (guest) {
    return true;
  }

  if (exact) {
    return userLevel === level;
  }

  // If the user's organization type is lower than required, present fallback
  if (accessLevelOrder.indexOf(userLevel) < accessLevelOrder.indexOf(level || AccessLevel.Normal)) {
    return false;
  }

  // Permission checks passed, present gated content
  return true;
}

function isUserAdmin(user: unknown) {
  return (user as { isAdmin?: boolean })?.isAdmin;
}
