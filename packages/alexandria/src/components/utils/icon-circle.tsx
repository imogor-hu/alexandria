import { Error, ErrorCircle } from "@styled-icons/boxicons-regular";
import { size10, roundedFull } from "../../themes/index.js";
import styled, { css } from "styled-components";

export const iconCircleKinds = {
  danger: css`
    background-color: ${({ theme }) => theme.red100};
    color: ${({ theme }) => theme.red600};
  `,
  warning: css`
    background-color: ${({ theme }) => theme.yellow100};
    color: ${({ theme }) => theme.yellow800};
  `,
  get default() {
    return this.warning;
  },
};

export type IconCircleKind = keyof typeof iconCircleKinds;

export const IconCircle = styled.div<{ $kind?: IconCircleKind }>`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;
  width: ${size10};
  height: ${size10};
  border-radius: ${roundedFull};
  font-size: ${({ theme }) => theme.text2xl};

  ${({ $kind = "default" }) => iconCircleKinds[$kind]};
`;

export function getKindDefaultIcon(kind?: IconCircleKind) {
  switch (kind) {
    case "danger":
      return Error;
    case "warning":
    case "default":
    default:
      return ErrorCircle;
  }
}
