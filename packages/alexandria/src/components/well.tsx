import { spacing2, spacing4 } from "../themes/index.js";
import styled from "styled-components";

export const Well = styled.div`
  background-color: ${({ theme }) => theme.gray50};
  border: 1px solid ${({ theme }) => theme.gray100};
  border-radius: ${({ theme }) => theme.roundedLg};
  padding: ${spacing2} ${spacing4};
`;
