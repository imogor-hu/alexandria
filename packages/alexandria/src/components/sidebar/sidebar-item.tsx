import { ChevronLeft, Circle } from "@styled-icons/boxicons-regular";
import { ReactNode, useEffect, useRef } from "react";
import { StyledIcon, StyledIconBase } from "@styled-icons/styled-icon";
import { Link, LinkProps, NavLinkProps, useLocation, useResolvedPath } from "react-router-dom";
import styled from "styled-components";
import { transition } from "../../themes/index.js";
import { Collapsible, CollapsibleContent, useCollapsible } from "./collapsible.js";

interface SidebarItemProps {
  icon?: StyledIcon;
  name?: ReactNode;
  to?: string;
  children?: ReactNode;
}

export function SidebarItem({ icon: Icon = Circle, name, to, children }: SidebarItemProps) {
  if (to) {
    return (
      <SidebarItemContent as={SidebarLink} to={to} end>
        <Icon /> <span>{name}</span>
      </SidebarItemContent>
    );
  } else if (children) {
    return (
      <Collapsible>
        <SidebarItemLabel>
          <Icon /> <span>{name}</span> <ChevronLeft />
        </SidebarItemLabel>
        <CollapsibleContent>
          <SidebarCollapsibleContent>{children}</SidebarCollapsibleContent>
        </CollapsibleContent>
      </Collapsible>
    );
  }

  return null;
}

function SidebarItemLabel({ children }: { children: ReactNode }) {
  const { isOpen, setIsOpen } = useCollapsible();

  return (
    <SidebarItemContent className={isOpen ? "collapsible" : undefined} onClick={() => setIsOpen(!isOpen)}>
      {children}
    </SidebarItemContent>
  );
}

const SidebarItemContent = styled.div`
  display: flex;
  align-items: center;
  margin: 0 ${({ theme }) => theme.spacing2};
  padding: ${({ theme }) => theme.spacing2} ${({ theme }) => theme.spacing3};
  transition: ${({ theme }) => transition(["background-color", "color"], theme.defaultTransitionTiming)};
  color: ${({ theme }) => theme.gray200};
  outline: none;
  text-decoration: none !important;
  cursor: pointer;
  border-radius: ${({ theme }) => theme.roundedMd};
  margin-top: ${({ theme }) => theme.spacing2};

  &:active,
  &:hover,
  &:focus,
  &.active {
    color: white;
  }

  &.active:not(.collapsible) {
    background-color: ${({ theme }) => theme.primary500};
  }

  &.collapsible > ${StyledIconBase}:last-child:not(:first-child) {
    transform: rotate(-90deg);
  }

  > span {
    flex-grow: 1;
  }

  > ${StyledIconBase} {
    font-size: ${({ theme }) => theme.text2xl};
  }

  > ${StyledIconBase}:first-child {
    margin-right: ${({ theme }) => theme.spacing2};
  }

  > ${StyledIconBase}:last-child:not(:first-child) {
    transform: rotate(0deg);
    transition: ${({ theme }) => transition(["transform"], theme.defaultTransitionTiming)};
  }
`;

const SidebarCollapsibleContent = styled.div`
  overflow: hidden; // Prevent margin collapse
  padding: 0 0 0 ${({ theme }) => theme.spacing6};

  ${SidebarItemContent} {
    > ${StyledIconBase} {
      font-size: ${({ theme }) => theme.textXl};
    }
  }
`;

// Partially based on:
// https://github.com/remix-run/react-router/blob/d32662ddd67d489a46171b450ec933a0ce88d346/packages/react-router-dom/index.tsx#L300
function SidebarLink({ caseSensitive = false, end = false, className = "", ...props }: NavLinkProps) {
  const isInitialCall = useRef(true);

  const { setIsOpen } = useCollapsible();
  const location = useLocation();
  const path = useResolvedPath(props.to);

  let locationPathname = location.pathname;
  let toPathname = path.pathname;
  if (!caseSensitive) {
    locationPathname = locationPathname.toLowerCase();
    toPathname = toPathname.toLowerCase();
  }

  const isActive =
    locationPathname === toPathname ||
    (!end && locationPathname.startsWith(toPathname) && locationPathname.charAt(toPathname.length) === "/");

  // Only match if the match is either exact (/new routers will be exact) or the current url does not end in
  // "/new". This is so list links will not match "/new" urls.
  const markAsActive = isActive && (locationPathname === toPathname || !locationPathname.endsWith("/new"));

  useEffect(() => {
    if (isActive) setIsOpen(true, !isInitialCall.current);

    isInitialCall.current = false;
  }, [isActive]);

  return <Link className={className + (markAsActive ? " active" : "")} {...(props as LinkProps)} />;
}
