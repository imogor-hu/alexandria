import { StyledIconBase } from "@styled-icons/styled-icon";
import { UserCircle } from "@styled-icons/boxicons-solid";
import styled from "styled-components";
import { Button } from "../inputs/button.js";
import { LoadingIcon } from "../utils/loading-icon.js";

type SidebarProfileProps = {
  name: string;
  isLoading: boolean;
  onLogout: () => void;
};

export function SidebarProfile({ name, isLoading, onLogout }: SidebarProfileProps) {
  return (
    <StyledSidebarProfile>
      <SidebarProfileImage>
        <UserCircle />
      </SidebarProfileImage>
      <SidebarProfileDetails>
        <SidebarProfileName>{name}</SidebarProfileName>
        <Row>
          <LogoutButton disabled={isLoading} onClick={onLogout}>
            {isLoading && <LoadingIcon />}
            Kijelentkezés
          </LogoutButton>
        </Row>
      </SidebarProfileDetails>
    </StyledSidebarProfile>
  );
}

const Row = styled.div`
  display: flex;
`;

const StyledSidebarProfile = styled.div`
  display: flex;
  border-radius: 10px;
  align-items: center;
  background-color: ${(props) => props.theme.gray700};
  padding: ${({ theme }) => theme.spacing3};
  color: ${({ theme }) => theme.gray200};
`;

const SidebarProfileImage = styled.div`
  margin-right: ${({ theme }) => theme.spacing2};

  ${StyledIconBase} {
    font-size: ${({ theme }) => theme.text5xl};
  }
`;

const SidebarProfileDetails = styled.div`
  flex-grow: 1;
`;

const SidebarProfileName = styled.div`
  margin-bottom: ${({ theme }) => theme.spacing2};
`;

const LogoutButton = styled(Button).attrs({ $kind: "none", $size: "sm" })`
  background-color: ${({ theme }) => theme.gray600};
  border-color: transparent;
  color: ${({ theme }) => theme.gray200};

  &:hover {
    background-color: ${({ theme }) => theme.gray500};
    color: ${({ theme }) => theme.white};
  }
`;
