import { defaultRebootTheme } from "styled-reboot";

export const baseTheme = {
  ...defaultRebootTheme,

  breakpoints: {
    xs: "0px",
    sm: "576px",
    md: "768px",
    lg: "992px",
    xl: "1200px",
    xxl: "1400px",
  },

  // Reboot props
  fontSizeBase: "16px",

  successColor: "hsl(134,61%,41%)",
  defaultColor: "hsl(210,17%,98%)",
  boxShadow:
    "0 2px 2px 0 hsla(0, 0%, 0%, 0.14), 0 3px 1px -2px hsla(0, 0%, 0%, 0.12), 0 1px 5px 0 hsla(0, 0%, 0%, 0.2)",

  // Black n white
  black: "#000000",
  white: "#ffffff",

  // Primary colors
  primary50: "hsl(250,100%,98%)",
  primary100: "hsl(263,48%,95%)",
  primary200: "hsl(261,46%,87%)",
  primary300: "hsl(262,46%,79%)",
  primary400: "hsl(262,46%,63%)",
  primary500: "hsl(262,52%,47%)",
  primary600: "hsl(262,52%,42%)",
  primary700: "hsl(262,51%,28%)",
  primary800: "hsl(261,52%,21%)",
  primary900: "hsl(262,53%,14%)",

  // Red colors
  red50: "#fdf2f2",
  red100: "#fde8e8",
  red200: "#fbd5d5",
  red300: "#f8b4b4",
  red400: "#f98080",
  red500: "#f05252",
  red600: "#e02424",
  red700: "#c81e1e",
  red800: "#9b1c1c",
  red900: "#771d1d",

  // Gray colors
  gray50: "#f9fafb",
  gray100: "#EBEBEB",
  gray200: "#CECECE",
  gray300: "#B1B1B1",
  gray400: "#767676",
  gray500: "#3B3B3B",
  gray600: "#353535",
  gray700: "#232323",
  gray800: "#1B1B1B",
  gray900: "#121212",

  // Green colors
  green50: "#f3faf7",
  green100: "#def7ec",
  green200: "#bcf0da",
  green300: "#84e1bc",
  green400: "#31c48d",
  green500: "#0e9f6e",
  green600: "#057a55",
  green700: "#046c4e",
  green800: "#03543f",
  green900: "#014737",

  yellow50: "#fdfdea",
  yellow100: "#fdf6b2",
  yellow200: "#fce96a",
  yellow300: "#faca15",
  yellow400: "#e3a008",
  yellow500: "#c27803",
  yellow600: "#9f580a",
  yellow700: "#8e4b10",
  yellow800: "#723b13",
  yellow900: "#633112",

  // Danger colors
  danger100: "#FFF5F5",
  danger200: "#FED7D7",
  danger300: "#FEB2B2",
  danger400: "#FC8181",
  danger500: "#F56565",
  danger600: "#E53E3E",
  danger700: "#C53030",
  danger800: "#9B2C2C",
  danger900: "#742A2A",

  // Success colors
  success100: "#F0FFF4",
  success200: "#C6F6D5",
  success300: "#9AE6B4",
  success400: "#68D391",
  success500: "#48BB78",
  success600: "#38A169",
  success700: "#2F855A",
  success800: "#276749",
  success900: "#22543D",

  // zIndexes
  zIndexBackground: -1,
  zIndexNormal: 0,
  zIndexAboveNormal: 1,
  zIndexModalBackground: 2,
  zIndexModal: 3,
  zIndexDragLayer: 9,
  zIndexTitleBar: 10,

  // Spacings
  spacing0: "0",
  spacingPx: "1px",
  spacing1: "0.25rem",
  spacing2: "0.5rem",
  spacing3: "0.75rem",
  spacing4: "1rem",
  spacing5: "1.25rem",
  spacing6: "1.5rem",
  spacing8: "2rem",
  spacing10: "2.5rem",
  spacing12: "3rem",
  spacing16: "4rem",
  spacing20: "5rem",
  spacing24: "6rem",
  spacing32: "8rem",
  spacing40: "10rem",
  spacing48: "12rem",
  spacing56: "14rem",
  spacing64: "16rem",

  // Font sizes
  get textXs(): string {
    return `calc(${this.fontSizeBase} * 0.75)`;
  },
  get textSm(): string {
    return `calc(${this.fontSizeBase} * 0.875)`;
  },
  get textBase(): string {
    return this.fontSizeBase;
  },
  get textLg(): string {
    return `calc(${this.fontSizeBase} * 1.125)`;
  },
  get textXl(): string {
    return `calc(${this.fontSizeBase} * 1.25)`;
  },
  get text2xl(): string {
    return `calc(${this.fontSizeBase} * 1.5)`;
  },
  get text3xl(): string {
    return `calc(${this.fontSizeBase} * 1.875)`;
  },
  get text4xl(): string {
    return `calc(${this.fontSizeBase} * 2.25)`;
  },
  get text5xl(): string {
    return `calc(${this.fontSizeBase} * 3)`;
  },
  get text6xl(): string {
    return `calc(${this.fontSizeBase} * 4)`;
  },

  // App
  get bodyColor(): string {
    return this.primary900;
  },
  get bodyBg(): string {
    return this.primary100;
  },
  fontFamilyBase: "Inter",
  headingsFontFamily: "Metropolis",
  appBorderColor: "#424242",
  textColorOnPrimary: "#1c1c1c",
  defaultBorderRadius: "0.4rem",
  defaultTransitionTiming: "200ms cubic-bezier(0.0, 0, 0.2, 1)",
  slowTransitionTiming: "400ms cubic-bezier(0.0, 0, 0.2, 1)",
  fastTransitionTiming: "150ms cubic-bezier(0.0, 0, 0.2, 1)",

  fieldMaxWidthXs: "12.5rem", //200px
  fieldMaxWidth: "34rem", //544px
  fieldMaxWidthLg: "22rem", //352px
  fieldMaxWidthXl: "30.5rem", //488px
  fieldMaxWidth2Xl: "40.5rem", //648px
  fieldMaxWidth3Xl: "46rem", //736px
  fieldMaxWidth5Xl: "56em", //896px

  roundedNone: "0",
  roundedSm: "0.125rem",
  rounded: "0.25rem",
  roundedMd: "0.375rem",
  roundedLg: "0.5rem",
  roundedXl: "0.75rem",
  roundedFull: "9999px",

  // Box
  get boxBg(): string {
    return this.gray800;
  },

  get boxPadding(): string {
    return this.spacing8;
  },

  // General
  headerBackgroundColor: "#CCC",

  // Sidebar
  sidebarHeaderBackgroundColor: "#BBB",
  sidebarHoverBackgroundColor: "#CCC",
  activeSidebarLinkColor: "red",
  sidebarBackgroundColor: "#F4F5F6",

  // Page
  pageTitleBorderColor: "black",

  leadingNone: "1",
  leadingTight: "1.25",
  leadingSnug: "1.375",
  leadingNormal: "1.5",
  leadingRelaxed: "1.625",
  leadingLoose: "2",
  leading3: ".75rem",
  leading4: "1rem",
  leading5: "1.25rem",
  leading6: "1.5rem",
  leading7: "1.75rem",
  leading8: "2rem",
  leading9: "2.25rem",
  leading10: "2.5rem",

  shadowXs: "0 0 0 1px rgba(0,0,0,.05)",
  shadowSm: "0 1px 2px 0 rgba(0,0,0,.05)",
  shadow: "0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06)",
  shadowMd: "0 4px 6px -1px rgba(0,0,0,.1), 0 2px 4px -1px rgba(0,0,0,.06)",
  shadowLg: "0 10px 15px -3px rgba(0,0,0,.1),0 4px 6px -2px rgba(0,0,0,.05)",
  shadowXl: "0 20px 25px -5px rgba(0,0,0,.1), 0 10px 10px -5px rgba(0,0,0,.04)",
  shadow2xl: "0 0px 50px 0px rgba(0,0,0,.25)",
  shadowInner: "inset 0 2px 4px 0 rgba(0,0,0,.06)",
  shadowOutline: "0 0 0 3px rgba(118,169,250,.45)",
  shadowNone: "none",
  shadowUpper: "0 -1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06)",
};

export type BaseThemeInterface = typeof baseTheme;
