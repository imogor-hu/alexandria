export const maxWidthNone = "max-width: none;";
export const maxWidthXs = "max-width: 20rem;";
export const maxWidthSm = "max-width: 24rem;";
export const maxWidthMd = "max-width: 28rem;";
export const maxWidthLg = "max-width: 32rem;";
export const maxWidthXl = "max-width: 36rem;";
export const maxWidth2Xl = "max-width: 42rem;";
export const maxWidth3Xl = "max-width: 47rem;";
export const maxWidth4Xl = "max-width: 56rem;";
export const maxWidth5Xl = "max-width: 64rem;";
export const maxWidth6Xl = "max-width: 72rem;";
export const maxWidth7Xl = "max-width: 80rem;";
export const maxWidthFull = "max-width: 100%;";

export const roundedNone = "0";
export const roundedSm = "0.125rem";
export const rounded = "0.25rem";
export const roundedMd = "0.375rem";
export const roundedLg = "0.5rem";
export const roundedFull = "9999px";

export const size1 = ".25rem";
export const size2 = ".5rem";
export const size3 = ".75rem";
export const size4 = "1rem";
export const size5 = "1.25rem";
export const size6 = "1.5rem";
export const size7 = "1.75rem";
export const size8 = "2rem";
export const size9 = "2.25rem";
export const size10 = "2.5rem";
export const size11 = "2.75rem";
export const size12 = "3rem";
export const size13 = "3.25rem";
export const size14 = "3.5rem";
export const size15 = "3.75rem";
export const size16 = "4rem";
export const size20 = "5rem";
export const size24 = "6rem";
export const size28 = "7rem";
export const size32 = "8rem";
export const size36 = "9rem";
export const size40 = "10rem";
export const size48 = "12rem";
export const size56 = "14rem";
export const size60 = "15rem";
export const size64 = "16rem";
export const size68 = "17rem";
export const size72 = "18rem";
export const size80 = "20rem";
export const size88 = "22rem";
export const size96 = "24rem";
export const size120 = "30rem";
export const size140 = "35rem";
export const size180 = "45rem";
export const size220 = "55rem";
export const size320 = "80rem";
export const sizePx = "1px";

export const widthXs = "20rem";
export const widthMd = "28rem";
export const widthLg = "32rem";
export const widthXl = "36rem";
export const width2xl = "42rem";
export const width3xl = "48rem";
export const width4xl = "56rem";
export const width6xl = "72rem";
export const width7xl = "80rem";
export const width8xl = "90rem";

export const spacing0 = "0";
export const spacingPx = "1px";
export const spacing2Px = "2px";
export const spacing1 = "0.25rem";
export const spacing2 = "0.5rem";
export const spacing3 = "0.75rem";
export const spacing4 = "1rem";
export const spacing5 = "1.25rem";
export const spacing6 = "1.5rem";
export const spacing8 = "2rem";
export const spacing9 = "2.25rem";
export const spacing10 = "2.5rem";
export const spacing12 = "3rem";
export const spacing16 = "4rem";
export const spacing18 = "4.5rem";
export const spacing20 = "5rem";
export const spacing24 = "6rem";
export const spacing28 = "7rem";
export const spacing32 = "8rem";
export const spacing40 = "10rem";
export const spacing48 = "12rem";
export const spacing56 = "14rem";
export const spacing64 = "16rem";
export const spacing72 = "18rem";

export const spacing = {
  _0: spacing0,
  _1: spacing1,
  _2: spacing2,
  _3: spacing3,
  _4: spacing4,
  _6: spacing6,
  _8: spacing8,
  _9: spacing9,
  _10: spacing10,
  _12: spacing12,
  _16: spacing16,
  _18: spacing18,
  _20: spacing20,
  _24: spacing24,
  _28: spacing28,
  _32: spacing32,
  _40: spacing40,
  _48: spacing48,
  _56: spacing56,
  _64: spacing64,
  _72: spacing72,
};

export const zIndex0 = "0";
export const zIndex10 = "10";
export const zIndex20 = "20";
export const zIndex30 = "30";
export const zIndex40 = "40";
export const zIndex50 = "50";
export const zIndexAuto = "auto";

export const shadowXs = "0 0 0 1px rgba(0,0,0,.05)";
export const shadowSm = "0 1px 2px 0 rgba(0,0,0,.05)";
export const shadow = "0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06)";
export const shadowMd = "0 4px 6px -1px rgba(0,0,0,.1), 0 2px 4px -1px rgba(0,0,0,.06)";
export const shadowLg = "0 10px 15px -3px rgba(0,0,0,.1),0 4px 6px -2px rgba(0,0,0,.05)";
export const shadowXl = "0 20px 25px -5px rgba(0,0,0,.1), 0 10px 10px -5px rgba(0,0,0,.04)";
export const shadow2xl = "0 25px 50px -12px rgba(0,0,0,.25)";
export const shadowUpper2xl = "0 0px 50px 0px rgba(0,0,0,.25)";
export const shadowInner = "inset 0 2px 4px 0 rgba(0,0,0,.06)";
export const shadowOutline = "0 0 0 3px rgba(118,169,250,.45)";
export const shadowNone = "none";

export const textXs = `0.75rem`;
export const textSm = `0.875rem`;
export const textBase = `1rem`;
export const textLg = `1.125rem`;
export const textXl = `1.25rem`;
export const text2xl = `1.5rem`;
export const text3xl = `1.875rem`;
export const text4xl = `2.25rem`;
export const text5xl = `3rem`;
export const text6xl = `4rem`;

export function transition(props: string[], timing: string) {
  return props.map((prop) => `${prop} ${timing}`).join(",");
}
