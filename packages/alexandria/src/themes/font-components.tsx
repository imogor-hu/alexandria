import styled from "styled-components";
import { FontWeight } from "./font-weight.js";

export const Regular = styled.span`
  font-weight: ${FontWeight.Regular};
`;

export const Medium = styled.b`
  font-weight: ${FontWeight.Medium};
`;
