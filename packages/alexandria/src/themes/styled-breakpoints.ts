import { createStyledBreakpoints, CreateStyledBreakpointsOptions } from "styled-breakpoints/styled-breakpoints";

const { up, down, between, only } = createStyledBreakpoints({
  // This "breakpoints" prop isn't really being used, it's just bad typing from the library
  breakpoints: {},
  // This is only used is a theme is not found. We set it to empty, so if the theme isn't properly set, it's easier
  // to spot.
  defaultBreakpoints: {},
  // Use our location
  pathToMediaQueries: "breakpoints",
  errorPrefix: "[styled-breakpoints]: ",
} as CreateStyledBreakpointsOptions);

export { up, down, between, only };
