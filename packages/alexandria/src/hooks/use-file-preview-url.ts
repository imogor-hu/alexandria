import { useEffect, useState } from "react";
import { FileEntry } from "../components/form/file-upload/file-entry.js";

export function useFilePreviewUrl(file: Omit<FileEntry, "entryId" | "status">) {
  const [url, setUrl] = useState("");

  useEffect(() => {
    if (file.contentUrl) {
      setUrl(file.contentUrl);
    } else if (file.file) {
      setUrl(URL.createObjectURL(file.file));

      return () => URL.revokeObjectURL(url);
    }
  }, [file.file, file.contentUrl]);

  return url;
}
