import { MutableRefObject, useRef } from "react";
import { useAction } from "use-action";

export function useLazyRef<T>(callback: () => T) {
  const ref = useRef<T>() as MutableRefObject<T>;

  useAction(() => {
    ref.current = callback();
  }, []);

  return ref;
}
