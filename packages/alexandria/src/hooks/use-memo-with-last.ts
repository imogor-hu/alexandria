import { useMemo, useRef } from "react";

export function useMemoWithLast<TArgs extends Array<any>, TResult>(
  callback: (lastArgs: TArgs, lastResult: TResult | undefined) => TResult,
  args: TArgs
): TResult {
  const lastArgs = useRef(args);
  const lastResult = useRef<TResult | undefined>();
  return useMemo(() => {
    const temp = lastArgs.current;
    lastArgs.current = args;
    return (lastResult.current = callback(temp, lastResult.current));
  }, args);
}
