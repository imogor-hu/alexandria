import { MutableRefObject, Ref, useCallback, useMemo, useRef } from "react";
import { isRefObject } from "../utils/is-ref-object.js";

interface UseFocusOptions {
  ref?: Ref<HTMLElement>;
  onFocusChange?(focused: boolean): void;
}

export function useFocus(
  options: UseFocusOptions = {}
): [{ onFocus(): void; onBlur(): void; ref: Ref<any> }, (focus: boolean) => void] {
  const isFocusedRef = useRef(false);
  const currentRef = useRef<HTMLElement>(null) as MutableRefObject<HTMLElement | null>;

  const onFocusChangeCallbackRef = useRef(options.onFocusChange);
  onFocusChangeCallbackRef.current = options.onFocusChange;

  const setFocus = useCallback((value: boolean) => {
    if (!currentRef.current) return;

    if (value) currentRef.current.focus();
    else currentRef.current.blur();
  }, []);

  const props = useMemo(() => {
    function onFocusChange(value: boolean) {
      if (isFocusedRef.current !== value) {
        isFocusedRef.current = value;
        onFocusChangeCallbackRef.current && onFocusChangeCallbackRef.current(value);
      }
    }

    return {
      onFocus() {
        onFocusChange(true);
      },
      onBlur() {
        onFocusChange(false);
      },
      ref(refValue: HTMLElement | null) {
        if (options.ref) {
          if (typeof options.ref === "function") options.ref(refValue);
          else if (isRefObject(options.ref)) (options.ref as MutableRefObject<HTMLElement | null>).current = refValue;
        }

        currentRef.current = refValue;
        const newRefIsFocused = !!currentRef.current && document.activeElement === currentRef.current;

        onFocusChange(newRefIsFocused);
      },
    };
  }, [options.ref]);

  return [props, setFocus];
}
