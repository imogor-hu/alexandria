import { describe, it, expect, vi } from "vitest";
import { cleanup, render, userEvent } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { convertStringToValue, convertValueToString, FormTimeField, isValidStringTime } from "../fields/index.js";
import { waitFor } from "@testing-library/dom";

describe("Time field", () => {
  afterEach(cleanup);
  it("should work as a boolean control", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={(values) => onSubmit(values)} defaultValues={{ time: "16:00" }}>
        <FormTimeField name="time" label={"time"} />
        <button type="submit">Submit</button>
      </Form>
    );

    const input = screen.getByLabelText("time");
    await userEvent.click(input);
    await userEvent.clear(input);
    await userEvent.type(input, "20:00");

    const submitButton = screen.getByRole("button");
    await userEvent.click(submitButton);
    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({ time: "20:00" });
    });
  });

  it("should convert properly", () => {
    expect(convertStringToValue("22:00", null)).toEqual("22:00");
    expect(convertStringToValue("22:00", "")).toEqual("22:00");

    expect(convertStringToValue("22:00", new Date()).getHours()).toEqual(22);
    expect(convertStringToValue("22:00", new Date()).getMinutes()).toEqual(0);

    expect(convertStringToValue("asdf", null)).toEqual("asdf");
    expect(convertStringToValue("22:00222", null)).toEqual("22:00222");
    expect(convertStringToValue("22:00222", "22:00")).toEqual("22:00222");
  });

  it("should recognize valid stringtime", () => {
    expect(isValidStringTime("22:00222")).toBeFalsy();
    expect(isValidStringTime("22:00123")).toBeFalsy();
  });

  it.only("should convert value to string", () => {
    expect(convertValueToString("22:00123")).toEqual("22:00123");
  });
});
