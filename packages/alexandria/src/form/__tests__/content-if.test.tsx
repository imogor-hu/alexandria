import { describe, it, vi, afterEach } from "vitest";
import { cleanup, render, waitFor } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { FormInputField, FormSelectField } from "../fields/index.js";
import { select } from "react-select-event";
import { ContentIf } from "../fields/content-if.js";

describe("ContentIf field", () => {
  afterEach(cleanup);
  it("should show content only if condition applies", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit}>
        <FormSelectField name="category" options={["a", "b"]} />
        <ContentIf matches={{ name: "category", value: "a" }}>
          <FormInputField name="fieldOfCategoryA" />
        </ContentIf>
        <ContentIf matches={{ name: "category", value: "b" }}>
          <FormInputField name="fieldOfCategoryB" />
        </ContentIf>
        <button type="submit">Submit</button>
      </Form>
    );

    await select(screen.getByLabelText("category"), "a");
    await waitFor(() => {
      expect(screen.getByText("fieldOfCategoryA")).toBeInTheDocument();
      expect(screen.queryByText("fieldOfCategoryB")).not.toBeInTheDocument();
    });
    await select(screen.getByLabelText("category"), "b");
    await waitFor(() => {
      expect(screen.getByText("fieldOfCategoryB")).toBeInTheDocument();
      expect(screen.queryByText("fieldOfCategoryA")).not.toBeInTheDocument();
    });
  });
});
