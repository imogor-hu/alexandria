import { describe, it, expect, vi, afterEach } from "vitest";
import { cleanup, render, userEvent, waitForElementToBeRemoved } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { FormInputField } from "../fields/input-field.js";
import { waitFor } from "@testing-library/dom";
import { noop } from "../../components/utils/noop.js";

describe("Validation", () => {
  afterEach(cleanup);
  it("should validate as e-mail address if 'email' prop is present", async () => {
    const screen = render(
      <Form onSubmit={noop} defaultValues={{ email: "bob" }}>
        <FormInputField
          name="email"
          email="Please provide a valid e-mail address"
          required="Email address is required"
        />
        <button type="submit">Submit</button>
      </Form>
    );
    const submitButton = screen.getByRole("button");
    userEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByText("Please provide a valid e-mail address")).toBeInTheDocument();
    });
  });

  it("should enforce minLength prop", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit} defaultValues={{ password: "admin" }}>
        <FormInputField name="password" required type="password" minLength={6} />
        <button type="submit">Submit</button>
      </Form>
    );
    const submitButton = screen.getByRole("button");
    userEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByText(`Has to be at least 6 characters long`)).toBeInTheDocument();
    });
  });

  it("should use custom minLength message", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit} defaultValues={{ password: "admin" }}>
        <FormInputField
          name="password"
          required
          type="password"
          minLength={{ value: 6, message: "Please, at least 6 chars hon" }}
        />
        <button type="submit">Submit</button>
      </Form>
    );
    const submitButton = screen.getByRole("button");
    userEvent.click(submitButton);
    await waitFor(() => {
      expect(screen.getByText(`Please, at least 6 chars hon`)).toBeInTheDocument();
    });
  });
});
