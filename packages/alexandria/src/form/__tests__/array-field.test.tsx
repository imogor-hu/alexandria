import { describe, it, vi } from "vitest";
import { render, userEvent, waitFor } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { FormInputField } from "../fields/index.js";
import { FormArrayField } from "../fields/array-field.js";
import { FormManager } from "../form-manager.js";

describe("Array field", () => {
  // afterEach(cleanup);
  it("should allow adding and editing array elements", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit}>
        <FormArrayField name="elements">
          <FormInputField name="name" />
          <FormInputField name="age" type="number" />
        </FormArrayField>
        <button type="submit">Submit</button>
      </Form>
    );

    await userEvent.click(screen.getByText("Add"));
    await waitFor(() => {
      expect(screen.getByText("name")).toBeInTheDocument();
    });
    await userEvent.type(screen.getByText("name"), "Bob");
    await userEvent.type(screen.getByLabelText("age"), "23");
    await userEvent.click(screen.getByText("Submit"));
    await new Promise((resolve) => setTimeout(resolve, 1500));
    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({ elements: [{ name: "Bob", age: 23 }] }, expect.any(FormManager));
    });
  });
});
