import { describe, it, afterEach, expect, vi } from "vitest";
import { cleanup, render, userEvent, waitFor } from "../../test/test-utils.js";
import { Form, useForm } from "../form.js";
import { FormInputField } from "../fields/input-field.js";
import { FormManager } from "../form-manager.js";
import { noop } from "../../components/utils/noop.js";

describe("Form", () => {
  afterEach(cleanup);

  it("should capture inputted values and pass them to onSubmit when submitted", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit}>
        <FormInputField name="name" />
        <FormInputField name="password" />
        <button type="submit">Submit</button>
      </Form>
    );
    const nameField = screen.getByText("name");
    await userEvent.type(nameField, "Bob");
    const passwordField = screen.getByText("password");
    await userEvent.type(passwordField, "admin1234");
    const submitButton = screen.getByRole("button");
    await userEvent.click(submitButton);
    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({ name: "Bob", password: "admin1234" }, expect.any(FormManager));
    });
  });

  it("should fill in values from defaultValues prop", () => {
    const screen = render(
      <Form onSubmit={noop} defaultValues={{ name: "Bob" }}>
        <FormInputField name="name" />
      </Form>
    );
    const nameField = screen.getByDisplayValue("Bob");
    expect(nameField).toBeInTheDocument();
  });

  it("should work with deep values", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form defaultValues={{ topLevel: { nestedA: "a original", nestedB: "b original" } }} onSubmit={onSubmit}>
        <FormInputField name="topLevel.nestedA" label="A" />
        <FormInputField name="topLevel.nestedB" label="B" />
        <button type="submit">Submit</button>
      </Form>
    );
    const aField = screen.getByDisplayValue("a original");
    await userEvent.clear(aField);
    await userEvent.type(aField, "A changed");
    const bField = screen.getByDisplayValue("b original");
    await userEvent.clear(bField);
    await userEvent.type(bField, "B changed");
    await userEvent.click(screen.getByRole("button"));
    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith(
        { topLevel: { nestedA: "A changed", nestedB: "B changed" } },
        expect.any(FormManager)
      );
    });
  });

  it("should have a working 'submitForm' function", async () => {
    const CustomSubmitButton = () => {
      const [form] = useForm((manager) => [manager], []);
      return <div onClick={() => form.submitForm()}>Submit</div>;
    };

    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={(values) => onSubmit(values)}>
        <FormInputField name="text" />
        <CustomSubmitButton />
      </Form>
    );

    await userEvent.type(screen.getByLabelText("text"), "asdf");
    await userEvent.click(screen.getByText("Submit"));

    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({ text: "asdf" });
    });
  });
});
