import { describe, it, expect, vi, afterEach } from "vitest";
import { cleanup, render, userEvent, waitForElementToBeRemoved } from "../../test/test-utils.js";
import { Form } from "../form.js";
import { FormSwitchField } from "../fields/form-switch-field.js";
import { FormManager } from "../form-manager.js";
import { waitFor } from "@testing-library/dom";

describe("SwitchField", () => {
  afterEach(cleanup);
  it("should work as a boolean control", async () => {
    const onSubmit = vi.fn();
    const screen = render(
      <Form onSubmit={onSubmit} defaultValues={{ isAdmin: true }}>
        <FormSwitchField name="isAdmin" />
        <button type="submit">Submit</button>
      </Form>
    );

    await userEvent.click(screen.getByLabelText("isAdmin"));

    const submitButton = screen.getByRole("button");
    await userEvent.click(submitButton);
    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({ isAdmin: false }, expect.any(FormManager));
    });
  });
});
