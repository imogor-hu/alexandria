import { BehaviorSubject } from "rxjs";
import { array, BaseSchema, mixed, StringSchema } from "yup";
import { object, string } from "yup";
import { get, set, uniqueId } from "lodash-es";
import produce from "immer";
import fastDeepEqual from "fast-deep-equal";

export const validationOptionFields = ["email", "minLength", "required", "baseSchema"];

export const validationConfig = {
  defaultMessages: {
    required: "Required field",
    email: "Please provide a valid e-mail address",
    minLength: (number: number) => `Has to be at least ${number} characters long`,
  },
} as const;

export interface ValidationOptions {
  required?: string | boolean;
  email?: string | boolean;
  minLength?: number | { value: number; message: string };
  baseSchema?: BaseSchema;
}

export interface FormError {
  message: string;
}

interface FormState<T extends object = object> {
  values: T;
  errors: Record<string, FormError>;
  submitError: Error | null;
  isSubmitting: boolean;
}

export class FormManager<T extends object = object> {
  public readonly state$: BehaviorSubject<FormState<T>>;
  public readonly id: string;
  private readonly validationSettings: { [key: string]: ValidationOptions } = {};
  public schema: BaseSchema | null = null;

  private lastDefaultValues: T;
  onSubmit: (values: T, manager: FormManager<T>) => Promise<void> | void;

  public get values() {
    return this.state$.value.values;
  }

  public get errors() {
    return this.state$.value.errors;
  }

  public get submitError() {
    return this.state$.value.submitError;
  }

  public get isSubmitting() {
    return this.state$.value.isSubmitting;
  }

  public constructor(defaultValues: T = {} as T, onSubmit: FormManager<T>["onSubmit"]) {
    this.lastDefaultValues = defaultValues;
    this.id = uniqueId("form-");
    this.state$ = new BehaviorSubject<FormState<T>>({
      values: defaultValues,
      errors: {},
      submitError: null,
      isSubmitting: false,
    });
    this.onSubmit = onSubmit;
  }

  public setFieldValue<TValue>(name: string, value: TValue) {
    this.updateState((state) => {
      set(state.values, name, value);
    });

    if (this.getFieldError(name)) {
      // noinspection JSIgnoredPromiseFromCall
      this.validate();
    }
  }

  public getFieldValue<T>(name: string) {
    return get(this.values, name) as T;
  }

  public getFieldError(name: string) {
    return get(this.errors, name);
  }

  public setDefaultValues(value: T) {
    this.lastDefaultValues = value;
  }

  public resetValues(deferUpdate = false) {
    this.setValues(this.lastDefaultValues, deferUpdate);
  }

  public setValues(values: T, deferUpdate = false) {
    if (!fastDeepEqual(this.values, values)) {
      if (deferUpdate) {
        Promise.resolve().then(() => {
          this.updateState((state) => {
            state.values = values;
          });
        });
      } else {
        this.updateState((state) => {
          state.values = values;
        });
      }
    }
  }

  /**
   * @deprecated Use {@see values} instead.
   */
  public getValues() {
    return this.values;
  }

  public setIsSubmitting(newValue: boolean) {
    this.updateState((state) => {
      state.isSubmitting = newValue;
    });
  }

  public setSubmitError(error: Error | null) {
    this.updateState((state) => {
      state.submitError = error;
    });
  }

  public registerField(name: string, validationOptions: ValidationOptions) {
    const options = Object.fromEntries(
      Object.entries(validationOptions).filter(
        ([key, value]) => value !== undefined && validationOptionFields.includes(key)
      )
    );

    const previousOptions = get(this.validationSettings, name);
    set(this.validationSettings, name, { ...previousOptions, ...options });
  }

  public validate() {
    const schema = this.schema || assembleYupSchema(this.validationSettings);
    try {
      schema.validateSync(this.values, { abortEarly: false });
      this.updateState((state) => {
        state.errors = {};
      });
      return true;
    } catch (e) {
      const newErrors = {};
      for (const elem of (e as { inner: { errors: string[]; path: string }[] }).inner) {
        set(newErrors, elem.path, { message: elem.errors[0] });
      }
      this.updateState((state) => {
        state.errors = newErrors;
      });
      return false;
    }
  }

  private updateState(producer: (draft: FormState<T>) => void) {
    this.state$.next(produce(this.state$.getValue(), producer));
  }

  public submitForm = async () => {
    try {
      this.setSubmitError(null);
      this.setIsSubmitting(true);
      const isValid = await this.validate();
      if (isValid) {
        await this.onSubmit(this.values, this);
      }
    } catch (error) {
      console.error(error);
      this.setSubmitError(error as Error);
    } finally {
      this.setIsSubmitting(false);
    }
  };
}

function assembleYupSchema(input: Record<string, ValidationOptions> | Record<string, ValidationOptions>[]): BaseSchema {
  if ((input as Record<string, ValidationOptions>[])[0]) {
    return array();
    // This worked for an array of similar items, but I haven't yet found a way of allowing differently shaped items
    // with yup. Might have to switch to something else to handle validation later.
    // return array().of(assembleYupSchema((input as Record<string, ValidationOptions>[])[0]));
  }
  const fields = {};
  for (const [name, options] of Object.entries(input)) {
    // Don't validate anything on the field, if we have no validation settings to go on
    if (!options || !Object.values(options).some((v) => v !== undefined)) {
      set(fields, name, mixed());
      continue;
    }

    const schemaIfPossible = generateSchema(options as ValidationOptions);
    if (schemaIfPossible) {
      set(fields, name, schemaIfPossible);
    } else {
      set(fields, name, assembleYupSchema(options));
    }
  }
  return object().shape(fields);
}

function generateSchema({ baseSchema = string(), ...options }: ValidationOptions) {
  if ([...Object.keys(options)].some((elem) => !validationOptionFields.includes(elem))) {
    return null;
  }
  let schema = baseSchema;
  if (options.email) {
    schema = (schema as StringSchema).email(
      typeof options.email === "string" ? options.email : validationConfig.defaultMessages.email
    );
  }
  if (options.minLength) {
    schema = (schema as StringSchema).min(
      (options.minLength as { value: number })?.value || (options.minLength as number),
      (options.minLength as { message: string })?.message ||
        validationConfig.defaultMessages.minLength(options.minLength as number)
    );
  }
  if (options.required) {
    schema = schema.required(
      typeof options.required === "string" ? options.required : validationConfig.defaultMessages.required
    );
  }
  return schema;
}
