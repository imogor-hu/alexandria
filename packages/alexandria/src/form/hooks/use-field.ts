import { useCallback, useContext, useEffect, useRef } from "react";
import { useFormContext } from "../form.js";
import { FormError, ValidationOptions } from "../form-manager.js";
import { distinctUntilChanged, Observable, tap } from "rxjs";
import { useObservable } from "@litbase/use-observable";
import { map, switchMap } from "rxjs/operators";
import useConstant from "use-constant";
import { identity } from "lodash-es";
import { useRefValue } from "../../hooks/index.js";
import { FormSectionContext, FormSectionOptions } from "../form-section.js";

export interface UseFieldValue<TValue> {
  value: TValue;
  setValue: (value: TValue) => void;
  error?: FormError;
  id: string;
  section: FormSectionOptions;
  fullName: string;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface UseFieldOptions<TOutside = any, TInside = any> extends ValidationOptions {
  onValueRead?(value: TOutside): TInside;
  onValueWrite?(value: TInside): TOutside;
}

export function useField<TValue, TOutside = TValue>(
  inputName: string,
  { onValueRead = identity, onValueWrite = identity, ...options }: UseFieldOptions<TOutside, TValue> = {}
): UseFieldValue<TValue> {
  const manager = useFormContext();
  const section = useContext(FormSectionContext);
  const name = `${section.namePrefix || ""}${inputName}${section.nameAffix || ""}`;
  const nameRef = useRefValue(name);
  const onValueReadRef = useRefValue(onValueRead);
  const onValueWriteRef = useRefValue(onValueWrite);

  if (!manager) {
    throw new Error("UseField used outside of context. Did you forget to place a <Form> component?");
  }

  useEffect(() => {
    if (options) {
      manager.registerField(name, options);
    }
  }, [name, options]);

  const lazy = useLazy(
    manager.state$.pipe(
      map(() => ({
        value: onValueReadRef.current(manager.getFieldValue<TOutside>(name)),
        error: manager.getFieldError(name),
      }))
    ),
    [name]
  );

  const setValue = useCallback(
    (value: TValue) => manager.setFieldValue(nameRef.current, onValueWriteRef.current(value)),
    []
  );

  return {
    get value() {
      return lazy.value;
    },
    get error() {
      return lazy.error;
    },
    setValue,
    id: manager.id + "-" + name,
    section,
    fullName: name,
  };
}

type GenericObject = Record<string | symbol, unknown>;

// Watch the given observable and only rerender when previously accessed fields change.
export function useLazy<T>(observable$: Observable<T>, deps: unknown[] = []) {
  // Fields that will trigger a re-render
  const watchedFields = useConstant<Set<string | symbol>>(() => new Set());
  // The wrapper proxy we always return
  const proxy = useConstant(
    () =>
      new Proxy(
        {},
        {
          get(target, property) {
            watchedFields.add(property);

            return valueRef.current[property];
          },
        }
      )
  );
  // Latest value received from observable
  const valueRef = useRef<GenericObject>({});

  const observableRef = useRefValue(observable$);
  // Note: we only use useObservable to trigger a rerender, when appropriate. We always return the proxy object.
  useObservable(
    (inputs$) =>
      inputs$.pipe(
        switchMap(() =>
          observableRef.current.pipe(
            // Always update valueRef with the latest value, so the proxy can instantly return the latest fields
            tap((currentValue) => (valueRef.current = currentValue as GenericObject)),
            distinctUntilChanged((previous, current) => {
              // If any of the watched fields changed, emit, otherwise don't
              for (const field of watchedFields.values()) {
                if ((previous as GenericObject)?.[field] !== (current as GenericObject)?.[field]) {
                  return false;
                }
              }

              return true;
            })
          )
        )
      ),
    null,
    deps
  );

  return proxy as T;
}
