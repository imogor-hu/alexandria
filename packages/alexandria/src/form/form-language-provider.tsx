import { ComponentType, createContext, ReactNode, useMemo } from "react";

export interface FormLanguageState {
  flags: Record<string, ComponentType<"svg">>;
  language: string;
}

export const FormLanguageContext = createContext<FormLanguageState>({ flags: {}, language: "en" });

export interface FormLanguageProviderProps extends FormLanguageState {
  children?: ReactNode;
}

export function FormLanguageProvider({ flags, language, children }: FormLanguageProviderProps) {
  const contextValue = useMemo<FormLanguageState>(
    () => ({
      flags,
      language,
    }),
    [flags, language]
  );

  return <FormLanguageContext.Provider value={contextValue}>{children}</FormLanguageContext.Provider>;
}
