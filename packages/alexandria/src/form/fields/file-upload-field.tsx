import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";
import { FileUploadUrl, FileUploadUrlProps } from "../../components/form/file-upload/file-upload-url.js";
import { useField } from "../hooks/use-field.js";
import { mixed } from "yup";

type UploadFieldProps = Omit<FileUploadUrlProps, "value" | "onChange" | "onFilesChange">;

export function FormFileUploadField({
  name,
  limit,
  ...props
}: FieldProps & Omit<FileUploadUrlProps, "value" | "onChange">) {
  const { value, setValue, id } = useField<string[]>(name, props);

  return (
    <FormFieldWrapper name={name} baseSchema={mixed()} {...props}>
      <FileUploadUrl
        value={value}
        id={id}
        onChange={(value) => {
          setValue(value);
        }}
        limit={limit}
        {...props}
      />
    </FormFieldWrapper>
  );
}

export function FormSingleFileUploadField({ name, minLength, required, ...props }: FieldProps & UploadFieldProps) {
  const { value, setValue, id } = useField<string>(name, { minLength, required, ...props });

  return (
    <FormFieldWrapper name={name} {...props}>
      <FileUploadUrl limit={1} id={id} {...props} value={value ? [value] : []} onChange={(urls) => setValue(urls[0])} />
    </FormFieldWrapper>
  );
}
