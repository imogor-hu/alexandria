import { FieldProps } from "../field-wrapper.js";
import { BinaryId, DocumentInterface, MultiPromiseQueryObject } from "@litbase/core";
import { FormSelectField } from "./select-field.js";
import { useQuery } from "@litbase/react";
import { useState } from "react";

export function FormDocumentSelectField<T extends DocumentInterface>({
  collection,
  fields,
  ...props
}: FieldProps & {
  collection: string;
  getLabel: (doc: T | null) => string;
  fields?: Record<string, unknown>;
  searchFields?: string[];
}) {
  const [query, setQuery] = useState("");
  const [docs] = useQuery<T>(collection, { $matchAll: {}, ...fields } as MultiPromiseQueryObject);

  return (
    <FormSelectField<BinaryId | undefined, T>
      options={docs}
      getValue={(elem) => elem?._id}
      valueToValueString={(_id) => _id?.value || ""}
      optionToValueString={(elem) => elem._id.value}
      inputValue={query}
      onInputChange={setQuery}
      {...props}
    />
  );
}
