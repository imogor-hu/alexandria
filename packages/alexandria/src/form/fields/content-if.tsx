import { ReactNode } from "react";
import { useField } from "../hooks/use-field.js";

interface Matcher {
  name: string;
  value: (() => boolean) | unknown;
}

export function ContentIf({ matches, children }: { matches: Matcher; children: ReactNode }) {
  const { value } = useField(matches.name);

  if (!checkMatch(matches, value)) {
    return null;
  }
  return <>{children}</>;
}

function checkMatch(matcher: Matcher, value: unknown) {
  if (typeof matcher.value === "function") {
    return matcher.value(value);
  }
  return matcher.value === value;
}
