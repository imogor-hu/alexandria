import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";
import { RichTextEditor, RichTextEditorProps } from "../../components/index.js";
import { string } from "yup";
import { useField } from "../hooks/use-field.js";

export function FormRichTextField({
  name,
  isVisibleTabs,
  config,
  onReady,
  getDownloadUrl,
  onUploadFile,
  ...props
}: FieldProps & Omit<RichTextEditorProps, "value" | "onChange">) {
  const { value, setValue } = useField<string>(name, props);

  return (
    <FormFieldWrapper name={name} baseSchema={string()} {...props}>
      <RichTextEditor
        value={value}
        onChange={setValue}
        isVisibleTabs={isVisibleTabs}
        config={config}
        onReady={onReady}
        getDownloadUrl={getDownloadUrl}
        onUploadFile={onUploadFile}
      />
    </FormFieldWrapper>
  );
}
