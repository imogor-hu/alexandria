import { useField } from "../hooks/use-field.js";
import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";
import { DateInput } from "../../components/form/date-field.js";

export function FormDateField({ name, ...props }: FieldProps) {
  const { value, setValue, error } = useField<Date>(name, props);

  return (
    <FormFieldWrapper name={name} {...props}>
      <DateInput hasError={!!error} value={value} onChange={(event) => setValue(event.target.value)} />
    </FormFieldWrapper>
  );
}
