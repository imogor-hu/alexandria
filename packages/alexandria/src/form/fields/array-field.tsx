import { Button } from "../../components/index.js";
import { ComponentProps, ComponentPropsWithoutRef, createContext, ReactNode, useContext } from "react";
import { array } from "yup";
import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";
import styled from "styled-components";
import { spacing } from "../../themes/index.js";
import { ChevronDown, ChevronUp, Plus, Trash } from "@styled-icons/boxicons-regular";
import { AutoAnimatedDiv } from "../../components/utils/auto-animated-div.js";
import { useField } from "../hooks/use-field.js";
import { FormSection } from "../form-section.js";

interface ArrayFieldProps<T> extends FieldProps {
  children?: ReactNode;
  buttonText?: string | ReactNode;
  className?: string;
  containerProps?: ComponentPropsWithoutRef<"div">;
  createEmpty?: () => T;
  addButtonProps?: ComponentProps<typeof Button>;
}

const VariantContext = createContext("a");

export function FormArrayField<T>({
  name,
  children,
  className,
  buttonText = "Add",
  containerProps,
  createEmpty = () => ({} as unknown as T),
  addButtonProps,
  ...props
}: ArrayFieldProps<T>) {
  const { value, setValue } = useField<T[]>(name, props);
  const variant = useContext(VariantContext);

  return (
    <FormFieldWrapper name={name} baseSchema={array()} {...containerProps} {...props}>
      <VariantContext.Provider value={variant === "a" ? "b" : "a"}>
        <Container
          data-variant={variant}
          style={{ backgroundColor: variant === "a" ? "whitesmoke" : "white" }}
          className={className}
        >
          {value?.map((elem, index) => (
            <FormSection key={index} namePrefix={name + "." + index + "."}>
              <ElemContainerRow>
                <ContentContainer>{children}</ContentContainer>
                <RemoveCol>
                  {index > 0 && (
                    <ChevronUp
                      onClick={() =>
                        setValue([
                          ...(value || []).slice(0, index - 1),
                          ...(value || []).slice(index - 1, index + 1).reverse(),
                          ...(value || []).slice(index + 1),
                        ])
                      }
                    />
                  )}
                  {index < (value?.length || 0) - 1 && (
                    <ChevronDown
                      onClick={() =>
                        setValue([
                          ...(value || []).slice(0, index),
                          ...(value || []).slice(index, index + 2).reverse(),
                          ...(value || []).slice(index + 2),
                        ])
                      }
                    />
                  )}
                  <Trash onClick={() => setValue(value?.filter((elem, i) => index !== i) || [])} />
                </RemoveCol>
              </ElemContainerRow>
            </FormSection>
          ))}
          <Button
            $size="sm"
            type="button"
            onClick={() => setValue([...(value || []), createEmpty()])}
            {...addButtonProps}
          >
            <Plus /> {buttonText}
          </Button>
        </Container>
      </VariantContext.Provider>
    </FormFieldWrapper>
  );
}

const ContentContainer = styled(AutoAnimatedDiv)`
  width: 100%;
  min-width: 0;
`;

const RemoveCol = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-left: ${spacing._2};
  font-size: 22px;
  svg {
    :last-child {
      color: red;
      :hover {
        color: lightpink;
      }
    }
    :hover {
      color: gray;
    }
    cursor: pointer;
    transition: color 0.3s;
  }
`;

const ElemContainerRow = styled.div`
  padding: ${spacing._6} 0;
  & + & {
    border-top: 1px solid ${({ theme }) => theme.gray200};
  }
  display: flex;
  > div:first-child {
    flex: 1;
  }
`;

const Container = styled(AutoAnimatedDiv)`
  display: block;
  background: ${({ theme }) => theme.gray50};
  padding: ${spacing._4};
  border-radius: ${({ theme }) => theme.rounded};
  > div:first-child {
    padding-top: 0;
  }
  > *:first-child {
    margin-top: 0;
  }
  border: 1px solid ${({ theme }) => theme.gray300};
  & > * & {
    background: white;
  }
`;
