import { ComponentPropsWithoutRef, ReactNode, useCallback } from "react";
import styled from "styled-components";
import { FontWeight, transition } from "../../themes/index.js";
import { FieldProps, FormFieldWrapper } from "../field-wrapper.js";
import { useField } from "../hooks/use-field.js";
import { ChangeEvent } from "react";
import { FieldLabel } from "../../components/index.js";

export interface FormCheckboxFieldProps
  extends ComponentPropsWithoutRef<"input">,
    Omit<FieldProps, "minLength" | "required" | "name"> {
  name: string;
  checkboxLabel?: ReactNode;
}

export function FormCheckboxField({
  name,
  checkboxLabel,
  description,
  labelFallsBackToName = true,
  label,
  ...props
}: FormCheckboxFieldProps) {
  const { value, setValue, id } = useField<boolean>(name);

  const onChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.currentTarget.checked);
  }, []);

  return (
    <FormFieldWrapper
      label={label as ReactNode}
      name={name}
      description={description as ReactNode}
      labelFallsBackToName={labelFallsBackToName as boolean}
    >
      <StyledCheckboxField>
        <StyledCheckbox type="checkbox" {...props} checked={value} onChange={onChange} id={id} />
        {checkboxLabel && <CheckboxFieldLabel htmlFor={id}>{checkboxLabel}</CheckboxFieldLabel>}
      </StyledCheckboxField>
    </FormFieldWrapper>
  );
}

const StyledCheckboxField = styled.div`
  display: flex;
  align-items: center;
`;

const StyledCheckbox = styled.input`
  appearance: none;
  display: inline-block;
  width: 1rem;
  height: 1rem;
  color: ${({ theme }) => theme.primary500};
  color-adjust: exact;
  border: 1px solid ${({ theme }) => theme.gray300};
  border-radius: ${({ theme }) => theme.rounded};
  transition: ${({ theme }) => transition(["background-color", "border-color"], theme.defaultTransitionTiming)};

  &:checked {
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg viewBox='0 0 16 16' fill='%23fff' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M5.707 7.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4a1 1 0 00-1.414-1.414L7 8.586 5.707 7.293z'/%3E%3C/svg%3E");
    border-color: transparent;
    background-color: currentColor;
    background-size: 100% 100%;
    background-position: 50%;
    background-repeat: no-repeat;
  }
}
`;

const CheckboxFieldLabel = styled(FieldLabel)`
  display: inline-block;
  margin: 0 ${({ theme }) => theme.spacing2};
  font-weight: ${FontWeight.Regular};
`;
