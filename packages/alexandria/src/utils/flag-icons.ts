import { HU, GB, CN, KR, VN, JP } from "country-flag-icons/react/3x2";

export const flagIcons: { [code: string]: typeof HU } = {
  hu: HU,
  en: GB,
  cn: CN,
  kr: KR,
  vn: VN,
  jp: JP,
};
