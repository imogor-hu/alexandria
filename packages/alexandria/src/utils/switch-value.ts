export const DefaultCase = Symbol("SwitchValueDefault");

export function switchValue<T>(value: unknown, entries: Record<string | typeof DefaultCase, T>): T | null;
export function switchValue<T>(value: unknown, entries: [unknown, T][]): T | null;
export function switchValue<T>(
  value: unknown,
  entries: Record<string | typeof DefaultCase, T> | [unknown, T][]
): T | null {
  if (Array.isArray(entries)) {
    for (const [entryValue, entryElement] of entries) {
      if (value === entryValue) {
        return entryElement;
      }
    }

    const defaultValue = entries.find((e) => e[0] === DefaultCase)?.[1];

    return defaultValue !== undefined ? defaultValue : null;
  } else {
    const valueAsString = String(value);
    if (valueAsString in entries) {
      return entries[valueAsString];
    }

    if (DefaultCase in entries) {
      return entries[DefaultCase];
    }

    return null;
  }
}
