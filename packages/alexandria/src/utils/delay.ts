export function delay(waitMs: number): Promise<undefined>;
export function delay<T>(waitMs: number, value: T): Promise<T>;
export function delay<T>(waitMs: number, value?: T) {
  return new Promise<T>((resolve) => setTimeout(() => resolve(value as T), waitMs));
}

export function delayReject<T extends Error>(waitMs: number, error: T) {
  return new Promise<T>((resolve, reject) => setTimeout(() => reject(error), waitMs));
}
