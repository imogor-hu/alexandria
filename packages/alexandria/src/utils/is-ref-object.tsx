import { MutableRefObject, Ref } from "react";

export function isRefObject<T>(ref?: Ref<T>): ref is MutableRefObject<T | null> {
  return !!ref && "current" in ref;
}
