declare module "@ckeditor/ckeditor5-react" {
  import { Class } from "utility-types";
  import { Component } from "react";

  export interface FileLoaderInterface {
    file: Promise<File | null>;
    uploadTotal?: number;
    uploaded?: number;
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface Adapter {}

  interface Repository {
    createUploadAdapter: (loader: FileLoaderInterface) => Adapter;
  }

  export interface Editor {
    getData(): string;
    plugins: { get: (name: "FileRepository") => Repository };
    ui: {
      getEditableElement(): HTMLElement;
      view: { toolbar: { element: HTMLElement } };
    };
  }

  // TODO finish typing for Editor and Config
  export interface CKEditorProps {
    editor: Class<Editor>;
    data: string;
    config?: Record<string, unknown>;
    onReady?: (editor: Editor) => void;
    onChange?: (eventInfo: EventInfo, editor: Editor) => void;
    onBlur?: (editor: Editor) => void;
    onFocus?: (editor: Editor) => void;
    disabled?: boolean;
  }

  export interface EventInfo {
    name: string;
    path: Record<string, unknown>[];
    return: unknown;
    source: Record<string, unknown>;
  }

  export class CKEditor extends Component<CKEditorProps, {}> {
    public constructor(props, context);
  }
}
