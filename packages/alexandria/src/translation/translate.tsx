import {
  BehaviorSubject,
  combineLatest,
  firstValueFrom,
  from,
  Observable,
  of,
  shareReplay,
  switchMap,
  tap,
} from "rxjs";
import { map } from "rxjs/operators";
import { LanguageSettings, TranslationEntry, TranslationInput, TranslationTable } from "./translation-interface.js";
import { isPlainObject } from "@litbase/core";
import { merge } from "lodash-es";
import { Interweave } from "interweave";
import { ReactElement } from "react";

export interface GetTranslationOptions {
  params?: Record<string, string | number>;
  translations?: TranslationTable;
  fallbackTranslations?: TranslationTable;
  settings?: LanguageSettings;
  disableHtml?: boolean;
}

const defaultLanguageSettings: LanguageSettings = {
  language: "en",
  fallbackLanguage: "en",
  forcePlaceholders: false,
  disableFallback: false,
  translationLoader: () => ({}),
};

export const currentLanguageSettings$ = new BehaviorSubject(defaultLanguageSettings);

let currentTranslations: TranslationTable = {};
let fallbackTranslations: TranslationTable = {};
export const currentTranslations$: Observable<TranslationTable> = currentLanguageSettings$.pipe(
  switchMap(({ fallbackLanguage, forcePlaceholders, disableFallback, language, translationLoader }) => {
    if (forcePlaceholders) return of({});

    const translationsObject = translationLoader(language);
    const translations$ = isPlainObject(translationsObject) ? of(translationsObject) : from(translationsObject);

    if (!disableFallback && language !== fallbackLanguage) {
      const fallbackTranslationsObject = translationLoader(fallbackLanguage);

      return combineLatest([
        translations$,
        isPlainObject(fallbackTranslationsObject) ? of(fallbackTranslationsObject) : from(fallbackTranslationsObject),
      ]).pipe(
        tap(([, _fallbackTranslations]) => {
          fallbackTranslations = _fallbackTranslations;
        }),
        map(
          ([translations, fallbackTranslations]) =>
            merge({}, fallbackTranslations || {}, translations || {}) as TranslationTable
        )
      );
    }

    return translations$;
  }),
  shareReplay({ bufferSize: 1, refCount: false })
);

currentTranslations$.subscribe((translations) => {
  currentTranslations = translations;
});

export function setLanguageSettings(settings: Partial<LanguageSettings>) {
  currentLanguageSettings$.next({ ...getLanguageSettings(), ...settings });
}

export function getLanguageSettings() {
  return currentLanguageSettings$.getValue();
}

export function getCurrentTranslations() {
  return currentTranslations;
}

export function getFallbackTranslations() {
  return fallbackTranslations;
}

export function getTranslation(value: TranslationInput, options: GetTranslationOptions & { disableHtml: true }): string;
export function getTranslation(value: TranslationInput, options?: GetTranslationOptions): string | ReactElement;
export function getTranslation(value: TranslationInput, options: GetTranslationOptions = {}): string | ReactElement {
  if (!value) return "";

  const {
    translations = getCurrentTranslations(),
    fallbackTranslations = getFallbackTranslations(),
    settings = getLanguageSettings(),
    params = {},
  } = options;
  const { language, fallbackLanguage } = settings;

  let translation: TranslationEntry;
  if (typeof value === "string") {
    const trimmedValue = value.trim();
    translation = (getIn(trimmedValue, translations) || getIn(trimmedValue, fallbackTranslations)) as unknown as
      | string
      | undefined;
  } else if (typeof value === "object") {
    translation = value[language] || value[fallbackLanguage];
  }

  const foundValue = translation ? String(translation) : `__${value}__`;

  let finalTranslation: string | ReactElement = replaceTranslationParams(foundValue, params);

  if (!options.disableHtml) {
    finalTranslation = createHtmlTranslationIfNeeded(finalTranslation);
  }

  return finalTranslation;
}

function getIn(path: string | string[], obj: Record<string, unknown>) {
  const explodedPath = Array.isArray(path) ? path : path.split(".");

  let currentParent: Record<string, unknown> = obj;

  for (const segment of explodedPath) {
    const newParent = currentParent[segment];

    if (null == newParent) return newParent;

    currentParent = newParent as Record<string, unknown>;
  }

  return currentParent;
}

function replaceTranslationParams(value: string, params: Record<string, string | number>) {
  return value.replace(/{{\s*([a-zA-Z0-9.]+)\s*}}/g, (match, p1) => {
    return String(getIn(p1, params));
  });
}

function createHtmlTranslationIfNeeded(value: string) {
  // Check if the string contains any html tags, like <p>. If not, no processing is needed.
  if (!/<\/?[a-z][\s\S]*>/i.test(value)) {
    return value;
  }

  return <Interweave content={value} />;
}
