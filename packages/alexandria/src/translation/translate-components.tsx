import { FunctionComponent, memo, ReactElement, ReactNode, useCallback } from "react";
import { useObservable } from "@litbase/use-observable";
import { TranslationInput, TranslationParams } from "./translation-interface.js";
import {
  currentLanguageSettings$,
  currentTranslations$,
  getCurrentTranslations,
  getLanguageSettings,
  getTranslation,
  GetTranslationOptions,
} from "./translate.js";

export interface UseTranslateFunction {
  (value: TranslationInput, params: TranslationParams, options: GetTranslationOptions & { disableHtml: true }): string;

  (value: TranslationInput, params?: TranslationParams, options?: GetTranslationOptions): ReactElement | string;
}

export interface UseTranslateStringFunction {
  (value: TranslationInput, params?: TranslationParams, options?: GetTranslationOptions): string;
}

export function useTranslation(
  value: TranslationInput,
  params: TranslationParams,
  options: GetTranslationOptions & { disableHtml: true }
): string;
export function useTranslation(value: TranslationInput, params?: TranslationParams): string | ReactElement;
export function useTranslation(
  value: TranslationInput,
  params?: TranslationParams,
  options?: GetTranslationOptions
): string | ReactElement {
  const translate = useTranslate();

  return translate(value, params, options);
}

export function useTranslations() {
  const [translationTable] = useObservable(() => currentTranslations$, getCurrentTranslations(), []);
  return translationTable;
}

export function useLanguageSettings() {
  const [settings] = useObservable(() => currentLanguageSettings$, getLanguageSettings());
  return settings;
}

export function useLanguage() {
  const { language } = useLanguageSettings();
  return language;
}

export function useTranslate() {
  const translations = useTranslations();
  const settings = useLanguageSettings();

  return useCallback(
    (value: TranslationInput, params?: TranslationParams, options?: GetTranslationOptions) =>
      getTranslation(value, { translations, settings, params, ...options }),
    [translations, settings]
  ) as UseTranslateFunction;
}

export function useTranslateString() {
  const translations = useTranslations();
  const settings = useLanguageSettings();

  return useCallback(
    (value: TranslationInput, params?: TranslationParams, options?: GetTranslationOptions) =>
      getTranslation(value, { translations, settings, params, disableHtml: true, ...options }),
    [translations, settings]
  ) as UseTranslateStringFunction;
}

export type TranslateProps = {
  [key: string]: string | number | string[] | TranslationInput;
} & { children?: TranslationInput | TranslationInput[] };

const devModeEnabled = typeof localStorage !== "undefined" && !!localStorage.getItem("translationDevMode");

function TranslateComponent({ children, ...params }: TranslateProps) {
  const key = Array.isArray(children) ? children.join("") : children;

  const translation = useTranslation(key, params as TranslationParams);

  if (devModeEnabled) {
    return <span title={String(key)}>{translation}</span>;
  }

  return translation as ReactNode;
}

export const Translate = memo(TranslateComponent as FunctionComponent<TranslateProps>);
export const T = Translate;
