const { mergeConfig } = require("vite");
const path = require("path");

module.exports = {
  stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: ["@storybook/addon-links", "@storybook/addon-essentials", "@storybook/addon-storysource"],
  typescript: {
    reactDocgen: "react-docgen",
  },
  core: {
    builder: "@storybook/builder-vite",
  },
  async viteFinal(config, { configType }) {
    // return the customized config
    return mergeConfig(config, {
      // customize the Vite config here
      resolve: {
        alias: {
          "@litbase/use-observable": path.resolve(__dirname, "../../use-observable/src/index.ts"),
          "@litbase/use-force-update": path.resolve(__dirname, "../../use-force-update/src/index.ts"),
        },
      },
    });
  },
};
