import { createGlobalStyle, ThemeProvider } from "styled-components";
import { baseGlobalStyle, baseTheme } from "../src";

export const parameters = {
  layout: "centered",
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const decorators = [
  (Story) => (
    <ThemeProvider theme={baseTheme}>
      <GlobalStyle />
      <div style={{ width: "fit-content", minWidth: "300px" }}>
        <Story />
      </div>
    </ThemeProvider>
  ),
];

const GlobalStyle = createGlobalStyle`
  ${baseGlobalStyle};
`;
