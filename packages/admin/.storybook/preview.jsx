import { ThemeProvider, createGlobalStyle } from "styled-components";
import { MemoryRouter, Route, Routes } from "react-router-dom";
import { baseTheme, baseGlobalStyle, ModalProvider, BaseGlobalStyle } from "@litbase/alexandria";
import { HookForm } from "@litbase/alexandria";
import "tippy.js/dist/tippy.css";
import "tippy.js/themes/light-border.css";
import { DataAccessProvider } from "../src/contexts/data-provider/data-provider";
import { dummyDelete, dummyFetcher, dummySingleFetcher } from "../src/components/admin/dummy-fetcher";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const decorators = [
  (Story) => (
    <ThemeProvider theme={{ ...baseTheme, fontFamilyBase: "Inter", headingsFontFamily: "Inter" }}>
      <MemoryRouter initialEntries={["/"]}>
        <BaseGlobalStyle />
        <ModalProvider>
          <DataAccessProvider
            fetchBatch={dummyFetcher}
            getId={(item) => item.id}
            fetchSingle={dummySingleFetcher}
            persist={async (coll, values) => {
              console.log({ values });
            }}
            delete={async (collection, id) => {
              dummyDelete(collection, id);
            }}
          >
            <GlobalStyle />
            {Story()}
          </DataAccessProvider>
        </ModalProvider>
      </MemoryRouter>
    </ThemeProvider>
  ),
];

export const formDecorator = (Story) => <HookForm>{Story()}</HookForm>;

const GlobalStyle = createGlobalStyle`
  ${baseGlobalStyle};
  body {
    background: white;
  }
`;
