import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { setGlobalConfig } from "@storybook/testing-react";
import { getWorker } from "msw-storybook-addon";
import * as globalStorybookConfig from "../.storybook/preview";

setGlobalConfig(globalStorybookConfig);

// Ensure MSW connections are closed
afterAll(() => {
  try {
    getWorker().close();
  } catch (e) {}
});
