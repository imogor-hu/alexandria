import { FiltersDescription, OrderingConfig } from "./filter-types";

export * from "./filter-types";

export type FetchArgs<ItemType = unknown, IdType = unknown> = {
  limit: number;
  filters: FiltersDescription;
  orderings: OrderingConfig;
  after?: ItemType;
  skip?: number;
  collection: string;
  getId?: (item: ItemType) => IdType;
};

export type BatchFetcher<ItemType> = (args: FetchArgs) => Promise<ItemType[]>;
export type SingleFetcher<IdType, ItemType> = (collection: string, id: IdType) => Promise<ItemType | null>;
