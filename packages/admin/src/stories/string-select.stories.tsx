import { HookForm, HookStringSelectField } from "@litbase/alexandria";

export default {
  title: "Form/StringSelect",
  component: HookStringSelectField,
};

const Template = () => (
  <HookForm defaultValues={{ mode: "Text" }}>
    <HookStringSelectField name="mode" label="Source" options={["Text", "Text file"]} />
  </HookForm>
);

export const Default = Template.bind({});
Default.args = {};
