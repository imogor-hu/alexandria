import { FieldTypeValue } from "../components";

export enum FieldType {
  string = "string",
  number = "number",
  date = "date",
  bool = "bool",
  any = "any",
  doc = "document",
  image = "image",
  images = "images",
  file = "file",
  files = "files",
  object = "object",
  text = "text",
}

export type FieldConfig<T extends FieldType = FieldType> = FieldConfigBase<T> & FieldConfigExtension[T];

export type FieldConfigBase<T extends FieldType = FieldType> = {
  type: T;
  defaultValue?: FieldTypeValue[T];
  displayName?: string;
  placeholder?: string;
  virtual?: boolean;
};

export type FieldConfigExtension = {
  [FieldType.doc]: {
    joinConfig: {
      otherCollection: string;
      searchableFields: string[];
      getLabel: (item?: Record<string, unknown>) => string;
    };
  };
  [FieldType.string]: {};
  [FieldType.number]: {};
  [FieldType.date]: {};
  [FieldType.bool]: {};
  [FieldType.any]: {};
  [FieldType.image]: {};
  [FieldType.images]: {};
  [FieldType.file]: {};
  [FieldType.files]: {};
  [FieldType.text]: {};
  [FieldType.object]: {
    getLabel: string | ((obj: unknown) => string);
  };
};
