import { FilterBadgeProps } from "../filter-badge";
import { Calendar } from "@styled-icons/boxicons-regular";
import { Body, KeyName, StyledValue, CloseButton } from "../style-components";
import { Duration, formatDuration, intervalToDuration } from "date-fns";
import { FieldType } from "../../../../../../types/field-types";

export function DateBadge({ value, name, isRelative = false, onDelete }: FilterBadgeProps<FieldType.date>) {
  return (
    <>
      <KeyName>
        <Calendar />
        {name}
      </KeyName>
      <StyledValue>{formatValue(value, isRelative)}</StyledValue>
      <CloseButton onClick={onDelete} />
    </>
  );
}

function formatValue(value: FilterBadgeProps<FieldType.date>["value"], isRelative: boolean) {
  if (!isRelative && !(value instanceof Date)) {
    const { from, to } = value as { from?: Date; to?: Date };
    return [from, to]
      .filter((elem) => !!elem)
      .map((elem) => formatDate(elem))
      .join(" - ");
  }

  if (value instanceof Date) {
    return formatDate(value);
  }

  const { from } = value;

  if (!from) return "";

  const duration = intervalToDuration({ start: from, end: new Date() });
  const keys = ["years", "months", "weeks", "days", "hours", "minutes", "seconds"]
    .filter((key) => !!duration[key as keyof typeof duration])
    .slice(0, 2);
  return "last " + formatDuration(duration, { format: keys });
}

function formatDate(date?: Date) {
  if (!date) return "";
  return new Date(date).toLocaleDateString(undefined, { month: "short", day: "numeric" });
}
