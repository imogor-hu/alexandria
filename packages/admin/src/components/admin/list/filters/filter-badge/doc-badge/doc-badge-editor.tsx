import { FilterBadgeProps, FilterBadgeConfig } from "../filter-badge";
import AsyncSelect from "react-select/async";
import { useResource } from "../../../../resource";
import { FieldConfig, FieldType } from "../../../../../../types/field-types";
import { useDataAccess } from "../../../../../../contexts/data-provider/data-provider";

export function DocBadgeEditor(props: {
  value: FilterBadgeProps<FieldType.doc>;
  onChange: (value: FilterBadgeProps<FieldType.doc>) => void;
}) {
  const { getId } = useDataAccess();
  return (
    <>
      <DocumentSelector
        {...props.value}
        value={props.value.doc as Record<string, unknown>}
        onChange={(doc) => props.onChange({ ...props.value, doc, value: getId(doc) })}
      />
    </>
  );
}

function DocumentSelector(
  props: FilterBadgeConfig<FieldType.doc> & {
    value?: Record<string, unknown>;
    onChange: (value: Record<string, unknown>) => void;
  }
) {
  const { fetchBatch, getId, idToString } = useDataAccess();

  const joinConfig = (props.fieldConfig as FieldConfig<FieldType.doc>).joinConfig;

  return (
    <>
      <AsyncSelect
        value={props.value}
        onChange={(option) => props.onChange(option as Record<string, unknown>)}
        getOptionValue={(option) => idToString(getId(option))}
        getOptionLabel={(option) => joinConfig.getLabel(option as Record<string, unknown>) || ""}
        loadOptions={(inputValue: string) => {
          if (!joinConfig) {
            throw new Error(
              JSON.stringify({
                reason: "No join config provided in field configuration",
                providedConfig: props.fieldConfig,
              })
            );
          }
          return fetchBatch({
            collection: joinConfig.otherCollection,
            filters: [
              {
                or: joinConfig.searchableFields.map((fieldName) => ({
                  name: fieldName,
                  type: FieldType.string,
                  value: inputValue,
                  fieldConfig: {
                    type: FieldType.string,
                  },
                })),
              },
            ],
            limit: 25,
            orderings: {},
          });
        }}
      />
    </>
  );
}
