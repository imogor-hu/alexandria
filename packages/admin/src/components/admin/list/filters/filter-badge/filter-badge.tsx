import { FilterPrimitive } from "../../../../../contexts/data-provider/types";
import { FieldType } from "../../../../../types/field-types";
import { FilterBadgeEditor } from "./filter-badge-editor";
import { FilterBadgeRenderer } from "./filter-badge-renderer";
import { Body } from "./style-components";
import { spacing2 } from "@litbase/alexandria";
import { ReactNode, useEffect, useRef } from "react";
import { Instance } from "tippy.js";
import Tippy from "@tippyjs/react";
import styled from "styled-components";

export type FilterBadgeConfig<T extends FieldType = FieldType> = FilterPrimitive<T> & {
  displayName?: string;
  type: T;
};

export type FilterBadgeProps<T extends FieldType = FieldType> = {
  onDelete: () => void;
  onChange: (value: FilterBadgeConfig<T>) => void;
} & FilterBadgeConfig<T>;

export function FilterBadge<T extends FieldType = FieldType>(props: FilterBadgeProps<T>) {
  return (
    <BadgeBody editor={<FilterBadgeEditor<T> type={props.type} value={props} onChange={props.onChange} />}>
      {/* For some reason ts thinks props should satisfy "IntrinsicAttributes". I have no clue why. */}
      {/* @ts-ignore */}
      <FilterBadgeRenderer<T> {...props} />
    </BadgeBody>
  );
}

export function BadgeBody({ children, editor }: { children: ReactNode; editor: ReactNode }) {
  const ref = useRef<HTMLDivElement>(null);
  const tippyRef = useRef<Instance | null>(null);

  useEffect(() => {
    tippyRef.current?.show();
  }, []);

  return (
    <Tippy
      onCreate={(instance: any) => {
        tippyRef.current = instance;
      }}
      appendTo={document.body}
      theme="light-border"
      interactive
      trigger="click"
      content={<Col>{editor}</Col>}
    >
      <Body ref={ref}>{children}</Body>
    </Tippy>
  );
}

const Col = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spacing2};
  width: 18rem;
`;
