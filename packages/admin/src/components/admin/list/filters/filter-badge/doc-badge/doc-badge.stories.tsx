import { DateBadge } from "../date-badge";
import { DocBadge } from "./doc-badge";
import { FilterBadge } from "../filter-badge";
import { ComponentStory } from "@storybook/react";
import { useState } from "react";
import { usersConfig } from "../../../utils";
import { Resource } from "../../../../resource";
import { FieldType } from "../../../../../../types/field-types";

export default {
  title: "Admin/ListPage/Filters/FilterBadge/DocBadge",
  component: DocBadge,
};

const Template: ComponentStory<typeof DocBadge> = (args) => {
  const [filter, setFilter] = useState(args);
  return (
    <Resource config={usersConfig}>
      <FilterBadge {...args} {...filter} onDelete={() => undefined} onChange={setFilter} />
    </Resource>
  );
};

export const Default = Template.bind({});
Default.args = {
  name: "parent",
  type: FieldType.doc,
  fieldConfig: {
    ...usersConfig.fields.parent,
  },
};
