import { DateBadge } from "./date-badge";
import { FilterBadge } from "../filter-badge";
import { ComponentStory } from "@storybook/react";
import { useState } from "react";
import { FieldType } from "../../../../../../types/field-types";

export default {
  title: "Admin/ListPage/Filters/FilterBadge/DateBadge",
  component: DateBadge,
};

const Template: ComponentStory<typeof DateBadge> = (args) => {
  const [filter, setFilter] = useState(args);
  return (
    <>
      <FilterBadge {...args} {...filter} onDelete={() => undefined} onChange={setFilter} />
    </>
  );
};

export const Relative = Template.bind({});
Relative.args = {
  type: FieldType.date,
  name: "birthday",
  value: {
    from: new Date(2022, 2, 13),
  },
  isRelative: true,
};

export const Range = Template.bind({});
Range.args = {
  ...Relative.args,
  isRelative: false,
  value: {
    from: new Date(2022, 2, 13),
    to: new Date(2022, 3, 13),
  },
};

export const SingleValue = Template.bind({});
SingleValue.args = {
  ...Relative.args,
  value: new Date(),
  isRelative: false,
};
