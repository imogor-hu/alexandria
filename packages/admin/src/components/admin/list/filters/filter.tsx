import { getDefaultFieldValue } from "../../types";
import { FilterBadgeConfig } from "./filter-badge";
import { ComponentProps } from "react";
import { FieldConfig, FieldType } from "../../../../types/field-types";

export function Filter<T extends FieldType = FieldType>(props: { property: string }) {
  return null;
}

export function createEmptyFilter(
  filterConfig: ComponentProps<typeof Filter>,
  fieldConfig: FieldConfig
): FilterBadgeConfig {
  return {
    ...filterConfig,
    name: filterConfig.property,
    displayName: fieldConfig.displayName || filterConfig.property,
    type: fieldConfig.type,
    value: getDefaultFieldValue(fieldConfig),
    fieldConfig,
  };
}
