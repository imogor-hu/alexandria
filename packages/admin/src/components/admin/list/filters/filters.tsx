import styled from "styled-components";
import { ComponentProps, ReactNode, useEffect, useRef, useState } from "react";
import { useChildrenProps } from "../utils/use-children-props";
import { useResource } from "../../resource";
import { Button, spacing1, spacing2, spacing3 } from "@litbase/alexandria";
import { Plus } from "@styled-icons/boxicons-regular";
import { FilterBadge, FilterBadgeConfig } from "./filter-badge/filter-badge";
import Tippy from "@tippyjs/react";
import { createEmptyFilter, Filter as FilterComponent } from "./filter";
import { Instance } from "tippy.js";
import { useQueryBuilder } from "../list-page/list-page";

type FiltersState = (FilterBadgeConfig | { or: FiltersState })[];

export function Filters({ children }: { children: ReactNode }) {
  const queryBuilder = useQueryBuilder();
  const resource = useResource();
  const ref = useRef<Instance | null>(null);
  const filterProps = useChildrenProps<ComponentProps<typeof FilterComponent>>(children, [FilterComponent]);

  const [filters, setFilters] = useState<FiltersState>([]);
  useEffect(() => {
    queryBuilder.setFilters(filters);
  }, [filters]);

  return (
    <>
      <Body>
        {filters.map((elem, index) => {
          const filterBadgeConfig = elem as FilterBadgeConfig;
          // TODO: Handle 'or' filters
          if ((elem as { or: FiltersState }).or) return null;
          return (
            <FilterBadge
              {...filterBadgeConfig}
              onDelete={() => setFilters(filters.filter((elem, i) => i !== index))}
              onChange={(filter) =>
                setFilters([...filters.slice(0, index), { ...elem, ...filter }, ...filters.slice(index + 1)])
              }
            />
          );
        })}
        <Tippy
          theme="light-border"
          interactive
          appendTo={document.body}
          trigger="click"
          onCreate={(instance: any) => {
            ref.current = instance;
          }}
          content={
            <>
              <Col>
                <PopoverTitle>Properties</PopoverTitle>
                {filterProps.map((props, index) => (
                  <PropertyListElem
                    key={"property-" + index}
                    onClick={() => {
                      ref.current?.hide();
                      setFilters([...filters, createEmptyFilter(props, resource.fields[props.property])]);
                    }}
                  >
                    <span>{resource.fields[props.property]?.displayName || props.property}</span>
                    <span>{resource.fields[props.property]?.type}</span>
                  </PropertyListElem>
                ))}
              </Col>
            </>
          }
        >
          <AddFilterButton $size="sm">
            <Plus /> Add filter
          </AddFilterButton>
        </Tippy>
        {children}
      </Body>
    </>
  );
}

const PopoverTitle = styled.span`
  font-weight: 500;
  font-size: ${({ theme }) => theme.textBase};
  border-bottom: 1px solid ${({ theme }) => theme.gray100};
  width: calc(100% + 2.2rem);
  margin-left: -1.1rem;
  padding-left: 1.1rem;
  padding-bottom: ${spacing2};
  margin-bottom: ${spacing2};
`;

const PropertyListElem = styled.span`
  display: flex;
  align-items: center;
  cursor: pointer;
  border-radius: ${({ theme }) => theme.roundedLg};
  padding: ${spacing1} ${spacing2};
  margin-left: -${spacing2};
  transition: background-color 0.3s;
  &:hover {
    background: whitesmoke;
  }
  > span:nth-child(1) {
    font-size: ${({ theme }) => theme.textSm};
    font-weight: 500;
    text-transform: capitalize;
    display: flex;
    align-items: center;
  }
  > span:nth-child(1):after {
    content: "";
    width: 3px;
    height: 3px;
    border-radius: 100%;
    background: gray;
    display: inline-block;
    margin-left: ${spacing2};
    margin-right: ${spacing2};
  }
  > span:nth-child(2) {
    color: gray;
    font-size: ${({ theme }) => theme.textXs};
  }
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spacing1} ${spacing2};
`;

const AddFilterButton = styled(Button)`
  border-radius: ${({ theme }) => theme.roundedLg};
  border: 1px solid ${({ theme }) => theme.gray100};
`;

const Body = styled.div`
  display: flex;
  > * + * {
    margin-left: ${spacing3};
  }
`;
