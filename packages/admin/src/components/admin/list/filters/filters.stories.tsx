import { Filters } from "./filters";
import { Filter } from "./filter";
import { usersConfig } from "../utils/example-resource-configs";
import { Resource } from "../../resource";
import { ComponentStory } from "@storybook/react";
import { ListPage } from "../list-page";

export default {
  title: "Admin/ListPage/Filters",
  component: Filters,
};

const Template: ComponentStory<typeof Resource> = (args) => {
  return (
    <Resource {...args}>
      <ListPage>
        <Filters>
          <Filter property="username" />
          <Filter property="birthday" />
          <Filter property="isAdmin" />
        </Filters>
      </ListPage>
    </Resource>
  );
};

export const Default = Template.bind({});
Default.args = {
  config: usersConfig,
};
