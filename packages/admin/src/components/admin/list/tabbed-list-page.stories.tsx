import { ComponentStory } from "@storybook/react";
import { ListPage } from "./list-page/list-page";
import { Resource } from "../resource";
import { usersConfig } from "./utils/example-resource-configs";
import { ExampleListPage } from "./example-list-page";

export default {
  title: "Admin/ListPage/Tabbed",
  component: ListPage,
};

const Template: ComponentStory<typeof Resource> = (args) => {
  return (
    <Resource {...args}>
      <ListPage>
        <ExampleListPage />
      </ListPage>
    </Resource>
  );
};

export const TabbedList = Template.bind({});
TabbedList.parameters = {
  msw: {
    handlers: [],
  },
};
TabbedList.args = {
  config: usersConfig,
};
