import React, { createContext, ReactNode, useContext, useEffect, useMemo, useRef } from "react";
import { spacing4 } from "@litbase/alexandria";
import styled from "styled-components";
import { share, Subject } from "rxjs";
import { debounce } from "lodash-es";
import { FiltersDescription, OrderingConfig } from "../../../../contexts/data-provider/types";
import { ResourceContext } from "../../resource";

export enum PaginationType {
  SIMPLE = "simple",
  INFINITE = "infinite",
}

export interface ListPageContextValue {
  changeStream$?: Subject<{ filters: FiltersDescription; orderings: OrderingConfig }>;
  setFilters: (filters: FiltersDescription) => void;
  setOrderings: (orderings: OrderingConfig) => void;
  filtersRef: { current: FiltersDescription };
  orderingsRef: { current: OrderingConfig };
  emptyMessage?: string;
  newLabel?: string;
}

export const ListPageContext = createContext<ListPageContextValue | null>(null);

export function useQueryBuilder() {
  const context = useContext(ListPageContext);
  if (!context) throw new Error("Tried to use queryBuilder outside a ListPage");
  return context;
}

export interface ListPageConfig {
  children: ReactNode;
}

export function ListPage(props: ListPageConfig) {
  const resourceContext = useContext(ResourceContext);
  useEffect(() => {
    if (resourceContext?.registerListPage) {
      resourceContext.registerListPage(props);
    }
  }, [resourceContext]);

  return resourceContext?.registerListPage ? null : <InternalListPage {...props} />;
}

export function InternalListPage({ children, ...props }: ListPageConfig) {
  const filtersRef = useRef<FiltersDescription>([]);
  const orderingsRef = useRef<OrderingConfig>({});
  const changeStream$ = useMemo(() => {
    return new Subject<{ filters: FiltersDescription; orderings: OrderingConfig }>();
  }, []);
  const setFilters = useMemo(() => {
    return debounce((filters: FiltersDescription) => {
      filtersRef.current = filters;
      changeStream$.next({ filters: filtersRef.current, orderings: orderingsRef.current });
    }, 300);
  }, [changeStream$]);
  const setOrderings = useMemo(() => {
    return debounce((orderings: typeof orderingsRef.current) => {
      orderingsRef.current = orderings;
      changeStream$.next({ filters: filtersRef.current, orderings: orderingsRef.current });
    }, 300);
  }, [changeStream$]);
  const contextValue = useMemo(() => {
    return { setFilters, setOrderings, changeStream$, filtersRef, orderingsRef, ...props };
  }, [setFilters, setOrderings, changeStream$, filtersRef, orderingsRef, props]);

  return (
    <ListPageContext.Provider value={contextValue}>
      <PageBody>{children}</PageBody>
    </ListPageContext.Provider>
  );
}

const PageBody = styled.div`
  display: flex;
  flex-direction: column;
  > * + * {
    margin-top: ${spacing4};
  }
`;
