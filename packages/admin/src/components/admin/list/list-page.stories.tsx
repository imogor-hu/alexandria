import { ComponentStory } from "@storybook/react";
import { ListPage } from "./list-page/list-page";
import { Resource } from "../resource";
import { usersConfig } from "./utils/example-resource-configs";
import { ExampleListPage } from "./example-list-page";

export default {
  title: "Admin/ListPage",
  component: ListPage,
};

const Template: ComponentStory<typeof Resource> = (args) => {
  return (
    <Resource {...args}>
      <ListPage>
        <ExampleListPage />
      </ListPage>
    </Resource>
  );
};

export const UserList = Template.bind({});
UserList.parameters = {
  msw: {
    handlers: [],
  },
};
UserList.args = {
  config: usersConfig,
};
