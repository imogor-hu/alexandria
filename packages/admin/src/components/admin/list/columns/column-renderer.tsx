import { FieldTypeValue } from "../../types";
import { Cell, Checkbox, spacing4 } from "@litbase/alexandria";
import { ColumnConfig } from "./column";
import { ComponentType, createContext, Fragment, useContext } from "react";
import { ItemContext } from "./item-context";
import styled from "styled-components";
import { FieldConfig, FieldType } from "../../../../types/field-types";

const ColumnContext = createContext<(FieldConfig & ColumnConfig) | null>(null);

export function ColumnRenderer<T extends FieldType, ItemType = Record<string, unknown>>({
  config,
  value,
}: {
  config: FieldConfig<T> & ColumnConfig;
  value: ItemType;
}) {
  if (config.children) {
    const CellComponent = config.customCell ? Fragment : Cell;
    return (
      <CellComponent>{typeof config.children === "function" ? config.children(value) : config.children}</CellComponent>
    );
  }
  const Renderer: ComponentType = columnRenderers[config.type] || DefaultColumnRenderer;
  return (
    <ColumnContext.Provider value={config}>
      <Renderer />
    </ColumnContext.Provider>
  );
}

function DefaultColumnRenderer() {
  return (
    <Cell>
      <Value />
    </Cell>
  );
}

const columnRenderers: Record<FieldType, ComponentType> = {
  [FieldType.string]: DefaultColumnRenderer,
  [FieldType.number]: DefaultColumnRenderer,
  [FieldType.doc]: DefaultColumnRenderer,
  [FieldType.image]: DefaultColumnRenderer,
  [FieldType.bool]: DefaultColumnRenderer,
  [FieldType.any]: DefaultColumnRenderer,
  [FieldType.date]: DefaultColumnRenderer,
  [FieldType.images]: DefaultColumnRenderer,
  [FieldType.files]: DefaultColumnRenderer,
  [FieldType.file]: DefaultColumnRenderer,
  [FieldType.object]: DefaultColumnRenderer,
  [FieldType.text]: DefaultColumnRenderer,
};

export function Value<T extends FieldType = FieldType>() {
  const config = useContext(ColumnContext);
  const { item } = useContext(ItemContext);
  if (!config) {
    console.error("<Value /> used outside a <Column />");
    return null;
  }
  const scalarValue = config.property ? (item as Record<string, unknown>)[config.property] : undefined;
  switch (config.type) {
    case FieldType.string:
    case FieldType.number:
      return <>{scalarValue}</>;
    case FieldType.date:
      return <>{new Date(scalarValue as Date).toLocaleDateString()}</>;
    case FieldType.doc:
      return <>{(config as FieldConfig<FieldType.doc>).joinConfig.getLabel(scalarValue as FieldTypeValue[T])}</>;
    case FieldType.bool:
      return (
        <>
          <Checkbox disabled checked={scalarValue as boolean} value={scalarValue as string} />
        </>
      );
    case FieldType.image:
      return (
        <>
          <Image src={scalarValue as string} />
        </>
      );
    case FieldType.object:
      const getLabel = (config as FieldConfig<FieldType.object>).getLabel;
      if (!config.property) return null;
      return (
        <>
          {typeof getLabel === "function"
            ? getLabel(item)
            : (item?.[config.property] as Record<string, unknown>)?.[getLabel]}
        </>
      );
    default:
      return null;
  }
}

const Image = styled.img`
  width: 50px;
  height: 50px;
  border-radius: ${({ theme }) => theme.rounded};
  object-fit: cover;
`;
