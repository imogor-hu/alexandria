import { useResource } from "../../resource";
import { useQueryBuilder } from "../list-page";
import { useSubscription } from "@litbase/use-observable";
import { forMap, GridRow, InViewNotifier, LoadingIcon, spacing4, spacing6 } from "@litbase/alexandria";
import { ColumnInfo } from "./column";
import { useEffect, useMemo, useRef, useState } from "react";
import { ColumnRenderer } from "./column-renderer";
import styled from "styled-components";
import { ItemContext } from "./item-context";
import { useDataAccess } from "../../../../contexts/data-provider/data-provider";
import { FieldType } from "admin/src/types/field-types";
import { skip } from "rxjs";

export function InfiniteScroller<ItemType = unknown, IdType = unknown>({
  limit,
  columns,
}: {
  limit: number;
  columns: ColumnInfo;
}) {
  const { getId } = useDataAccess<ItemType, IdType>();
  const atBottom = useRef(false);
  const isLoading = useRef(true);
  const queryBuilder = useQueryBuilder();
  const [page, setPage] = useState(1);
  const [lastElem, setLastElem] = useState<ItemType | undefined>(undefined);
  const [queryCount, setQueryCount] = useState(0);

  // @ts-ignore
  useSubscription(() => {
    return (
      queryBuilder?.changeStream$?.pipe(skip(1)).subscribe(() => {
        isLoading.current = true;
        setLastElem(undefined);
        setPage(1);
        setQueryCount((prev) => prev + 1);
      }) || undefined
    );
  }, [queryBuilder?.changeStream$]);

  return (
    <>
      {forMap(0, page, (index) => (
        <RowBatch<ItemType, IdType>
          key={queryCount + "-" + index}
          index={index}
          limit={limit}
          afterId={lastElem ? getId(lastElem) : undefined}
          onLoaded={(items) => {
            isLoading.current = false;
            if (items.length >= limit) {
              setLastElem(items.slice(-1)[0]);
              if (atBottom.current) {
                setPage((prev) => prev + 1);
                isLoading.current = true;
              }
            } else {
              setLastElem(undefined);
            }
          }}
          columns={columns}
        />
      ))}
      <InViewNotifier
        onInView={() => {
          atBottom.current = true;
          if (!isLoading.current) {
            if (lastElem) {
              isLoading.current = true;
              setPage((prev) => prev + 1);
            }
          }
        }}
        onOutOfView={() => {
          atBottom.current = false;
        }}
      />
    </>
  );
}

function RowBatch<ItemType, IdType>({
  limit,
  afterId,
  onLoaded,
  columns,
  index: batchIndex,
}: {
  limit: number;
  afterId: IdType | undefined;
  onLoaded: (items: ItemType[]) => void;
  columns: ColumnInfo;
  index: number;
}) {
  const queryBuilder = useQueryBuilder();
  const resource = useResource();
  const { fetchBatch } = useDataAccess<ItemType, IdType>();
  const [items, setItems] = useState<ItemType[] | null>(null);
  const cachedAfter = useMemo(() => {
    return afterId;
  }, []);

  useEffect(() => {
    fetchBatch({
      limit,
      filters: queryBuilder.filtersRef.current,
      orderings: queryBuilder.orderingsRef.current,
      ...(queryBuilder.orderingsRef.current ? { skip: batchIndex * limit } : { after: cachedAfter }),
      collection: resource.name,
    }).then((items) => {
      setItems(items);
    });
  }, []);

  useEffect(() => {
    if (items) {
      onLoaded(items);
    }
  }, [items]);

  if (!items) {
    return (
      <GridRow>
        <LoadingIconContainer>
          <LoadingIcon />
        </LoadingIconContainer>
      </GridRow>
    );
  }
  if (batchIndex === 0 && items.length === 0) {
    return (
      <GridRow>
        <EmptyMessage>{queryBuilder?.emptyMessage || "No results found"}</EmptyMessage>
      </GridRow>
    );
  }

  return (
    <>
      {items.map((item, itemIndex) => (
        <GridRowRenderer
          item={item}
          key={itemIndex}
          batchIndex={batchIndex}
          limit={limit}
          itemIndex={itemIndex}
          columns={columns}
        />
      ))}
    </>
  );
}

function GridRowRenderer<ItemType>({
  item,
  batchIndex,
  itemIndex,
  limit,
  columns,
}: {
  item: ItemType;
  batchIndex: number;
  limit: number;
  itemIndex: number;
  columns: ColumnInfo;
}) {
  const [isDeleted, setIsDeleted] = useState(false);
  if (isDeleted) return null;
  return (
    <ItemContext.Provider value={{ item: item as Record<string, unknown>, onDeleted: () => setIsDeleted(true) }}>
      <GridRow key={batchIndex * limit + itemIndex}>
        {columns.map((config, colIndex) => (
          <ColumnRenderer<FieldType, ItemType> key={colIndex} config={config} value={item} />
        ))}
      </GridRow>
    </ItemContext.Provider>
  );
}

const EmptyMessage = styled.div`
  grid-column: 1 / -1;
  display: flex;
  justify-content: center;
  padding: ${spacing6};
`;

const LoadingIconContainer = styled.div`
  display: flex;
  grid-column: 1 / -1;
  justify-content: center;
  padding: ${spacing4};
`;
