import { ReactNode, useMemo } from "react";
import { useChildrenProps } from "../utils";
import { Card, Grid, GridRow, HeadCell, spacing4 } from "@litbase/alexandria";
import { useResource } from "../../resource";
import styled from "styled-components";
import { DownArrow, UpArrow } from "@styled-icons/boxicons-solid";
import { InfiniteScroller } from "./infinite-scroller";
import { Column, ColumnConfig, ColumnInfo } from "./column";
import { useQueryBuilder } from "../list-page";
import { FieldConfig } from "../../../../types/field-types";
import { useObservable } from "@litbase/use-observable";
import { map, switchMap } from "rxjs";

export function Table({ children, limit = 7 }: { children: ReactNode; limit?: number }) {
  const resource = useResource();
  const columnProps = useChildrenProps<ColumnConfig>(children, [Column]);

  const columns = useMemo(() => {
    return columnProps.map((columnConfig) => ({
      ...columnConfig,
      ...(columnConfig.property ? resource.fields[columnConfig.property] : {}),
    })) as ColumnInfo;
  }, [columnProps, resource]);

  return (
    <>
      <Card $padding={0}>
        <Grid columns={columns.map((elem) => elem.columnSpace || "1fr").join(" ")}>
          <GridRow>
            {columns.map((config, index) => (
              <HeadCellComponent config={config} index={index} key={index} />
            ))}
          </GridRow>
          <InfiniteScroller columns={columns} limit={limit} />
        </Grid>
      </Card>
      {children}
    </>
  );
}

function HeadCellComponent({ config }: { config: ColumnConfig; index: number }) {
  const queryBuilder = useQueryBuilder();

  const [orderings] = useObservable(
    (inputs$) => {
      return inputs$.pipe(
        switchMap(([changeStream$]) => changeStream$),
        map(() => {
          return queryBuilder?.orderingsRef.current;
        })
      );
    },
    queryBuilder.orderingsRef.current,
    [queryBuilder.changeStream$!]
  );

  return (
    <StyledHeadCell>
      {(config as FieldConfig).displayName || config.property}
      {config.property && config.sortable && (
        <OrderingArrowCol>
          <StyledArrow
            $active={orderings[config.property] === 1}
            onClick={() =>
              queryBuilder.setOrderings(
                orderings[config.property as string] !== 1 ? { [config.property as string]: 1 } : {}
              )
            }
          />
          <StyledArrow
            $active={orderings[config.property] === -1}
            onClick={() =>
              queryBuilder.setOrderings(
                orderings[config.property as string] === -1 ? {} : { [config.property as string]: -1 }
              )
            }
            as={DownArrow}
          />
        </OrderingArrowCol>
      )}
    </StyledHeadCell>
  );
}

const StyledHeadCell = styled(HeadCell)``;

const OrderingArrowCol = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-left: ${spacing4};
`;

const StyledArrow = styled(UpArrow)<{ $active?: boolean }>`
  transition: color 0.3s;
  color: ${({ $active }) => ($active ? "black" : "#bbbbbb")};
  cursor: pointer;
  &:hover {
    color: black;
  }
`;
