import { ModelConfig } from "../../types";
import { User } from "@styled-icons/boxicons-solid";
import { FieldType } from "../../../../types/field-types";

export const usersConfig: ModelConfig = {
  name: "users",
  displayName: "Users",
  singleName: "User",
  icon: User,
  fields: {
    username: {
      type: FieldType.string,
      displayName: "Name",
    },
    birthday: {
      type: FieldType.date,
      displayName: "Birthday",
    },
    isAdmin: {
      type: FieldType.bool,
      displayName: "Has admin privileges",
    },
    profilePic: {
      type: FieldType.image,
      displayName: "Profile picture",
    },
    photos: {
      type: FieldType.images,
      displayName: "Photos",
    },
    parent: {
      type: FieldType.doc,
      displayName: "Parent",
      joinConfig: {
        otherCollection: "users",
        searchableFields: ["username"],
        getLabel: (item) => {
          return (item?.username || "") as string;
        },
      },
    },
    parentDoc: {
      type: FieldType.object,
      displayName: "ParentDoc",
      getLabel: "name",
    },
  },
};
