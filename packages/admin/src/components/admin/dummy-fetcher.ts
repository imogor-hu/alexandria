import { FilterBadgeConfig, ListPageContextValue } from "./list";
import { FieldConfig, FieldType } from "../../types/field-types";

export async function dummyFetcher({
  limit,
  filters,
  orderings,
  after,
  collection,
  getId,
  skip,
}: {
  limit: number;
  filters: ListPageContextValue["filtersRef"]["current"];
  orderings: ListPageContextValue["orderingsRef"]["current"];
  after?: unknown;
  skip?: number;
  collection: string;
  getId: (item: any) => any;
}) {
  const baseList: Record<string, unknown>[] = collections[collection] || [];

  return new Promise<Record<string, unknown>[]>((resolve) => {
    setTimeout(() => {
      const filteredList = baseList.filter((elem) => {
        for (const filter of filters) {
          const orFilters = filter as { or: FilterBadgeConfig[] };
          if (Array.isArray(orFilters.or)) {
            if (orFilters.or.length === 0) continue;
            let foundMatching = false;
            for (const orFilter of orFilters.or) {
              if (doesElemSatisfyFilter(orFilter, elem, getId)) foundMatching = true;
            }
            if (!foundMatching) return false;
          } else {
            if (!doesElemSatisfyFilter(filter as FilterBadgeConfig, elem, getId)) return false;
          }
        }
        return true;
      });
      const [orderKey, orderDirection] =
        Object.entries(orderings).length > 0 ? [...Object.entries(orderings)[0]] : [undefined, undefined];
      const orderedList = !orderKey
        ? filteredList
        : filteredList.sort((a, b) => compareValues(a[orderKey], b[orderKey]) * (orderDirection === 1 ? 1 : -1));
      const index = !after ? skip || 0 : orderedList.findIndex((elem) => elem.id === after) + 1;
      const paginatedList = orderedList.slice(index, index + limit);
      resolve(paginatedList);
    }, 100);
  });
}

export async function dummySingleFetcher(collection: string, id: number) {
  return collections[collection].find((elem) => elem.id == id) || null;
}

function compareValues(a: unknown, b: unknown) {
  if (typeof a === "string") {
    return a.localeCompare(b as string);
  }
  if (["number", "object"].includes(typeof a)) {
    return (a as number) - (b as number);
  }
  return 0;
}

function doesElemSatisfyFilter(filter: FilterBadgeConfig, elem: Record<string, unknown>, getId: (item: any) => any) {
  switch (filter.type) {
    case FieldType.number:
    case FieldType.bool:
      if (elem[filter.name] !== filter.value) {
        return false;
      }
      break;
    case FieldType.string:
      if (!(elem[filter.name] as string).includes(filter.value)) return false;
      break;
    case FieldType.date:
      if (filter.value.from) {
        if (
          new Date(elem[filter.name] as Date) > new Date(filter.value.from) &&
          (!filter.value.to || new Date(filter.value.to) > new Date(elem[filter.name] as Date))
        ) {
        } else {
          return false;
        }
      } else {
        if (!elem[filter.name] !== filter.value) {
          return false;
        }
      }
      break;
    case FieldType.doc:
      if (!(filter.fieldConfig as FieldConfig<FieldType.doc>).joinConfig)
        throw new Error(
          JSON.stringify({
            reason: "Improperly configured field. Please provide a joinConfig when using the 'doc' fieldType",
            providedConfig: filter.fieldConfig,
          })
        );
      if (getId(elem[filter.name]) !== filter.value) return false;
  }
  return true;
}

export function dummyDelete(collection: string, id: number) {
  collections[collection] = collections[collection].filter((elem) => elem.id !== id);
}

const collections: Record<string, Record<string, unknown>[]> = {
  users: [
    { id: 1, username: "Bob", birthday: new Date(2022, 1, 13), isAdmin: true },
    {
      id: 2,
      username: "Jim",
      birthday: new Date(2022, 1, 12),
      isAdmin: false,
      parent: 1,
      parentDoc: {
        name: "Hello there",
        some: "other value",
      },
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 3,
      username: "Jill",
      birthday: new Date(1998, 1, 13),
      isAdmin: false,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 4,
      username: "Kate",
      birthday: new Date(1978, 1, 13),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 5,
      username: "Sam",
      birthday: new Date(2022, 0, 12),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 6,
      username: "Frodo",
      birthday: new Date(2022, 3, 4),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 7,
      username: "Bilbo",
      birthday: new Date(1997, 10, 26),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 8,
      username: "Geralt",
      birthday: new Date(),
      isAdmin: true,
    },
    { profilePic: "http://placekitten.com/200/300", id: 9, username: "Ciri", birthday: new Date(), isAdmin: true },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 10,
      username: "Luffy",
      birthday: new Date(),
      isAdmin: true,
    },
    { profilePic: "http://placekitten.com/200/300", id: 11, username: "Nami", birthday: new Date(), isAdmin: true },
    { profilePic: "http://placekitten.com/200/300", id: 12, username: "Zoro", birthday: new Date(), isAdmin: true },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 13,
      username: "Sanji",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 14,
      username: "Blackbeard",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 15,
      username: "Edward Kenway",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 16,
      username: "Ezio Audiotre da Firenze",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 17,
      username: "Desmond",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 18,
      username: "The moon bear",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 19,
      username: "Muffin",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 20,
      username: "Llama",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 21,
      username: "I like trains",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 22,
      username: "Carl Poppa",
      birthday: new Date(),
      isAdmin: true,
    },
    { profilePic: "http://placekitten.com/200/300", id: 23, username: "Yoda", birthday: new Date(), isAdmin: true },
    { profilePic: "http://placekitten.com/200/300", id: 24, username: "Luke", birthday: new Date(), isAdmin: true },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 25,
      username: "Leila",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 26,
      username: "Han Solo",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 27,
      username: "Boba Fett",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 28,
      username: "Darth Revan",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 29,
      username: "Commander Shepard",
      birthday: new Date(),
      isAdmin: true,
    },
    {
      profilePic: "http://placekitten.com/200/300",
      id: 30,
      username: "My favourite store on the citadel",
      birthday: new Date(),
      isAdmin: true,
    },
  ],
};
