import { ModelConfig } from "./types";
import { createContext, ReactNode, useContext, useEffect, useMemo, useState } from "react";
import { InternalListPage, ListPageConfig } from "./list";
import { Route, Routes } from "react-router-dom";
import { AdminContext } from "./admin";
import { noop } from "lodash-es";
import { EditPageConfig, InternalEditPage } from "./edit";

interface ResourceContextValue extends ModelConfig {
  registerEditPage: (editPageConfig: EditPageConfig) => void;
  registerListPage: (listPageConfig: ListPageConfig) => void;
}

export const ResourceContext = createContext<ResourceContextValue | null>({
  name: "",
  fields: {},
  registerEditPage: noop,
  registerListPage: noop,
});

export function useResource() {
  const context = useContext(ResourceContext);
  if (!context) throw new Error("Tried to useResource outside a <Resource />");
  return context;
}

export function Resource({
  config,
  children,
  noRoutes = false,
}: {
  config: ModelConfig;
  children: ReactNode;
  noRoutes?: boolean;
}) {
  const adminContext = useContext(AdminContext);
  const [configs, setConfigs] = useState<{ editPageConfig?: EditPageConfig; listPageConfig?: ListPageConfig }>({});
  const shouldRenderRoutes = !noRoutes && !!adminContext && (configs.listPageConfig || configs.editPageConfig);
  const contextConfig = useMemo(() => {
    return {
      ...config,
      registerEditPage: (editPageConfig: EditPageConfig) => setConfigs((prev) => ({ ...prev, editPageConfig })),
      registerListPage: (listPageConfig: ListPageConfig) => setConfigs((prev) => ({ ...prev, listPageConfig })),
    };
  }, [config]);

  useEffect(() => {
    if (adminContext?.registerModel) {
      adminContext.registerModel({ config, children, noRoutes });
    }
  }, [config, children, noRoutes, adminContext?.registerModel]);

  return (
    <ResourceContext.Provider value={contextConfig}>
      {shouldRenderRoutes ? (
        <Routes>
          {configs.listPageConfig && (
            <Route path={config.name} element={<InternalListPage {...configs.listPageConfig} />} />
          )}
          {configs.editPageConfig && (
            <Route path={config.name + "/:id"} element={<InternalEditPage {...configs.editPageConfig} />} />
          )}
        </Routes>
      ) : (
        <>
          {configs.listPageConfig && <InternalListPage {...configs.listPageConfig} />}
          {configs.editPageConfig && <InternalEditPage {...configs.editPageConfig} />}
        </>
      )}
      {children}
    </ResourceContext.Provider>
  );
}
